User-agent: *
Disallow: /simple_cms/
Disallow: /sc/
Disallow: /simple-cms/
Disallow: *utm*=
Disallow: *openstat=
Disallow: *from=
Disallow: /api/
Disallow: /adminer.php
Disallow: /index.php

Allow: *?page=

Sitemap: https://lapa-kota.ru/sitemap.xml