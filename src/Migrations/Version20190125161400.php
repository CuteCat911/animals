<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125161400 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE view (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, user_agent VARCHAR(512) NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_FEFDAB8E7294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, breadcrumb_name VARCHAR(128) DEFAULT NULL, path VARCHAR(128) DEFAULT NULL, published TINYINT(1) NOT NULL, index_number INT NOT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(128) DEFAULT NULL, email VARCHAR(128) NOT NULL, password VARCHAR(128) NOT NULL, name VARCHAR(64) DEFAULT NULL, surname VARCHAR(128) DEFAULT NULL, patronymic VARCHAR(128) DEFAULT NULL, is_active TINYINT(1) NOT NULL, is_blocked TINYINT(1) NOT NULL, subscribe TINYINT(1) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649AA08CB10 (login), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_article (user_id INT NOT NULL, article_id INT NOT NULL, INDEX IDX_5A37106CA76ED395 (user_id), INDEX IDX_5A37106C7294869C (article_id), PRIMARY KEY(user_id, article_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, author_id INT DEFAULT NULL, approve TINYINT(1) NOT NULL, publication_time DATETIME DEFAULT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, breadcrumb_name VARCHAR(128) DEFAULT NULL, path VARCHAR(128) DEFAULT NULL, published TINYINT(1) NOT NULL, index_number INT NOT NULL, INDEX IDX_23A0E6612469DE2 (category_id), INDEX IDX_23A0E66F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_user (article_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3DD151487294869C (article_id), INDEX IDX_3DD15148A76ED395 (user_id), PRIMARY KEY(article_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE view ADD CONSTRAINT FK_FEFDAB8E7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE user_article ADD CONSTRAINT FK_5A37106CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_article ADD CONSTRAINT FK_5A37106C7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6612469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article_user ADD CONSTRAINT FK_3DD151487294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_user ADD CONSTRAINT FK_3DD15148A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_field ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE content_field ADD CONSTRAINT FK_B036118A12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_B036118A12469DE2 ON content_field (category_id)');
        $this->addSql('ALTER TABLE file_field ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE file_field ADD CONSTRAINT FK_F176F56F12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_F176F56F12469DE2 ON file_field (category_id)');
        $this->addSql('ALTER TABLE tag ADD created_time DATETIME DEFAULT NULL, ADD updated_time DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE image_field ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_field ADD CONSTRAINT FK_4CB0C1F112469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_4CB0C1F112469DE2 ON image_field (category_id)');
        $this->addSql('ALTER TABLE multiple_field ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE multiple_field ADD CONSTRAINT FK_CD707B6412469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_CD707B6412469DE2 ON multiple_field (category_id)');
        $this->addSql('ALTER TABLE text_field ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF0512469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_D41FF0512469DE2 ON text_field (category_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE content_field DROP FOREIGN KEY FK_B036118A12469DE2');
        $this->addSql('ALTER TABLE file_field DROP FOREIGN KEY FK_F176F56F12469DE2');
        $this->addSql('ALTER TABLE image_field DROP FOREIGN KEY FK_4CB0C1F112469DE2');
        $this->addSql('ALTER TABLE multiple_field DROP FOREIGN KEY FK_CD707B6412469DE2');
        $this->addSql('ALTER TABLE text_field DROP FOREIGN KEY FK_D41FF0512469DE2');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6612469DE2');
        $this->addSql('ALTER TABLE user_article DROP FOREIGN KEY FK_5A37106CA76ED395');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66F675F31B');
        $this->addSql('ALTER TABLE article_user DROP FOREIGN KEY FK_3DD15148A76ED395');
        $this->addSql('ALTER TABLE view DROP FOREIGN KEY FK_FEFDAB8E7294869C');
        $this->addSql('ALTER TABLE user_article DROP FOREIGN KEY FK_5A37106C7294869C');
        $this->addSql('ALTER TABLE article_user DROP FOREIGN KEY FK_3DD151487294869C');
        $this->addSql('DROP TABLE view');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_article');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_user');
        $this->addSql('DROP INDEX IDX_B036118A12469DE2 ON content_field');
        $this->addSql('ALTER TABLE content_field DROP category_id');
        $this->addSql('DROP INDEX IDX_F176F56F12469DE2 ON file_field');
        $this->addSql('ALTER TABLE file_field DROP category_id');
        $this->addSql('DROP INDEX IDX_4CB0C1F112469DE2 ON image_field');
        $this->addSql('ALTER TABLE image_field DROP category_id');
        $this->addSql('DROP INDEX IDX_CD707B6412469DE2 ON multiple_field');
        $this->addSql('ALTER TABLE multiple_field DROP category_id');
        $this->addSql('ALTER TABLE tag DROP created_time, DROP updated_time');
        $this->addSql('DROP INDEX IDX_D41FF0512469DE2 ON text_field');
        $this->addSql('ALTER TABLE text_field DROP category_id');
    }
}
