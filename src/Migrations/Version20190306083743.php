<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190306083743 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE subscription_email_subscription_type (subscription_email_id INT NOT NULL, subscription_type_id INT NOT NULL, INDEX IDX_E77E5734CF9C9EE5 (subscription_email_id), INDEX IDX_E77E5734B6596C08 (subscription_type_id), PRIMARY KEY(subscription_email_id, subscription_type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, description LONGTEXT DEFAULT NULL, index_number INT NOT NULL, disabled TINYINT(1) NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subscription_email_subscription_type ADD CONSTRAINT FK_E77E5734CF9C9EE5 FOREIGN KEY (subscription_email_id) REFERENCES subscription_email (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subscription_email_subscription_type ADD CONSTRAINT FK_E77E5734B6596C08 FOREIGN KEY (subscription_type_id) REFERENCES subscription_type (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE article_user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription_email_subscription_type DROP FOREIGN KEY FK_E77E5734B6596C08');
        $this->addSql('CREATE TABLE article_user (article_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_3DD151487294869C (article_id), INDEX IDX_3DD15148A76ED395 (user_id), PRIMARY KEY(article_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE article_user ADD CONSTRAINT FK_3DD151487294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_user ADD CONSTRAINT FK_3DD15148A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE subscription_email_subscription_type');
        $this->addSql('DROP TABLE subscription_type');
    }
}
