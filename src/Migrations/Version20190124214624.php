<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190124214624 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE snippet (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, global_data_id INT DEFAULT NULL, server_name VARCHAR(128) NOT NULL, content LONGTEXT DEFAULT NULL, variables LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_961C8CD5727ACA70 (parent_id), INDEX IDX_961C8CD55C7161DD (global_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE snippet ADD CONSTRAINT FK_961C8CD5727ACA70 FOREIGN KEY (parent_id) REFERENCES snippet_template (id)');
        $this->addSql('ALTER TABLE snippet ADD CONSTRAINT FK_961C8CD55C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('ALTER TABLE snippet_template ADD structure LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE snippet');
        $this->addSql('ALTER TABLE snippet_template DROP structure');
    }
}
