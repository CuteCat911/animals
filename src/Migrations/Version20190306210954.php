<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190306210954 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE email_template (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, description LONGTEXT DEFAULT NULL, index_number INT NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE email ADD template_id INT DEFAULT NULL, DROP template');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C745DA0FB8 FOREIGN KEY (template_id) REFERENCES email_template (id)');
        $this->addSql('CREATE INDEX IDX_E7927C745DA0FB8 ON email (template_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C745DA0FB8');
        $this->addSql('DROP TABLE email_template');
        $this->addSql('DROP INDEX IDX_E7927C745DA0FB8 ON email');
        $this->addSql('ALTER TABLE email ADD template VARCHAR(256) NOT NULL COLLATE utf8mb4_unicode_ci, DROP template_id');
    }
}
