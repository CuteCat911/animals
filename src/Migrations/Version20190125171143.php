<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125171143 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE share_user_agent (id INT AUTO_INCREMENT NOT NULL, global_data_id INT DEFAULT NULL, name VARCHAR(32) NOT NULL, user_agent VARCHAR(128) NOT NULL, INDEX IDX_20AC5F035C7161DD (global_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE share_user_agent_image (id INT AUTO_INCREMENT NOT NULL, user_agent_id INT DEFAULT NULL, path VARCHAR(256) NOT NULL, INDEX IDX_4C22222AD499950B (user_agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page_share_user_agent_image (page_id INT NOT NULL, share_user_agent_image_id INT NOT NULL, INDEX IDX_959F7458C4663E4 (page_id), INDEX IDX_959F74582149018E (share_user_agent_image_id), PRIMARY KEY(page_id, share_user_agent_image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_share_user_agent_image (category_id INT NOT NULL, share_user_agent_image_id INT NOT NULL, INDEX IDX_316BE79612469DE2 (category_id), INDEX IDX_316BE7962149018E (share_user_agent_image_id), PRIMARY KEY(category_id, share_user_agent_image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_share_user_agent_image (article_id INT NOT NULL, share_user_agent_image_id INT NOT NULL, INDEX IDX_1290A19B7294869C (article_id), INDEX IDX_1290A19B2149018E (share_user_agent_image_id), PRIMARY KEY(article_id, share_user_agent_image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE share_user_agent ADD CONSTRAINT FK_20AC5F035C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('ALTER TABLE share_user_agent_image ADD CONSTRAINT FK_4C22222AD499950B FOREIGN KEY (user_agent_id) REFERENCES share_user_agent (id)');
        $this->addSql('ALTER TABLE page_share_user_agent_image ADD CONSTRAINT FK_959F7458C4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE page_share_user_agent_image ADD CONSTRAINT FK_959F74582149018E FOREIGN KEY (share_user_agent_image_id) REFERENCES share_user_agent_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_share_user_agent_image ADD CONSTRAINT FK_316BE79612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_share_user_agent_image ADD CONSTRAINT FK_316BE7962149018E FOREIGN KEY (share_user_agent_image_id) REFERENCES share_user_agent_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_share_user_agent_image ADD CONSTRAINT FK_1290A19B7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_share_user_agent_image ADD CONSTRAINT FK_1290A19B2149018E FOREIGN KEY (share_user_agent_image_id) REFERENCES share_user_agent_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content_field ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE content_field ADD CONSTRAINT FK_B036118A7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_B036118A7294869C ON content_field (article_id)');
        $this->addSql('ALTER TABLE file_field ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE file_field ADD CONSTRAINT FK_F176F56F7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_F176F56F7294869C ON file_field (article_id)');
        $this->addSql('ALTER TABLE image_field ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_field ADD CONSTRAINT FK_4CB0C1F17294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_4CB0C1F17294869C ON image_field (article_id)');
        $this->addSql('ALTER TABLE multiple_field ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE multiple_field ADD CONSTRAINT FK_CD707B647294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_CD707B647294869C ON multiple_field (article_id)');
        $this->addSql('ALTER TABLE text_field ADD article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF057294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_D41FF057294869C ON text_field (article_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE share_user_agent_image DROP FOREIGN KEY FK_4C22222AD499950B');
        $this->addSql('ALTER TABLE page_share_user_agent_image DROP FOREIGN KEY FK_959F74582149018E');
        $this->addSql('ALTER TABLE category_share_user_agent_image DROP FOREIGN KEY FK_316BE7962149018E');
        $this->addSql('ALTER TABLE article_share_user_agent_image DROP FOREIGN KEY FK_1290A19B2149018E');
        $this->addSql('DROP TABLE share_user_agent');
        $this->addSql('DROP TABLE share_user_agent_image');
        $this->addSql('DROP TABLE page_share_user_agent_image');
        $this->addSql('DROP TABLE category_share_user_agent_image');
        $this->addSql('DROP TABLE article_share_user_agent_image');
        $this->addSql('ALTER TABLE content_field DROP FOREIGN KEY FK_B036118A7294869C');
        $this->addSql('DROP INDEX IDX_B036118A7294869C ON content_field');
        $this->addSql('ALTER TABLE content_field DROP article_id');
        $this->addSql('ALTER TABLE file_field DROP FOREIGN KEY FK_F176F56F7294869C');
        $this->addSql('DROP INDEX IDX_F176F56F7294869C ON file_field');
        $this->addSql('ALTER TABLE file_field DROP article_id');
        $this->addSql('ALTER TABLE image_field DROP FOREIGN KEY FK_4CB0C1F17294869C');
        $this->addSql('DROP INDEX IDX_4CB0C1F17294869C ON image_field');
        $this->addSql('ALTER TABLE image_field DROP article_id');
        $this->addSql('ALTER TABLE multiple_field DROP FOREIGN KEY FK_CD707B647294869C');
        $this->addSql('DROP INDEX IDX_CD707B647294869C ON multiple_field');
        $this->addSql('ALTER TABLE multiple_field DROP article_id');
        $this->addSql('ALTER TABLE text_field DROP FOREIGN KEY FK_D41FF057294869C');
        $this->addSql('DROP INDEX IDX_D41FF057294869C ON text_field');
        $this->addSql('ALTER TABLE text_field DROP article_id');
    }
}
