<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190124205922 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE navigation_item (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, navigation_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, index_number INT NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_289BF06CC4663E4 (page_id), INDEX IDX_289BF06C39F79D6D (navigation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, index_number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE global_data (id INT AUTO_INCREMENT NOT NULL, site_name VARCHAR(64) DEFAULT NULL, site_lang VARCHAR(2) DEFAULT NULL, site_name_on_title TINYINT(1) NOT NULL, default_share_image VARCHAR(128) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, breadcrumb_name VARCHAR(128) DEFAULT NULL, path VARCHAR(128) DEFAULT NULL, published TINYINT(1) NOT NULL, index_number INT NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_140AB620727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE snippet_template (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, published TINYINT(1) NOT NULL, index_number INT NOT NULL, template LONGTEXT DEFAULT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE navigation (id INT AUTO_INCREMENT NOT NULL, server_name VARCHAR(32) NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE navigation_item ADD CONSTRAINT FK_289BF06CC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE navigation_item ADD CONSTRAINT FK_289BF06C39F79D6D FOREIGN KEY (navigation_id) REFERENCES navigation (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620727ACA70 FOREIGN KEY (parent_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE content_field ADD global_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE content_field ADD CONSTRAINT FK_B036118A5C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('CREATE INDEX IDX_B036118A5C7161DD ON content_field (global_data_id)');
        $this->addSql('ALTER TABLE file_field ADD global_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE file_field ADD CONSTRAINT FK_F176F56F5C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('CREATE INDEX IDX_F176F56F5C7161DD ON file_field (global_data_id)');
        $this->addSql('ALTER TABLE image_field ADD global_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_field ADD CONSTRAINT FK_4CB0C1F15C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('CREATE INDEX IDX_4CB0C1F15C7161DD ON image_field (global_data_id)');
        $this->addSql('ALTER TABLE multiple_field ADD global_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE multiple_field ADD CONSTRAINT FK_CD707B645C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('CREATE INDEX IDX_CD707B645C7161DD ON multiple_field (global_data_id)');
        $this->addSql('ALTER TABLE text_field ADD global_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF055C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('CREATE INDEX IDX_D41FF055C7161DD ON text_field (global_data_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE content_field DROP FOREIGN KEY FK_B036118A5C7161DD');
        $this->addSql('ALTER TABLE file_field DROP FOREIGN KEY FK_F176F56F5C7161DD');
        $this->addSql('ALTER TABLE image_field DROP FOREIGN KEY FK_4CB0C1F15C7161DD');
        $this->addSql('ALTER TABLE multiple_field DROP FOREIGN KEY FK_CD707B645C7161DD');
        $this->addSql('ALTER TABLE text_field DROP FOREIGN KEY FK_D41FF055C7161DD');
        $this->addSql('ALTER TABLE navigation_item DROP FOREIGN KEY FK_289BF06CC4663E4');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB620727ACA70');
        $this->addSql('ALTER TABLE navigation_item DROP FOREIGN KEY FK_289BF06C39F79D6D');
        $this->addSql('DROP TABLE navigation_item');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE global_data');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE snippet_template');
        $this->addSql('DROP TABLE navigation');
        $this->addSql('DROP INDEX IDX_B036118A5C7161DD ON content_field');
        $this->addSql('ALTER TABLE content_field DROP global_data_id');
        $this->addSql('DROP INDEX IDX_F176F56F5C7161DD ON file_field');
        $this->addSql('ALTER TABLE file_field DROP global_data_id');
        $this->addSql('DROP INDEX IDX_4CB0C1F15C7161DD ON image_field');
        $this->addSql('ALTER TABLE image_field DROP global_data_id');
        $this->addSql('DROP INDEX IDX_CD707B645C7161DD ON multiple_field');
        $this->addSql('ALTER TABLE multiple_field DROP global_data_id');
        $this->addSql('DROP INDEX IDX_D41FF055C7161DD ON text_field');
        $this->addSql('ALTER TABLE text_field DROP global_data_id');
    }
}
