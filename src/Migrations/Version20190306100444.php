<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190306100444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE email (id INT AUTO_INCREMENT NOT NULL, user_email_id INT DEFAULT NULL, type_id INT DEFAULT NULL, sent TINYINT(1) NOT NULL, template VARCHAR(256) NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', time_sending DATETIME DEFAULT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_E7927C7448BF25C9 (user_email_id), INDEX IDX_E7927C74C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C7448BF25C9 FOREIGN KEY (user_email_id) REFERENCES subscription_email (id)');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C74C54C8C93 FOREIGN KEY (type_id) REFERENCES subscription_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE email');
    }
}
