<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190417105743 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE text_field (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, global_data_id INT DEFAULT NULL, category_id INT DEFAULT NULL, article_id INT DEFAULT NULL, page_id INT DEFAULT NULL, value VARCHAR(512) DEFAULT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_D41FF05443707B0 (field_id), INDEX IDX_D41FF055C7161DD (global_data_id), INDEX IDX_D41FF0512469DE2 (category_id), INDEX IDX_D41FF057294869C (article_id), INDEX IDX_D41FF05C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF05443707B0 FOREIGN KEY (field_id) REFERENCES editable_field (id)');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF055C7161DD FOREIGN KEY (global_data_id) REFERENCES global_data (id)');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF0512469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF057294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF05C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE text_field');
    }
}
