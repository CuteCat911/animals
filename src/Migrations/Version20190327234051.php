<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190327234051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cms_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, server_name VARCHAR(128) NOT NULL, description VARCHAR(512) DEFAULT NULL, index_number INT NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_6CA2D53C5E237E06 (name), UNIQUE INDEX UNIQ_6CA2D53CD5A30FE6 (server_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content_field ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE content_field ADD CONSTRAINT FK_B036118AC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_B036118AC4663E4 ON content_field (page_id)');
        $this->addSql('ALTER TABLE file_field ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE file_field ADD CONSTRAINT FK_F176F56FC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_F176F56FC4663E4 ON file_field (page_id)');
        $this->addSql('ALTER TABLE image_field ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_field ADD CONSTRAINT FK_4CB0C1F1C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_4CB0C1F1C4663E4 ON image_field (page_id)');
        $this->addSql('ALTER TABLE multiple_field ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE multiple_field ADD CONSTRAINT FK_CD707B64C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_CD707B64C4663E4 ON multiple_field (page_id)');
        $this->addSql('ALTER TABLE text_field ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE text_field ADD CONSTRAINT FK_D41FF05C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_D41FF05C4663E4 ON text_field (page_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cms_category');
        $this->addSql('ALTER TABLE content_field DROP FOREIGN KEY FK_B036118AC4663E4');
        $this->addSql('DROP INDEX IDX_B036118AC4663E4 ON content_field');
        $this->addSql('ALTER TABLE content_field DROP page_id');
        $this->addSql('ALTER TABLE file_field DROP FOREIGN KEY FK_F176F56FC4663E4');
        $this->addSql('DROP INDEX IDX_F176F56FC4663E4 ON file_field');
        $this->addSql('ALTER TABLE file_field DROP page_id');
        $this->addSql('ALTER TABLE image_field DROP FOREIGN KEY FK_4CB0C1F1C4663E4');
        $this->addSql('DROP INDEX IDX_4CB0C1F1C4663E4 ON image_field');
        $this->addSql('ALTER TABLE image_field DROP page_id');
        $this->addSql('ALTER TABLE multiple_field DROP FOREIGN KEY FK_CD707B64C4663E4');
        $this->addSql('DROP INDEX IDX_CD707B64C4663E4 ON multiple_field');
        $this->addSql('ALTER TABLE multiple_field DROP page_id');
        $this->addSql('ALTER TABLE text_field DROP FOREIGN KEY FK_D41FF05C4663E4');
        $this->addSql('DROP INDEX IDX_D41FF05C4663E4 ON text_field');
        $this->addSql('ALTER TABLE text_field DROP page_id');
    }
}
