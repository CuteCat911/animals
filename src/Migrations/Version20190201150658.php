<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190201150658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_1DAA7D07E7927C74 ON subscription_email');
        $this->addSql('ALTER TABLE subscription_email ADD user_id INT DEFAULT NULL, DROP email');
        $this->addSql('ALTER TABLE subscription_email ADD CONSTRAINT FK_1DAA7D07A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1DAA7D07A76ED395 ON subscription_email (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription_email DROP FOREIGN KEY FK_1DAA7D07A76ED395');
        $this->addSql('DROP INDEX UNIQ_1DAA7D07A76ED395 ON subscription_email');
        $this->addSql('ALTER TABLE subscription_email ADD email VARCHAR(128) NOT NULL COLLATE utf8mb4_unicode_ci, DROP user_id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1DAA7D07E7927C74 ON subscription_email (email)');
    }
}
