<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190214055742 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE action_cms_user (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, code INT NOT NULL, event VARCHAR(128) NOT NULL, time DATETIME NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_9DD20B52A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action_cms_user ADD CONSTRAINT FK_9DD20B52A76ED395 FOREIGN KEY (user_id) REFERENCES cms_user (id)');
        $this->addSql('DROP TABLE event_cms_user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event_cms_user (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, code INT NOT NULL, event VARCHAR(128) NOT NULL COLLATE utf8mb4_unicode_ci, time DATETIME NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_CE992E4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE event_cms_user ADD CONSTRAINT FK_CE992E4A76ED395 FOREIGN KEY (user_id) REFERENCES cms_user (id)');
        $this->addSql('DROP TABLE action_cms_user');
    }
}
