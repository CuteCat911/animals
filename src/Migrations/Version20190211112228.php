<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190211112228 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cms_user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(32) NOT NULL, email VARCHAR(128) NOT NULL, password VARCHAR(64) NOT NULL, index_number INT NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', is_blocked TINYINT(1) NOT NULL, session_time INT NOT NULL, UNIQUE INDEX UNIQ_4A057B34AA08CB10 (login), UNIQUE INDEX UNIQ_4A057B34E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cms_user');
    }
}
