<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190123204249 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE content_field (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, value LONGTEXT DEFAULT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_B036118A443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_field (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, resource_path VARCHAR(256) DEFAULT NULL, resource_name VARCHAR(256) DEFAULT NULL, resource_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_F176F56F443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_field (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, resource_path VARCHAR(256) DEFAULT NULL, resource_name VARCHAR(256) DEFAULT NULL, resource_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_4CB0C1F1443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE multiple_field (id INT AUTO_INCREMENT NOT NULL, field_id INT DEFAULT NULL, value LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', structure LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, INDEX IDX_CD707B64443707B0 (field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content_field ADD CONSTRAINT FK_B036118A443707B0 FOREIGN KEY (field_id) REFERENCES editable_field (id)');
        $this->addSql('ALTER TABLE file_field ADD CONSTRAINT FK_F176F56F443707B0 FOREIGN KEY (field_id) REFERENCES editable_field (id)');
        $this->addSql('ALTER TABLE image_field ADD CONSTRAINT FK_4CB0C1F1443707B0 FOREIGN KEY (field_id) REFERENCES editable_field (id)');
        $this->addSql('ALTER TABLE multiple_field ADD CONSTRAINT FK_CD707B64443707B0 FOREIGN KEY (field_id) REFERENCES editable_field (id)');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_object');
        $this->addSql('ALTER TABLE text_field ADD value VARCHAR(512) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE category_object (id INT AUTO_INCREMENT NOT NULL, created_time DATETIME DEFAULT NULL, updated_time DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE content_field');
        $this->addSql('DROP TABLE file_field');
        $this->addSql('DROP TABLE image_field');
        $this->addSql('DROP TABLE multiple_field');
        $this->addSql('ALTER TABLE text_field DROP value');
    }
}
