<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190126131214 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE page ADD seo_title VARCHAR(256) DEFAULT NULL, ADD seo_description VARCHAR(512) DEFAULT NULL, ADD seo_keywords VARCHAR(512) DEFAULT NULL, ADD seo_image VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE category ADD seo_title VARCHAR(256) DEFAULT NULL, ADD seo_description VARCHAR(512) DEFAULT NULL, ADD seo_keywords VARCHAR(512) DEFAULT NULL, ADD seo_image VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE article ADD seo_title VARCHAR(256) DEFAULT NULL, ADD seo_description VARCHAR(512) DEFAULT NULL, ADD seo_keywords VARCHAR(512) DEFAULT NULL, ADD seo_image VARCHAR(256) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP seo_title, DROP seo_description, DROP seo_keywords, DROP seo_image');
        $this->addSql('ALTER TABLE category DROP seo_title, DROP seo_description, DROP seo_keywords, DROP seo_image');
        $this->addSql('ALTER TABLE page DROP seo_title, DROP seo_description, DROP seo_keywords, DROP seo_image');
    }
}
