<?php

namespace App\Entity\Traits;

trait SeoInfo
{

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $seoDescription;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $seoKeywords;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $seoImage;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $canonical;

    public function getSeoTitle(): ?string
    {
        return $this->seoTitle;
    }

    public function setSeoTitle(?string $title): ?self
    {
        $this->seoTitle = $title;
        return $this;
    }

    public function getSeoDescription(): ?string
    {
        return $this->seoDescription;
    }

    public function setSeoDescription(?string $description): ?self
    {
        $this->seoDescription = $description;
        return $this;
    }

    public function getSeoKeywords(): ?string
    {
        return $this->seoKeywords;
    }

    public function setSeoKeywords(?string $keywords): ?self
    {
        $this->seoKeywords = $keywords;
        return $this;
    }

    public function getSeoImage(): ?array
    {
        return $this->seoImage;
    }

    public function setSeoImage(?array $image): ?self
    {
        $this->seoImage = $image;
        return $this;
    }

    public function getCanonical(): ?string
    {
        return $this->canonical;
    }

    public function setCanonical(?string $url): ?self
    {
        $this->canonical = $url;
        return $this;
    }

    public function getSeoInfo()
    {
        return [
            'title' => $this->seoTitle,
            'description' => $this->seoDescription,
            'keywords' => $this->seoKeywords,
            'image' => $this->seoImage,
            'canonical' => $this->canonical
        ];
    }

}