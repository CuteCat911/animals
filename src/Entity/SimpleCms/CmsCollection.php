<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\App\Category;
use App\Entity\App\Article;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\CmsCollectionRepository")
 */
class CmsCollection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serverName;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Category", mappedBy="collections")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Article", mappedBy="collections")
     */
    private $articles;

    public function __construct()
    {
        $this->indexNumber = 0;
        $this->categories = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getIndexNumber(): ?int
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

    public function getCategories(): ?Collection
    {
        return $this->categories;
    }

    public function addCategory(?Category $category): ?self
    {

        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addCollection($this);
        }

        return $this;

    }

    public function removeCategory(?Category $category): ?self
    {

        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeCollection($this);
        }

        return $this;

    }

    public function getArticles(): ?Collection
    {
        return $this->articles;
    }

    public function addArticle(?Article $article): ?self
    {

        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->addCollection($this);
        }

        return $this;

    }

    public function removeArticle(?Article $article): ?self
    {

        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            $article->removeCollection($this);
        }

        return $this;

    }

    public function getItems(): ?array
    {

        $articles = [];

        foreach ($this->articles as $article) {
            $articles[] = $article->getJsonData();
        }

        $data = [
            'article' => [
                'count' => count($articles),
                'list' => $articles
            ]
        ];

        return $data;

    }

}
