<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\App\Article;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\ViewRepository")
 * @ORM\HasLifecycleCallbacks
 */
class View
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $userAgent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Article", inversedBy="views")
     */
    private $article;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function setUserAgent(?string $user_agent): ?self
    {
        $this->userAgent = $user_agent;
        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): ?self
    {
        $this->article = $article;
        return $this;
    }

}
