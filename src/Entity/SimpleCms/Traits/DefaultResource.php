<?php

namespace App\Entity\SimpleCms\Traits;

trait DefaultResource
{

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $serverName;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $breadcrumbName;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getBreadcrumbName(): ?string
    {
        return $this->breadcrumbName;
    }

    public function setBreadcrumbName(?string $name): ?self
    {
        $this->breadcrumbName = $name;
        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): ?self
    {
        $this->path = $path;
        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(?bool $status): ?self
    {
        $this->published = $status;
        return $this;
    }

    public function getIndexNumber(): ?int
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

}