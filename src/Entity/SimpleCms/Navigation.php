<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\NavigationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Navigation
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $serverName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\NavigationItem", mappedBy="navigation")
     */
    private $navigationItems;

    public function __construct()
    {
        $this->navigationItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getNavigationItems(): Collection
    {
        return $this->navigationItems;
    }

}
