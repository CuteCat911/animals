<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\ShareUserAgentImageRepository")
 */
class ShareUserAgentImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\ShareUserAgent", inversedBy="images")
     */
    private $userAgent;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\Page", mappedBy="shareImages")
     */
    private $pages;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Category", mappedBy="shareImages")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Article", mappedBy="shareImages")
     */
    private $articles;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): ?self
    {
        $this->path = $path;
        return $this;
    }

    public function getUserAgent(): ?ShareUserAgent
    {
        return $this->userAgent;
    }

    public function setUserAgent(?ShareUserAgent $user_agent): ?self
    {
        $this->userAgent = $user_agent;
        return $this;
    }

    public function getPages(): ?Collection
    {
        return $this->pages;
    }

    public function getCategories(): ?Collection
    {
        return $this->categories;
    }

    public function getArticles(): ?Collection
    {
        return $this->articles;
    }

}
