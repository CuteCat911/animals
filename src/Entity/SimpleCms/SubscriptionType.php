<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\SubscriptionTypeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class SubscriptionType
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serverName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $disabled;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\SubscriptionEmail", mappedBy="subscriptionTypes")
     */
    private $subscriptionEmails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\Email", mappedBy="type")
     */
    private $emails;

    public function __construct()
    {
        $this->indexNumber = 0;
        $this->disabled = false;
        $this->subscriptionEmails = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): ?self
    {
        $this->description = $description;
        return $this;
    }

    public function getIndexNumber(): ?int
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

    public function isDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $status): ?self
    {
        $this->disabled = $status;
        return $this;
    }

    public function getSubscriptionEmails(): ?Collection
    {
        return $this->subscriptionEmails;
    }

    public function addSubscriptionEmail(?SubscriptionEmail $email): ?self
    {

        if (!$this->subscriptionEmails->contains($email)) {
            $this->subscriptionEmails[] = $email;
        }

        return $this;

    }

    public function removeSubscriptionEmail(?SubscriptionEmail $email): ?self
    {

        if ($this->subscriptionEmails->contains($email)) {
            $this->subscriptionEmails->removeElement($email);
        }

        return $this;

    }

    public function getEmails(): ?Collection
    {
        return $this->emails;
    }

}
