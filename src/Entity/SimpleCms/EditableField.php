<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\EditableFieldRepository")
 * @ORM\HasLifecycleCallbacks
 */
class EditableField
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serverName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\EditableFieldType", inversedBy="editableFields")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\TextField", mappedBy="field")
     */
    private $textFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ContentField", mappedBy="field")
     */
    private $contentFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ImageField", mappedBy="field")
     */
    private $imageFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\FileField", mappedBy="field")
     */
    private $fileFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\MultipleField", mappedBy="field")
     */
    private $multipleFields;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $items;

    public function __construct()
    {
        $this->indexNumber = 0;
        $this->textFields = new ArrayCollection();
        $this->contentFields = new ArrayCollection();
        $this->imageFields = new ArrayCollection();
        $this->fileFields = new ArrayCollection();
        $this->multipleFields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): ?self
    {
        $this->description = $description;
        return $this;
    }

    public function getIndexNumber(): ?int
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

    public function getType(): ?EditableFieldType
    {
        return $this->type;
    }

    public function setType(?EditableFieldType $type): ?self
    {
        $this->type = $type;
        return $this;
    }

    public function getTextFields(): ?Collection
    {
        return $this->textFields;
    }

    public function getContentFields(): ?Collection
    {
        return $this->contentFields;
    }

    public function getImageFields(): ?Collection
    {
        return $this->imageFields;
    }

    public function getFileFields(): ?Collection
    {
        return $this->fileFields;
    }

    public function getMultipleFields(): ?Collection
    {
        return $this->multipleFields;
    }

    public function getFieldsData(): ?array
    {
        return [
            'text' => [
                'namespace' => 'App\Entity\SimpleCms\TextField',
                'methodsName' => [
                    'getAll' => 'getTextFields'
                ]
            ],
            'content' => [
                'namespace' => 'App\Entity\SimpleCms\ContentField',
                'methodsName' => [
                    'getAll' => 'getContentFields'
                ]
            ],
            'image' => [
                'namespace' => 'App\Entity\SimpleCms\ImageField',
                'methodsName' => [
                    'getAll' => 'getImageFields'
                ]
            ],
            'file' => [
                'namespace' => 'App\Entity\SimpleCms\FileField',
                'methodsName' => [
                    'getAll' => 'getFileFields'
                ]
            ],
            'multiple' => [
                'namespace' => 'App\Entity\SimpleCms\MultipleField',
                'methodsName' => [
                    'getAll' => 'getMultipleFields'
                ]
            ]
        ];
    }

    public function getItems(): ?array
    {
        return $this->items;
    }

    public function setItems(?array $items): ?self
    {
        $this->items = $items;
        return $this;
    }

    public function getJsonData(): ?array
    {

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'serverName' => $this->getServerName(),
            'description' => $this->getDescription(),
            'index' => $this->getIndexNumber(),
            'type' => $this->getType()->getId(),
            'fieldsData' => $this->getFieldsData(),
            'items' => $this->getItems()
        ];

    }

}
