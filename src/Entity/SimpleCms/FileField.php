<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\App\Category;
use App\Entity\App\Article;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\FileFieldRepository")
 * @ORM\HasLifecycleCallbacks
 */
class FileField
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $resourcePath;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $resourceName;

    /**
     * @ORM\Column(type="array")
     */
    private $resourceTypes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\EditableField", inversedBy="fileFields")
     */
    private $field;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\GlobalData", inversedBy="fileFields")
     */
    private $globalData;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Category", inversedBy="fileFields")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Article", inversedBy="fileFields")
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\Page", inversedBy="fileFields")
     */
    private $page;

    public function __construct()
    {
        $this->resourceTypes = ["all"];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResourcePath(): ?string
    {
        return $this->resourcePath;
    }

    public function setResourcePath(?string $path): ?self
    {
        $this->resourcePath = $path;
        return $this;
    }

    public function getResourceName(): ?string
    {
        return $this->resourceName;
    }

    public function setResourceName(?string $name): ?self
    {
        $this->resourceName = $name;
        return $this;
    }

    public function getResourceTypes(): ?array
    {
        return $this->resourceTypes;
    }

    public function setResourceTypes(?array $types): ?self
    {
        $this->resourceTypes = $types;
        return $this;
    }

    public function getField(): ?EditableField
    {
        return $this->field;
    }

    public function setField(?EditableField $field): ?self
    {
        $this->field = $field;
        return $this;
    }

    public function getGlobalData(): ?GlobalData
    {
        return $this->globalData;
    }

    public function setGlobalData(?GlobalData $data): ?self
    {
        $this->globalData = $data;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): ?self
    {
        $this->category = $category;
        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): ?self
    {
        $this->article = $article;
        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): ?self
    {
        $this->page = $page;
        return $this;
    }

    public function getJsonData(): ?array
    {
        return [
            'id' => $this->id,
            'path' => $this->resourcePath,
            'name' => $this->resourceName,
            'types' => $this->resourceTypes
        ];
    }

}
