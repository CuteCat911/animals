<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\GlobalDataRepository")
 */
class GlobalData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $siteName;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $siteLang;

    /**
     * @ORM\Column(type="boolean")
     */
    private $siteNameOnTitle;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $defaultShareImage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\TextField", mappedBy="globalData")
     */
    private $textFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ContentField", mappedBy="globalData")
     */
    private $contentFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ImageField", mappedBy="globalData")
     */
    private $imageFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\FileField", mappedBy="globalData")
     */
    private $fileFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\MultipleField", mappedBy="globalData")
     */
    private $multipleFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\Snippet", mappedBy="globalData")
     */
    private $snippets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ShareUserAgent", mappedBy="globalData")
     */
    private $shareUserAgents;

    public function __construct()
    {
        $this->siteNameOnTitle = false;
        $this->textFields = new ArrayCollection();
        $this->contentFields = new ArrayCollection();
        $this->imageFields = new ArrayCollection();
        $this->fileFields = new ArrayCollection();
        $this->multipleFields = new ArrayCollection();
        $this->snippets = new ArrayCollection();
        $this->shareUserAgents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(?string $name): ?self
    {
        $this->siteName = $name;
        return $this;
    }

    public function getSiteLang(): ?string
    {
        return $this->siteLang;
    }

    public function setSiteLang(?string $lang): ?self
    {
        $this->siteLang = $lang;
        return $this;
    }

    public function getSiteNameOnTitle(): ?bool
    {
        return $this->siteNameOnTitle;
    }

    public function setSiteNameOnTitle(?bool $status): ?self
    {
        $this->siteNameOnTitle = $status;
        return $this;
    }

    public function getDefaultShareImage(): ?string
    {
        return $this->defaultShareImage;
    }

    public function setDefaultShareImage(?string $image): ?self
    {
        $this->defaultShareImage = $image;
        return $this;
    }

    public function getTextFields(): ?Collection
    {
        return $this->textFields;
    }

    public function getContentFields(): ?Collection
    {
        return $this->contentFields;
    }

    public function getImageFields(): ?Collection
    {
        return $this->imageFields;
    }

    public function getFileFields(): ?Collection
    {
        return $this->fileFields;
    }

    public function getMultipleFields(): ?Collection
    {
        return $this->multipleFields;
    }

    public function getFields(): ?array
    {

        $fields_array = [];
        $fields = array_merge(
            $this->textFields->toArray(),
            $this->contentFields->toArray(),
            $this->imageFields->toArray(),
            $this->fileFields->toArray(),
            $this->multipleFields->toArray()
        );

        foreach ($fields as $field) {
            $fields_array[$field->getField()->getServerName()] = $field;
        }

        return $fields_array;

    }

    public function getSnippets(): ?Collection
    {
        return $this->snippets;
    }

    public function getShareUserAgents(): ?Collection
    {
        return $this->shareUserAgents;
    }

    public function getData(): ?array
    {
        return [
            'data' => $this,
            'fields' => $this->getFields(),
            'snippets' => $this->getSnippets()
        ];
    }

}
