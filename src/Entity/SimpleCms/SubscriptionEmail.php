<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\App\User;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\SubscriptionEmailRepository")
 */
class SubscriptionEmail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $subscribeEmail;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\App\User", inversedBy="subscribe")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\SubscriptionType", inversedBy="subscriptionEmails")
     */
    private $subscriptionTypes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\Email", mappedBy="userEmail")
     */
    private $emails;

    public function __construct()
    {
        $this->subscriptionTypes = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubscribeEmail(): ?string
    {
        return $this->subscribeEmail;
    }

    public function setSubscribeEmail(?string $email): ?self
    {
        $this->subscribeEmail = $email;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): ?self
    {
        $this->user = $user;
        return $this;
    }

    public function getSubscriptionTypes(): ?Collection
    {
        return $this->subscriptionTypes;
    }

    public function addSubscriptionType(?SubscriptionType $type): ?self
    {

        if (!$this->subscriptionTypes->contains($type)) {
            $this->subscriptionTypes[] = $type;
            $type->addSubscriptionEmail($this);
        }

        return $this;

    }

    public function removeSubscriptionType(?SubscriptionType $type): ?self
    {

        if ($this->subscriptionTypes->contains($type)) {
            $this->subscriptionTypes->removeElement($type);
            $type->removeSubscriptionEmail($this);
        }

        return $this;

    }

    public function getEmails(): ?Collection
    {
        return $this->emails;
    }

}
