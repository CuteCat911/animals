<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\SnippetRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Snippet
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serverName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="array")
     */
    private $variables;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\SnippetTemplate", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\GlobalData", inversedBy="snippets")
     */
    private $globalData;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): ?self
    {
        $this->content = $content;
        return $this;
    }

    public function getVariables(): ?array
    {
        return $this->variables;
    }

    public function setVariables(?array $variables): ?self
    {
        $this->variables = $variables;
        return $this;
    }

    public function getParent(): ?SnippetTemplate
    {
        return $this->parent;
    }

    public function setParent(?SnippetTemplate $parent): ?self
    {
        $this->parent = $parent;
        return $this;
    }

    public function getGlobalData(): ?GlobalData
    {
        return $this->globalData;
    }

    public function setGlobalData(?GlobalData $data): ?self
    {
        $this->globalData = $data;
        return $this;
    }

}
