<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\App\Category;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\NavigationItemRepository")
 * @ORM\HasLifecycleCallbacks
 */
class NavigationItem
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\Page", inversedBy="navigationItems")
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Category", inversedBy="navigationItems")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\Navigation", inversedBy="navigationItems")
     */
    private $navigation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getIndexNumber(): ?string
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): ?self
    {
        $this->page = $page;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): ?self
    {
        $this->category = $category;
        return $this;
    }

    public function getNavigation(): ?Navigation
    {
        return $this->navigation;
    }

    public function setNavigation(?Navigation $navigation): ?self
    {
        $this->navigation = $navigation;
        return $this;
    }

}
