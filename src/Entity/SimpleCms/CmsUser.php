<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\CmsUserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class CmsUser implements UserInterface, \Serializable
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32, unique=true)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBlocked;

    /**
     * @ORM\Column(type="integer")
     */
    private $sessionTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastActionTime;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ActionCmsUser", mappedBy="user")
     */
    private $actions;

    public function __construct()
    {
        $this->indexNumber = 0;
        $this->roles = ['ROLE_ADMIN'];
        $this->isBlocked = false;
        $this->sessionTime = 1200000;
        $this->actions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->login;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): ?self
    {
        $this->login = $login;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): ?self
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): ?self
    {
        $this->password = $password;
        return $this;
    }

    public function getIndexNumber(): ?int
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): ?self
    {
        $this->roles = $roles;
        return $this;
    }

    public function getIsBlocked(): ?bool
    {
        return $this->isBlocked;
    }

    public function setIsBlocked(?bool $status): ?self
    {
        $this->isBlocked = $status;
        return $this;
    }

    public function getSessionTime(): ?int
    {
        return $this->sessionTime;
    }

    public function setSessionTime(?int $time): ?self
    {
        $this->sessionTime = $time;
        return $this;
    }

    public function getLastActionTime()
    {
        return $this->lastActionTime;
    }

    public function setLastActionTime($time): ?self
    {
        $this->lastActionTime = $time;
        return $this;
    }

    public function eraseCredentials() {}

    public function getSalt()
    {
        return null;
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->login,
            $this->password
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->login,
            $this->password
            ) = unserialize($serialized);
    }

    public function getActions(): ?Collection
    {
        return $this->actions;
    }

}
