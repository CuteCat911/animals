<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\SnippetTemplateRepository")
 * @ORM\HasLifecycleCallbacks
 */
class SnippetTemplate
{

    const DEFAULT_IMAGE_DIR = 'storage/images/snippets';
    const DEFAULT_FILE_DIR = 'storage/files/snippets';

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serverName;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="integer")
     */
    private $indexNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $template;

    /**
     * @ORM\Column(type="array")
     */
    private $structure;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\Snippet", mappedBy="parent")
     */
    private $children;

    public function __construct()
    {
        $this->published = false;
        $this->indexNumber = 0;
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(?bool $status): ?self
    {
        $this->published = $status;
        return $this;
    }

    public function getIndexNumber(): ?int
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(?int $index): ?self
    {
        $this->indexNumber = $index;
        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(?string $template): ?self
    {
        $this->template = $template;
        return $this;
    }

    public function getStructure(): ?array
    {
        return $this->structure;
    }

    public function setStructure(?array $structure): ?self
    {
        $this->structure = $structure;
        return $this;
    }

    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    public function getDefaultDirs(): ?array
    {
        return [
            'image' => $this::DEFAULT_IMAGE_DIR,
            'file' => $this::DEFAULT_FILE_DIR
        ];
    }

    public function getJsonData(): ?array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'serverName' => $this->serverName,
            'template' => $this->template,
            'structure' => $this->structure
        ];
    }

}
