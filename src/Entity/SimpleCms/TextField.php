<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\App\Category;
use App\Entity\App\Article;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\TextFieldRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TextField
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\EditableField", inversedBy="textFields")
     */
    private $field;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\GlobalData", inversedBy="textFields")
     */
    private $globalData;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Category", inversedBy="textFields")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Article", inversedBy="textFields")
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\Page", inversedBy="textFields")
     */
    private $page;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): ?self
    {
        $this->value = $value;
        return $this;
    }

    public function getField(): ?EditableField
    {
        return $this->field;
    }

    public function setField(?EditableField $field): ?self
    {
        $this->field = $field;
        return $this;
    }

    public function getGlobalData(): ?GlobalData
    {
        return $this->globalData;
    }

    public function setGlobalData(?GlobalData $data): ?self
    {
        $this->globalData = $data;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): ?self
    {
        $this->category = $category;
        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): ?self
    {
        $this->article = $article;
        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): ?self
    {
        $this->page = $page;
        return $this;
    }

    public function getJsonData(): ?array
    {
        return [
            'id' => $this->id,
            'value' => $this->value
        ];
    }

}
