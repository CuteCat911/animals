<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\CollectionItemStructureRepository")
 */
class CollectionItemStructure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serverName;

    /**
     * @ORM\Column(type="array")
     */
    private $structure;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getServerName(): ?string
    {
        return $this->serverName;
    }

    public function setServerName(?string $name): ?self
    {
        $this->serverName = $name;
        return $this;
    }

    public function getStructure(): ?array
    {
        return $this->structure;
    }

    public function setStructure(?array $structure): ?self
    {
        $this->structure = $structure;
        return $this;
    }

}
