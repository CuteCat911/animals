<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\ActionCmsUserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ActionCmsUser
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $event;

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\CmsUser", inversedBy="actions")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(?int $code): ?self
    {
        $this->code = $code;
        return $this;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(?string $event): ?self
    {
        $this->event = $event;
        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time): ?self
    {
        $this->time = $time;
        return $this;
    }

    public function getUser(): ?CmsUser
    {
        return $this->user;
    }

    public function setUser(?CmsUser $user): ?self
    {
        $this->user = $user;
        return $this;
    }

}
