<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\PageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Page
{

    use \App\Entity\Traits\CreateUpdateTime;
    use \App\Entity\SimpleCms\Traits\DefaultResource;
    use \App\Entity\Traits\SeoInfo;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\Page", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\Page", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\TextField", mappedBy="page")
     */
    private $textFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ContentField", mappedBy="page")
     */
    private $contentFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ImageField", mappedBy="page")
     */
    private $imageFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\FileField", mappedBy="page")
     */
    private $fileFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\FileField", mappedBy="page")
     */
    private $multipleFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\NavigationItem", mappedBy="page")
     */
    private $navigationItems;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\ShareUserAgentImage", inversedBy="pages")
     */
    private $shareImages;

    public function __construct()
    {
        $this->published = false;
        $this->indexNumber = 0;
        $this->children = new ArrayCollection();
        $this->textFields = new ArrayCollection();
        $this->contentFields = new ArrayCollection();
        $this->imageFields = new ArrayCollection();
        $this->fileFields = new ArrayCollection();
        $this->multipleFields = new ArrayCollection();
        $this->navigationItems = new ArrayCollection();
        $this->shareImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    public function getParent(): ?Page
    {
        return $this->parent;
    }

    public function setParent(?Page $parent): ?self
    {
        $this->parent = $parent;
        return $this;
    }

    public function getTextFields(): ?Collection
    {
        return $this->textFields;
    }

    public function getContentFields(): ?Collection
    {
        return $this->contentFields;
    }

    public function getImageFields(): ?Collection
    {
        return $this->imageFields;
    }

    public function getFileFields(): ?Collection
    {
        return $this->fileFields;
    }

    public function getMultipleFields(): ?Collection
    {
        return $this->multipleFields;
    }

    public function getFields(): ?array
    {

        $fields_array = [];
        $fields = array_merge(
            $this->textFields->toArray(),
            $this->contentFields->toArray(),
            $this->imageFields->toArray(),
            $this->fileFields->toArray(),
            $this->multipleFields->toArray()
        );

        foreach ($fields as $field) {
            $fields_array[$field->getField()->getServerName()] = $field;
        }

        return $fields_array;

    }

    public function getNavigationItems(): ?Collection
    {
        return $this->navigationItems;
    }

    public function getShareImages(): ?Collection
    {
        return $this->shareImages;
    }

}
