<?php

namespace App\Entity\SimpleCms;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SimpleCms\EmailRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Email
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\EmailTemplate", inversedBy="emails")
     */
    private $template;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $timeSending;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\SubscriptionEmail", inversedBy="emails")
     */
    private $userEmail;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SimpleCms\SubscriptionType", inversedBy="emails")
     */
    private $type;

    public function __construct()
    {
        $this->sent = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isSent(): ?bool
    {
        return $this->sent;
    }

    public function setSent(?bool $status): ?self
    {
        $this->sent = $status;
        return $this;
    }

    public function getTemplate(): ?EmailTemplate
    {
        return $this->template;
    }

    public function setTemplate(?EmailTemplate $template): ?self
    {
        $this->template = $template;
        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(?array $data): ?self
    {
        $this->data = $data;
        return $this;
    }

    public function getTimeSending()
    {
        return $this->timeSending;
    }

    public function setTimeSending($time): ?self
    {
        $this->timeSending = $time;
        return $this;
    }

    public function getUserEmail(): ?SubscriptionEmail
    {
        return $this->userEmail;
    }

    public function setUserEmail(?SubscriptionEmail $email): ?self
    {
        $this->userEmail = $email;
        return $this;
    }

    public function getType(): ?SubscriptionType
    {
        return $this->type;
    }

    public function setType(?SubscriptionType $type): ?self
    {
        $this->type = $type;
        return $this;
    }

}
