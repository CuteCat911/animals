<?php

namespace App\Entity\App;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\SimpleCms\SubscriptionEmail;

/**
 * @ORM\Entity(repositoryClass="App\Repository\App\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, \Serializable
{

    use \App\Entity\Traits\CreateUpdateTime;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32, unique=true, nullable=true)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $patronymic;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBlocked;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SimpleCms\SubscriptionEmail", mappedBy="user")
     */
    private $subscribe;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Article", inversedBy="favoritesUsers")
     */
    private $favoritesArticles;

    public function __construct()
    {
        $this->isActive = false;
        $this->isBlocked = false;
        $this->roles = ['ROLE_DEFAULT_USER'];
        $this->favoritesArticles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): ?self
    {
        $this->login = $login;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): ?self
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): ?self
    {
        $this->password = $password;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ?self
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): ?self
    {
        $this->surname = $surname;
        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(?string $patronymic): ?self
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $image): ?self
    {
        $this->avatar = $image;
        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $status): ?self
    {
        $this->isActive = $status;
        return $this;
    }

    public function getIsBlocked(): ?bool
    {
        return $this->isBlocked;
    }

    public function setIsBlocked(?bool $status): ?self
    {
        $this->isBlocked = $status;
        return $this;
    }

    public function getSubscribe(): ?SubscriptionEmail
    {
        return $this->subscribe;
    }

    public function setSubscribe(?SubscriptionEmail $subscribe): ?self
    {
        $this->subscribe = $subscribe;
        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): ?self
    {
        $this->roles = $roles;
        return $this;
    }

    public function getFavoritesArticles (): ?Collection
    {
        return $this->favoritesArticles;
    }

    public function addFavoriteArticle(?Article $article): ?self
    {

        if (!$this->favoritesArticles->contains($article)) {
            $this->favoritesArticles[] = $article;
        }

        return $this;

    }

    public function removeFavoriteArticle(?Article $article): ?self
    {

        if ($this->favoritesArticles->contains($article)) {
            $this->favoritesArticles->removeElement($article);
        }

        return $this;

    }

    public function eraseCredentials() {}

    public function getSalt()
    {
        return null;
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password
        ]);
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password
            ) = unserialize($serialized);
    }

}
