<?php

namespace App\Entity\App;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\SimpleCms\CmsCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\App\ArticleRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Article
{

    use \App\Entity\Traits\CreateUpdateTime;
    use \App\Entity\SimpleCms\Traits\DefaultResource;
    use \App\Entity\Traits\SeoInfo;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publicationTime;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Category", inversedBy="articles")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\Tag", mappedBy="articles")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\View", mappedBy="article")
     */
    private $views;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\User", mappedBy="favoritesArticles")
     */
    private $favoritesUsers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\TextField", mappedBy="article")
     */
    private $textFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ContentField", mappedBy="article")
     */
    private $contentFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ImageField", mappedBy="article")
     */
    private $imageFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\FileField", mappedBy="article")
     */
    private $fileFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\MultipleField", mappedBy="article")
     */
    private $multipleFields;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\ShareUserAgentImage", inversedBy="articles")
     */
    private $shareImages;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\CmsCollection", inversedBy="articles")
     */
    private $collections;

    public function __construct()
    {
        $this->published = false;
        $this->indexNumber = 0;
        $this->categories = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->views = new ArrayCollection();
        $this->favoritesUsers = new ArrayCollection();
        $this->textFields = new ArrayCollection();
        $this->contentFields = new ArrayCollection();
        $this->imageFields = new ArrayCollection();
        $this->fileFields = new ArrayCollection();
        $this->multipleFields = new ArrayCollection();
        $this->shareImages = new ArrayCollection();
        $this->collections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublicationTime()
    {
        return $this->publicationTime;
    }

    public function setPublicationTime($time): ?self
    {
        $this->publicationTime = $time;
        return $this;
    }

    public function getCategories(): ?Collection
    {
        return $this->categories;
    }

    public function addCategory(?Category $category): ?self
    {

        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addArticle($this);
        }

        return $this;

    }

    public function removeCategory(?Category $category): ?self
    {

        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeArticle($this);
        }

        return $this;

    }

    public function getTags(): ?Collection
    {
        return $this->tags;
    }

    public function getViews(): ?Collection
    {
        return $this->views;
    }

    public function getFavoritesUsers(): ?Collection
    {
        return $this->favoritesUsers;
    }

    public function getTextFields(): ?Collection
    {
        return $this->textFields;
    }

    public function getContentFields(): ?Collection
    {
        return $this->contentFields;
    }

    public function getImageFields(): ?Collection
    {
        return $this->imageFields;
    }

    public function getFileFields(): ?Collection
    {
        return $this->fileFields;
    }

    public function getMultipleFields(): ?Collection
    {
        return $this->multipleFields;
    }

    public function getFields(): ?array
    {

        $fields_array = [];
        $fields = array_merge(
            $this->textFields->toArray(),
            $this->contentFields->toArray(),
            $this->imageFields->toArray(),
            $this->fileFields->toArray(),
            $this->multipleFields->toArray()
        );

        foreach ($fields as $field) {
            $fields_array[$field->getField()->getServerName()] = $field;
        }

        return $fields_array;

    }

    public function getJsonFields(): ?array
    {

        $fields_array = [];
        $fields = array_merge(
            $this->textFields->toArray(),
            $this->contentFields->toArray(),
            $this->imageFields->toArray(),
            $this->fileFields->toArray(),
            $this->multipleFields->toArray()
        );

        foreach ($fields as $field) {
            $fields_array[$field->getField()->getServerName()] = $field->getJsonData();
        }

        return $fields_array;

    }

    public function getShareImages(): ?Collection
    {
        return $this->shareImages;
    }

    public function getCollections(): ?Collection
    {
        return $this->collections;
    }

    public function addCollection(?CmsCollection $collection): ?self
    {

        if (!$this->collections->contains($collection)) {
            $this->collections[] = $collection;
            $collection->addArticle($this);
        }

        return $this;

    }

    public function removeCollection(?CmsCollection $collection): ?self
    {

        if ($this->collections->contains($collection)) {
            $this->collections->removeElement($collection);
            $collection->removeArticle($this);
        }

        return $this;

    }

    public function getJsonData($from = false): ?array
    {

        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'serverName' => $this->serverName,
            'breadcrumbName' => $this->breadcrumbName,
            'path' => $this->path,
            'published' => $this->published,
            'index' => $this->indexNumber,
            'fields' => $this->getJsonFields(),
            'views' => count($this->views)
        ];

        return $data;

    }

}
