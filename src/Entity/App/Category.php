<?php

namespace App\Entity\App;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Entity\SimpleCms\CmsCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\App\CategoryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Category
{

    use \App\Entity\Traits\CreateUpdateTime;
    use \App\Entity\SimpleCms\Traits\DefaultResource;
    use \App\Entity\Traits\SeoInfo;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\App\Category", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\App\Category", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\App\Article", mappedBy="categories")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\TextField", mappedBy="category")
     */
    private $textFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ContentField", mappedBy="category")
     */
    private $contentFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\ImageField", mappedBy="category")
     */
    private $imageFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\FileField", mappedBy="category")
     */
    private $fileFields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\MultipleField", mappedBy="category")
     */
    private $multipleFields;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\ShareUserAgentImage", inversedBy="categories")
     */
    private $shareImages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SimpleCms\NavigationItem", mappedBy="category")
     */
    private $navigationItems;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SimpleCms\CmsCollection", inversedBy="categories")
     */
    private $collections;

    public function __construct()
    {
        $this->published = false;
        $this->indexNumber = 0;
        $this->children = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->textFields = new ArrayCollection();
        $this->contentFields = new ArrayCollection();
        $this->imageFields = new ArrayCollection();
        $this->fileFields = new ArrayCollection();
        $this->multipleFields = new ArrayCollection();
        $this->shareImages = new ArrayCollection();
        $this->navigationItems;
        $this->collections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    public function getParent(): ?Category
    {
        return $this->parent;
    }

    public function setParent(?Category $parent): ?self
    {
        $this->parent = $parent;
        return $this;
    }

    public function getArticles(): ?Collection
    {
        return $this->articles;
    }

    public function addArticle(?Article $article): ?self
    {

        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->addCategory($this);
        }

        return $this;

    }

    public function removeArticle(?Article $article): ?self
    {

        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            $article->removeCategory($this);
        }

        return $this;

    }

    public function getTextFields(): ?Collection
    {
        return $this->textFields;
    }

    public function getContentFields(): ?Collection
    {
        return $this->contentFields;
    }

    public function getImageFields(): ?Collection
    {
        return $this->imageFields;
    }

    public function getFileFields(): ?Collection
    {
        return $this->fileFields;
    }

    public function getMultipleFields(): ?Collection
    {
        return $this->multipleFields;
    }

    public function getFields(): ?array
    {

        $fields_array = [];
        $fields = array_merge(
            $this->textFields->toArray(),
            $this->contentFields->toArray(),
            $this->imageFields->toArray(),
            $this->fileFields->toArray(),
            $this->multipleFields->toArray()
        );

        foreach ($fields as $field) {
            $fields_array[$field->getField()->getServerName()] = $field;
        }

        return $fields_array;

    }

    public function getShareImages(): ?Collection
    {
        return $this->shareImages;
    }

    public function getNavigationItems(): ?Collection
    {
        return $this->navigationItems;
    }

    public function getCollections(): ?Collection
    {
        return $this->collections;
    }

    public function addCollection(?CmsCollection $collection): ?self
    {

        if (!$this->collections->contains($collection)) {
            $this->collections[] = $collection;
            $collection->addCategory($this);
        }

        return $this;

    }

    public function removeCollection(?CmsCollection $collection): ?self
    {

        if ($this->collections->contains($collection)) {
            $this->collections->removeElement($collection);
            $collection->removeCategory($this);
        }

        return $this;

    }

    public function getJsonData($from = false): ?array
    {

        $children_data = [];
        $articles_data = [];

        foreach ($this->children as $child) {
            $children_data[] = $child->getJsonData($from);
        }

        if ($from != 'article') {
            foreach ($this->articles as $article) {
                $articles_data[] = $article->getJsonData('category');
            }
        }

        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'serverName' => $this->serverName,
            'breadcrumbName' => $this->breadcrumbName,
            'path' => $this->path,
            'published' => $this->published,
            'index' => $this->indexNumber,
            'fields' => $this->getFields(),
            'parent' => $this->parent ? $this->parent->getJsonData($from) : null,
            'children' => $children_data
        ];

        if ($from != 'article') $data['articles'] = $articles_data;

        return $data;

    }

}
