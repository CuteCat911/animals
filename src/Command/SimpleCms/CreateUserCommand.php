<?php

namespace App\Command\SimpleCms;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\SimpleCms\CmsUser;

class CreateUserCommand extends ContainerAwareCommand
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('create_cms_user')
            ->addArgument('login', InputArgument::REQUIRED)
            ->addArgument('email', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::REQUIRED)
            ->addArgument('roles', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $login = $input->getArgument('login');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $roles = $input->getArgument('roles');

        if (!$login || !$email || !$password || !$roles) {
            $output->writeln('Missing login or email or password or roles');
            return false;
        }

        $check_login = preg_match('/[^а-яёА-ЯЁ]{3,}/i', $login);

        if (!$check_login) {
            $output->writeln('Not valid login');
            return false;
        }

        $check_email = preg_match('/.+@.+\..+/i', $email);

        if (!$check_email) {
            $output->writeln('Not valid email');
            return false;
        }

        $check_password = mb_strlen($password) >= 6;

        if (!$check_password) {
            $output->writeln('Not valid password');
            return false;
        }

        $old_user = $em->getRepository(CmsUser::class)->findOneBy(['login' => $login]);

        if ($old_user) {
            $output->writeln('User with such login was already registered');
            return false;
        }

        $old_user = $em->getRepository(CmsUser::class)->findOneBy(['email' => $email]);

        if ($old_user) {
            $output->writeln('User with such email was already registered');
            return false;
        }

        $new_user = new CmsUser();
        $encoded_password = $this->encoder->encodePassword($new_user, $password);
        $new_user->setLogin($login)->setEmail($email)->setPassword($encoded_password)->setRoles(explode(',', $roles));
        $em->persist($new_user);
        $em->flush();
        $output->writeln('User with ' . $login . ' has been registered');
        return true;

    }

}