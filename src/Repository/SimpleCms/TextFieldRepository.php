<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\TextField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TextField|null find($id, $lockMode = null, $lockVersion = null)
 * @method TextField|null findOneBy(array $criteria, array $orderBy = null)
 * @method TextField[]    findAll()
 * @method TextField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TextFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TextField::class);
    }

    // /**
    //  * @return TextField[] Returns an array of TextField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TextField
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
