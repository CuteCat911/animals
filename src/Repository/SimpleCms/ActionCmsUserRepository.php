<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\ActionCmsUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ActionCmsUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActionCmsUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActionCmsUser[]    findAll()
 * @method ActionCmsUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActionCmsUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ActionCmsUser::class);
    }

    // /**
    //  * @return ActionCmsUser[] Returns an array of ActionCmsUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActionCmsUser
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
