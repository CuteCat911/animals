<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\FileField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FileField|null find($id, $lockMode = null, $lockVersion = null)
 * @method FileField|null findOneBy(array $criteria, array $orderBy = null)
 * @method FileField[]    findAll()
 * @method FileField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FileField::class);
    }

    // /**
    //  * @return FileField[] Returns an array of FileField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FileField
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
