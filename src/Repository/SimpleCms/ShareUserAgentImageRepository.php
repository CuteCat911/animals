<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\ShareUserAgentImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShareUserAgentImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShareUserAgentImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShareUserAgentImage[]    findAll()
 * @method ShareUserAgentImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShareUserAgentImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShareUserAgentImage::class);
    }

    // /**
    //  * @return ShareUserAgentImage[] Returns an array of ShareUserAgentImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShareUserAgentImage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
