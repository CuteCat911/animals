<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\ContentField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContentField|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContentField|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContentField[]    findAll()
 * @method ContentField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContentField::class);
    }

    // /**
    //  * @return ContentField[] Returns an array of ContentField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContentField
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
