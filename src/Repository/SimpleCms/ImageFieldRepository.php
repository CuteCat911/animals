<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\ImageField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ImageField|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageField|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageField[]    findAll()
 * @method ImageField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ImageField::class);
    }

    // /**
    //  * @return ImageField[] Returns an array of ImageField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImageField
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
