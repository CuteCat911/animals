<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\SnippetTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SnippetTemplate|null find($id, $lockMode = null, $lockVersion = null)
 * @method SnippetTemplate|null findOneBy(array $criteria, array $orderBy = null)
 * @method SnippetTemplate[]    findAll()
 * @method SnippetTemplate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SnippetTemplateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SnippetTemplate::class);
    }

    // /**
    //  * @return SnippetTemplate[] Returns an array of SnippetTemplate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SnippetTemplate
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
