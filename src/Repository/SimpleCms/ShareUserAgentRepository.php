<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\ShareUserAgent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShareUserAgent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShareUserAgent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShareUserAgent[]    findAll()
 * @method ShareUserAgent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShareUserAgentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShareUserAgent::class);
    }

    // /**
    //  * @return ShareUserAgent[] Returns an array of ShareUserAgent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShareUserAgent
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
