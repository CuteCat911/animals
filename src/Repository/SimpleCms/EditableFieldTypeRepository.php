<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\EditableFieldType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EditableFieldType|null find($id, $lockMode = null, $lockVersion = null)
 * @method EditableFieldType|null findOneBy(array $criteria, array $orderBy = null)
 * @method EditableFieldType[]    findAll()
 * @method EditableFieldType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EditableFieldTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EditableFieldType::class);
    }

    // /**
    //  * @return EditableFieldType[] Returns an array of EditableFieldType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EditableFieldType
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
