<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\SimpleManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SimpleManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method SimpleManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method SimpleManager[]    findAll()
 * @method SimpleManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SimpleManagerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SimpleManager::class);
    }

    // /**
    //  * @return SimpleManager[] Returns an array of SimpleManager objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SimpleManager
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
