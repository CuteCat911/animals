<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\CollectionItemStructure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CollectionItemStructure|null find($id, $lockMode = null, $lockVersion = null)
 * @method CollectionItemStructure|null findOneBy(array $criteria, array $orderBy = null)
 * @method CollectionItemStructure[]    findAll()
 * @method CollectionItemStructure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollectionItemStructureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CollectionItemStructure::class);
    }

    // /**
    //  * @return CollectionItemStructure[] Returns an array of CollectionItemStructure objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CollectionItemStructure
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
