<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\CmsCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CmsCollection|null find($id, $lockMode = null, $lockVersion = null)
 * @method CmsCollection|null findOneBy(array $criteria, array $orderBy = null)
 * @method CmsCollection[]    findAll()
 * @method CmsCollection[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CmsCollectionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CmsCollection::class);
    }

    // /**
    //  * @return CmsCollection[] Returns an array of CmsCollection objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CmsCollection
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
