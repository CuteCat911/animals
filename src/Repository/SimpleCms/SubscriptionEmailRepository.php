<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\SubscriptionEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubscriptionEmail|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubscriptionEmail|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubscriptionEmail[]    findAll()
 * @method SubscriptionEmail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionEmailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SubscriptionEmail::class);
    }

    // /**
    //  * @return SubscriptionEmail[] Returns an array of SubscriptionEmail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubscriptionEmail
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
