<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\MultipleField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MultipleField|null find($id, $lockMode = null, $lockVersion = null)
 * @method MultipleField|null findOneBy(array $criteria, array $orderBy = null)
 * @method MultipleField[]    findAll()
 * @method MultipleField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MultipleFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MultipleField::class);
    }

    // /**
    //  * @return MultipleField[] Returns an array of MultipleField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MultipleField
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
