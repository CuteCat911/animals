<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\EditableField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EditableField|null find($id, $lockMode = null, $lockVersion = null)
 * @method EditableField|null findOneBy(array $criteria, array $orderBy = null)
 * @method EditableField[]    findAll()
 * @method EditableField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EditableFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EditableField::class);
    }

    // /**
    //  * @return EditableField[] Returns an array of EditableField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EditableField
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
