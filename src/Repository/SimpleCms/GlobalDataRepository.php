<?php

namespace App\Repository\SimpleCms;

use App\Entity\SimpleCms\GlobalData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GlobalData|null find($id, $lockMode = null, $lockVersion = null)
 * @method GlobalData|null findOneBy(array $criteria, array $orderBy = null)
 * @method GlobalData[]    findAll()
 * @method GlobalData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GlobalDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GlobalData::class);
    }

    // /**
    //  * @return GlobalData[] Returns an array of GlobalData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GlobalData
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
