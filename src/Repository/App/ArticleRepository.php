<?php

namespace App\Repository\App;

use App\Entity\App\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function getArticlesOnMainPage($page)
    {

        $result = $this->createQueryBuilder('a')
            ->andWhere('a.published = true')
            ->orderBy('a.publicationTime', 'DESC')
            ->getQuery()
            ->getResult();

        if ($page == 1) {
            return array_slice($result, $page - 1, 21);
        } else {
            return array_slice($result, 21 + (20 * ($page - 2)), 20);
        }

    }

    public function getArticlesOnPage($category, $page)
    {
        $result = $this->createQueryBuilder('a')
            ->join('a.categories', 'categories')
            ->andWhere('categories = :cat')
            ->andWhere('a.published = true')
            ->setParameter('cat', $category)
            ->orderBy('a.publicationTime', 'DESC')
            ->getQuery()
            ->getResult();

        if ($page == 1) {
            return array_slice($result, $page - 1, 21);
        } else {
            return array_slice($result, 21 * ($page - 1), 21);
        }

    }

    public function getPopularArticles()
    {

        return $this->createQueryBuilder('a')
            ->join('a.views', 'v')
            ->andWhere('a.published = true')
            ->orderBy('COUNT(v)', 'DESC')
            ->addGroupBy('a')
            ->setMaxResults(7)
            ->getQuery()
            ->getResult();

    }

    public function findByCategory($category)
    {

        $result = $this->createQueryBuilder('a')
            ->join('a.categories', 'categories')
            ->andWhere('categories = :cat')
            ->andWhere('a.published = true')
            ->setParameter('cat', $category)
            ->getQuery()
            ->getResult();

        return $result;

    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
