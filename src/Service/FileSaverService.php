<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileSaverService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function saveFile(?UploadedFile $file, $file_dir): ?string
    {

        $new_file_name = $this->getHash($file->getBasename()) . '.' . $file->getClientOriginalExtension();
        $file->move($file_dir, $new_file_name);
        $uri = DIRECTORY_SEPARATOR . $file_dir . DIRECTORY_SEPARATOR . $new_file_name;

        return $uri;

    }

    public function getHash($name)
    {
        return md5($name . time() . microtime(true));
    }

}