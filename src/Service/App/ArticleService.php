<?php

namespace App\Service\App;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\App\Article;

class ArticleService
{

    private $em;
    private $random_articles_array;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->random_articles_array = [];
    }

    public function getRandomArticles($count, $category = false)
    {

        if (!$count) {
            return false;
        }

        $articles_repository = $this->em->getRepository(Article::class);
        $articles = $category ? $articles_repository->findByCategory($category) : $articles_repository->findAll();
        $articles_count = count($articles);

        if ($count > $articles_count) $count = $articles_count;

        $article_index = rand(0, $articles_count - 1);

        while (count($this->random_articles_array) < $count) {

            if (!in_array($article_index, $this->random_articles_array)) {
                $this->random_articles_array[] = $article_index;
            }

            $article_index = rand(0, $count);

        }

        return array_filter($articles, function($value, $key) {

            if (in_array($key, $this->random_articles_array)) {
                return true;
            } else {
                return false;
            }

        }, ARRAY_FILTER_USE_BOTH);

    }

}