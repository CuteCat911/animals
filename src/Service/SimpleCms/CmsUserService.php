<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\CmsUser;
use App\Entity\SimpleCms\ActionCmsUser;

class CmsUserService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function checkSession(?CmsUser $user)
    {

        if (!$user) {
            return false;
        }

        $session_time = $user->getSessionTime();
        $now_time = new \DateTime();
        $last_action_time = $user->getLastActionTime();
        $diff = $now_time->diff($last_action_time);
        $milliseconds_diff = ($diff->y * 365 * 24 * 60 * 60 +
            $diff->m * 30 * 24 * 60 * 60 +
            $diff->d * 24 * 60 * 60 +
            $diff->h * 60 * 60 +
            $diff->i * 60 +
            $diff->s) * 1000;

        return $milliseconds_diff < $session_time;

    }

    public function addAction(?CmsUser $user, ?array $action)
    {

        if (!$user || !$action || !isset($action['code']) || !isset($action['event'])) {
            return false;
        }

        $time = new \DateTime(preg_replace('/\s\(.*\)/i', '', $action['time']));
        $new_action = new ActionCmsUser();
        $new_action->setCode($action['code'])->setEvent($action['event'])->setTime($time)->setUser($user);
        $this->em->persist($new_action);
        $user->setLastActionTime($time);
        $this->em->flush();

        return true;

    }

}