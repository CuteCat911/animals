<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\View;

class ViewService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createView($user_agent, $article)
    {

        // TODO поменять на универсальный entity

        if (!$user_agent || !$article) {
            return false;
        }

        $view = new View();
        $view->setUserAgent($user_agent);
        $view->setArticle($article);
        $this->em->persist($view);
        $this->em->flush();

        return true;

    }

}