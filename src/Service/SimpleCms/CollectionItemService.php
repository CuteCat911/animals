<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\CmsCollection;
use App\Entity\SimpleCms\EditableField;
use App\Service\FileSaverService;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CollectionItemService
{

    private $em;
    private $file_saver;

    public function __construct(EntityManagerInterface $em, FileSaverService $file_saver)
    {
        $this->em = $em;
        $this->file_saver = $file_saver;
    }

    public function checkItemData($data, $response, $item_id = false, $collection_item = false)
    {

        if (!isset($data)) return $response->setMessage('1')->json();
        if (!property_exists($data, 'collectionId') || empty($data->collectionId)) return $response->setMessage('')->json();
        if (!property_exists($data, 'type') || empty($data->type)) return $response->setMessage('')->json();
        if (!$item_id && $collection_item) return $response->setMessage('2')->json();

        $collection = $this->em->getRepository(CmsCollection::class)->find($data->collectionId);

        if (empty($collection)) return $response->setMessage('3')->json();

        if ($collection_item) {
            $entity_namespace = $collection_item['namespace'];
            $entity = $this->em->getRepository($entity_namespace)->find($item_id);

            if (empty($entity)) return $response->setMessage('4')->json();
        }

        return true;

    }

    public function setItemData($request, $response, $method_name, $collection_item_info, $entity)
    {

        $is_simple_form = !($request->request->get('general') && $request->request->get('seo'));

        if ($is_simple_form) {

        } else {

            $general_data = json_decode($request->request->get('general'));
            $seo_data = json_decode($request->request->get('seo'));
            $other_data = json_decode($request->request->get('other'));
            $collection_id = json_decode($request->request->get('item'))->collectionId;

            $name = property_exists($general_data, 'name') ? $general_data->name : false;
            $path = property_exists($general_data, 'path') ? $general_data->path : false;
            $breadcrumb_name = property_exists($general_data, 'breadcrumbName') ? $general_data->breadcrumbName : false;
            $published = property_exists($general_data, 'published') ? $general_data->published : false;
            $publication_time = property_exists($general_data, 'publicationTime') ? $general_data->publicationTime : false;
            $category = property_exists($general_data, 'category') ? $general_data->category : false;
            $seo_title = property_exists($seo_data, 'title') ? $seo_data->title : false;
            $seo_keys = property_exists($seo_data, 'keys') ? $seo_data->keys : false;
            $seo_description = property_exists($seo_data, 'description') ? $seo_data->description : false;
            $seo_image = property_exists($seo_data, 'image') ? $seo_data->image : false;

            if (!$name || !$path || !$publication_time) {

                $errors = [];

                if (!$name) $errors['name'] = [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'empty'
                ];

                if (!$path) $errors['path'] = [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'empty'
                ];

                if (!$publication_time) $errors['publicationTime'] = [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'empty'
                ];

                return $response->setErrors($errors)->json();

            }

            $entity
                ->setName($name)
                ->setPath($path)
                ->setPublicationTime(new \DateTime(preg_replace('/\s\(.*\)/i', '', $publication_time)))
                ->setPublished(!!$published)
            ;

            $categories_info = $collection_item_info['category'];
            $collection  = $this->em->getRepository(CmsCollection::class)->find($collection_id);
            $add_entity_methods_name = $collection_item_info['methodsName']['add'];
            $remove_entity_methods_name = $collection_item_info['methodsName']['remove'];

            if ($category) {

                foreach ($categories_info as $category_info) {

                    foreach ($category as $category_id) {

                        $category_entity_namespace = $category_info['namespace'];
                        $category_entity = $this->em->getRepository($category_entity_namespace)->find($category_id);

                        if (!$category_entity) continue;

                        $add_entity_methods_name = $category_info['methodsName']['add'];
                        $category_entity->$add_entity_methods_name($entity);

                    }

                }

            }

            if ($collection) {

                if ($category) {
                    $collection->$remove_entity_methods_name($entity);
                } else {
                    $collection->$add_entity_methods_name($entity);
                }

            }

            $apply_categories = $entity->getCategories();

            foreach ($categories_info as $category_info) {

                foreach ($apply_categories as $apply_category) {

                    $apply_category_class = get_class($apply_category);
                    $category_entity_namespace = $category_info['namespace'];

                    if ($apply_category_class !== $category_entity_namespace) continue;

                    $apply_category_id = $apply_category->getId();

                    if (in_array($apply_category_id, $category)) continue;

                    $remove_entity_methods_name = $category_info['methodsName']['remove'];

                    $apply_category->$remove_entity_methods_name($entity);

                }

            }

            if ($breadcrumb_name) $entity->setBreadcrumbName($breadcrumb_name);
            if ($seo_title) $entity->setSeoTitle($seo_title);
            if ($seo_keys) $entity->setSeoKeys($seo_keys);
            if ($seo_description) $entity->setSeoDescription($seo_description);
//            if ($seo_image) $entity->setSeoImage($seo_image);

            if ($other_data) {
                foreach ($other_data as $key=>$item) {

                    $item = json_decode($item);

                    if (!$item->fieldData->fieldItemId) break;

                    $editable_field = $this->em->getRepository(EditableField::class)->find($item->fieldData->fieldId);

                    if (!$editable_field) break;

                    $fields_data = $editable_field->getFieldsData();
                    $field_namespace = $fields_data[$item->type]['namespace'];
                    $field = $this->em->getRepository($field_namespace)->find($item->fieldData->fieldItemId);

                    if (!$field) break;

                    if ($item->type != 'image') {
                        $field->setValue($item->value);
                    } else {

                        $file = $request->files->get($item->type . '-' . $key);

                        if (!$file || !$file instanceof UploadedFile) continue;

                        $uri = $this->file_saver->saveFile($file, $field::DEFAULT_DIR);

                        if ($item->value) $field->setResourceName($item->value);
                        if ($uri) $field->setResourcePath($uri);

                    }

                }
            }

        }

        return true;

    }

}