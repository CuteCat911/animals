<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\CmsCollection;

class CmsCollectionService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getCmsCollectionJsonData(?CmsCollection $collection)
    {

        if (!$collection) {
            return false;
        }

        $categories_data = [];
        $categories = $collection->getCategories();

        foreach ($categories as $category) {
            $categories_data[] = $category->getJsonData();
        }

        $data = [
            'id' => $collection->getId(),
            'name' => $collection->getName(),
            'serverName' => $collection->getServerName(),
            'index' => $collection->getIndexNumber(),
            'categories' => $categories_data,
            'items' => $collection->getItems()
        ];

        return $data;

    }

    public function getCmsCollectionsJsonData()
    {

        $collections = $this->em->getRepository(CmsCollection::class)->findAll();
        $collections_data = [];

        foreach ($collections as $collection) {
            $collections_data[] = $this->getCmsCollectionJsonData($collection);
        }

        return $collections_data;

    }

    public function checkCollectionItemData($data, $response, $collection_item = false)
    {

        if (!isset($data)) return $response->setMessage('')->json();
        if (!property_exists($data, 'collectionId') || empty($data->collectionId)) return $response->setMessage('')->json();
        if (!property_exists($data, 'type') || empty($data->type)) return $response->setMessage('')->json();
        if ($collection_item && (!property_exists($data, 'id') || empty($data->id))) return $response->setMessage('')->json();

        $collection = $this->em->getRepository(CmsCollection::class)->find($data->collectionId);

        if (empty($collection)) return $response->setMessage('')->json();

        if ($collection_item) {

            $entity_namespace = $collection_item['namespace'];
            $entity = $this->em->getRepository($entity_namespace)->find($data->id);

            if (empty($entity)) return $response->setMessage('')->json();

        }

        return true;

    }

}