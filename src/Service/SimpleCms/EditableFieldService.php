<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\EditableField;
use App\Entity\SimpleCms\Page;

class EditableFieldService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function applyForItem($params)
    {

        $field = $params['field'];
        $fields_data = $field->getFieldsData();
        $type_name = $field->getType()->getName();
        $item = $params['item'];
        $entity = $params['entity'];
        $entity_fields_method_name = $fields_data[$type_name]['methodsName']['getAll'];
        $entity_fields = $entity->$entity_fields_method_name();
        $current_item_field = null;
        $check_field = false;
        $type = $params['type'];

        foreach ($entity_fields as $entity_field) {
            if ($entity_field->getField()->getId() == $field->getId()) {
                $current_item_field = $entity_field;
                $check_field = true;
                break;
            }
        }

        if ($item->checked && !$check_field) {
            $new_field = new $fields_data[$type_name]['namespace']();

            if ($type == 'page') {
                $new_field->setPage($entity);
            } else if ($type == 'other') {
                $add_entity_method_name = $item->entity->methodsName->addForField;
                $new_field->$add_entity_method_name($entity);
            }

            $new_field->setField($field);
            $this->em->persist($new_field);
        } else if (!$item->checked && $check_field) {
            $this->em->remove($current_item_field);
        }

    }

    public function applyForItems(?EditableField $field)
    {

        if (!isset($field)) {
            return false;
        }

        $items = $field->getItems();

        if (!$items) {
            return false;
        }

        foreach ($items['pages'] as $page) {

            $page_entity = $this->em->getRepository(Page::class)->find($page->id);

            $this->applyForItem([
                'field' => $field,
                'item' => $page,
                'entity' => $page_entity,
                'type' => 'page'
            ]);

            $this->em->flush();

        }

        foreach ($items['other'] as $item) {

            $namespace = $item->entity->namespace;
            $item_entity = $item->id == 'global' ? $this->em->getRepository($namespace)->find(1) : false;
            $item_entities = $item->id !== 'global' ? $this->em->getRepository($namespace)->findAll() : false;

            if ($item_entity) {

                $this->applyForItem([
                    'field' => $field,
                    'item' => $item,
                    'entity' => $item_entity,
                    'type' => 'other'
                ]);
                $this->em->flush();

            } else if ($item_entities) {

                foreach ($item_entities as $item_entity) {
                    $this->applyForItem([
                        'field' => $field,
                        'item' => $item,
                        'entity' => $item_entity,
                        'type' => 'other'
                    ]);
                    $this->em->flush();
                }

            }

        }

    }

    public function getStructureForCollectionItem($item_type, $default_structure, $categories, $entity = false)
    {

        $editable_fields = $this->em->getRepository(EditableField::class)->findAll();

        foreach ($editable_fields as $editable_field) {

            $editable_field_items = $editable_field->getItems();
            $editable_fields_data = $editable_field->getFieldsData();

            if (empty($editable_field_items)) continue;

            $editable_field_items = $editable_field_items['other'];
            $current_editable_field_item = null;

            foreach ($editable_field_items as $editable_field_item) {
                if ($editable_field_item && ($item_type == $editable_field_item->name && $editable_field_item->checked)) $current_editable_field_item = $editable_field_item;
            }

            if (!$current_editable_field_item) continue;

            $editable_field_type_name = $editable_field->getType()->getName();
            $get_fields_method_name = $editable_fields_data[$editable_field_type_name]['methodsName']['getAll'];
            $get_field_entity_method_name = $current_editable_field_item->entity->methodsName->getForField;
            $fields = $editable_field->$get_fields_method_name();
            $current_field = null;

            foreach ($fields as $field) {
                $field_entity = $field->$get_field_entity_method_name();
                if ($field_entity && $entity && $field_entity->getId() == $entity->getId()) $current_field = $field;
            }

            $value = null;

            if ($editable_field_type_name == 'image') {
                $value = [
                    'path' => $entity ? $current_field->getResourcePath() : null,
                    'name' => $entity ? $current_field->getResourceName() : null,
                    'acceptTypes' => $entity ? $current_field->getResourceTypes() : ['png', 'jpg', 'jpeg']
                ];
            } else {
                $value = $entity ? $current_field->getValue() : null;
            }

            $default_structure['other'][] = [
                'name' => $editable_field->getName(),
                'value' => $value,
                'serverName' => $editable_field->getServerName(),
                'type' => $editable_field_type_name,
                'description' => $editable_field->getDescription(),
                'index' => $editable_field->getIndexNumber(),
                'fieldData' => [
                    'fieldId' => $editable_field->getId(),
                    'fieldItemId' => $entity ? $current_field->getId() : null
                ]
            ];

        }

        $new_general = [];
        $new_seo = [];

        foreach ($default_structure['general'] as $item) {

            if ($entity) {
                switch ($item['name']) {
                    case 'name':
                        $item['value'] = $entity->getName();
                        break;
                    case 'path':
                        $item['value'] = $entity->getPath();
                        break;
                    case 'breadcrumbName':
                        $item['value'] = $entity->getBreadcrumbName();
                        break;
                    case 'published':
                        $item['value'] = $entity->getPublished();
                        break;
                    case 'index':
                        $item['value'] = $entity->getIndexNumber();
                        break;
                    case 'publicationTime':
                        $item['value'] = $entity->getPublicationTime();
                        break;
                    case 'category':

                        $categories_entity = $entity->getCategories();
                        $categories_id = [];

                        foreach ($categories_entity as $category) {
                            $categories_id[] = $category->getId();
                        }

                        $item['value'] = $categories_id;

                        break;
                }
            }

            if ($categories) {
                if ($item['name'] === 'category') $item['params']['checkboxes'] = $categories;
            }

            $new_general[] = $item;

        }

        foreach ($default_structure['seo'] as $item) {

            if ($entity) {
                switch ($item['name']) {
                    case 'title':
                        $item['value'] = $entity->getSeoTitle();
                        break;
                    case 'keys':
                        $item['value'] = $entity->getSeoKeywords();
                        break;
                    case 'description':
                        $item['value'] = $entity->getSeoDescription();
                        break;
                    case 'image':
                        $item['value'] = $entity->getSeoImage();
                        break;
                }
            }

            $new_seo[] = $item;

        }

        $default_structure['general'] = $new_general;
        $default_structure['seo'] = $new_seo;

        return $default_structure;

    }

}