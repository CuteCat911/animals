<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\SnippetTemplate;
use App\Entity\SimpleCms\GlobalData;
use App\Entity\SimpleCms\Snippet;
use App\Service\FileSaverService;

class SnippetService
{

    private $em;
    private $file_saver;

    public function __construct(EntityManagerInterface $em, FileSaverService $file_saver)
    {
        $this->em = $em;
        $this->file_saver = $file_saver;
    }

    public function checkStructure(?SnippetTemplate $template, $data)
    {

        $errors = [];
        $new_structure = [];

        foreach ($template->getStructure() as $item) {
             if (!property_exists($data, $item['serverName'])) {
                 $errors[] = 1;
             } else {

                 $property_name = $item['serverName'];
                 $item['value'] = $data->$property_name;
                 $new_structure[] = $item;

             }
        }

        if (count($errors)) {
            return [
                'status' => false
            ];
        } else {
            return [
                'status' => true,
                'structure' => $new_structure
            ];
        }

    }

    public function setSnippetFilesData($request, $structure, $dirs)
    {

        foreach ($structure as $item) {

            $type = $item['type'];

            if ($type == 'image') {

                $file = $request->files->get($item['type'] . '-' . $item['serverName']);
                $file_path = $this->file_saver->saveFile($file, $dirs[$type]);
                $item['value']->path = $file_path;

            }

        }

    }

    public function createNewSnippet(?SnippetTemplate $template, $structure)
    {

        $template_html = $template->getTemplate();

        foreach ($structure as $item) {

            $value = $item['value'];

            if (gettype($value) == 'object') {

                foreach ($value as $property_key=>$property) {

                    if (gettype($property) == 'string') {

                        $find_str = $item['serverName'] . '.' . $property_key;
                        $template_html = preg_replace('/{# +'. $find_str .'+ #}/m', $property, $template_html);

                    }

                }

            } else if ($value && $value != 'NULL') {
                $template_html = preg_replace('/{# +'. $item['serverName'] .'+ #}/m', $value, $template_html);
            } else {
                $template_html = preg_replace('/{# +'. $item['serverName'] .'+ #}/m', '', $template_html);
            }
        }

        $new_snippet = new Snippet();
        $global_data = $this->em->getRepository(GlobalData::class)->find(1);
        $new_snippet->setParent($template)->setContent($template_html)->setVariables($structure)->setGlobalData($global_data);

        return $new_snippet;

    }

}