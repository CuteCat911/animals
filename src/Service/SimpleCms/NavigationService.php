<?php

namespace App\Service\SimpleCms;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\SimpleCms\Navigation;

class NavigationService {

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getNavigation()
    {

        $navigation = $this->em->getRepository(Navigation::class)->findAll();
        $navigation_data = [];

        foreach ($navigation as $navigation_item) {
            $navigation_data[$navigation_item->getServerName()] = $navigation_item->getNavigationItems();
        }

        return $navigation_data;

    }

}