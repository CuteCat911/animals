<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class OrderFilter extends AbstractExtension
{

    public function getFilters()
    {
        return [
            new TwigFilter('orderFilter', [$this, 'modifiedArray'])
        ];
    }

    public function modifiedArray($array, $order = 'ASC')
    {

        $filtered_array = [];
        $max_index = 0;
        $i = 0;

        foreach ($array as $item) {

            $index = $item->getIndexNumber();

            if ($index > $max_index) {
                $max_index = $index;
            }
        }

        while ($order == 'ASC' ? $i <= $max_index : $i >= 0) {

            foreach ($array as $item) {
                if ($item->getIndexNumber() == $i) {
                    $filtered_array[] = $item;
                }
            }

            if ($order == 'ASC') {
                $i++;
            } else if ($order == 'DESC') {
                $i--;
            }

        }

        return $filtered_array;

    }

}