<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigTest;

class CheckNumber extends AbstractExtension
{

    public function getTests()
    {
        return [
            new TwigTest('number', [$this, 'checkNumber'])
        ];
    }

    public function checkNumber($value)
    {
        return is_numeric($value);
    }

}