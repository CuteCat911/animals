<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class Content extends AbstractExtension
{

    public function getFilters()
    {
        return [
            new TwigFilter('content', [$this, 'modifiedText'])
        ];
    }

    public function modifiedText($text, $snippets)
    {

        foreach ($snippets as $snippet) {
            $snippet_name = $snippet->getServerName();
            $text = str_replace('{# ' . $snippet_name . ' #}', $snippet->getContent(), $text);
        }

        return $text;

    }

}