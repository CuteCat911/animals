<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CutText extends AbstractExtension
{

    public function getFilters()
    {
        return [
            new TwigFilter('cutText', [$this, 'shortcutText'])
        ];
    }

    public function shortcutText($text, $count)
    {

        $text = trim($text);

        if (!isset($count) || !is_numeric($count)) {
            return $text;
        }

        $text_length = mb_strlen($text);

        if ($text_length < $count) {
            return $text;
        }

        $text_array = explode(' ', $text);
        $text_length = 0;
        $new_text_array = [];

        foreach ($text_array as $item) {

            $item_length = mb_strlen($item);
            $text_length += $item_length;
            $new_text_array[] = $item;

            if ($text_length > $count) {
                array_pop($new_text_array);
                break;
            }

        }

        $new_text_array[] = $this->modifiedLastWord(array_pop($new_text_array));
        $text = implode(' ', $new_text_array);

        return $text;

    }

    private function modifiedLastWord($word)
    {

        $word_length = mb_strlen($word);
        $last_symbol = mb_substr($word, $word_length - 1);

        if (!preg_match('/[A-zА-яё0-9]/i', $last_symbol)) {
            $word = mb_substr($word, 0, $word_length - 1);
        }

        return $word . '...';

    }

}