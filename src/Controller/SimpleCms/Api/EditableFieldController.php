<?php

namespace App\Controller\SimpleCms\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\EditableField;
use App\Entity\SimpleCms\EditableFieldType;
use App\Service\SimpleCms\CmsUserService;
use App\Service\SimpleCms\EditableFieldService;
use App\View\AjaxResponse;

/**
 * @Route("/sc/api/editable-field/")
 */
class EditableFieldController extends AbstractController
{

    /**
     * @Route("new", name="editable-field_api_new")
     */
    public function editableFieldApiNew(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, EditableFieldService $editable_field_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'editable-field_api_new';

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $name = $request->request->get('name');
        $server_name = $request->request->get('serverName');
        $description = $request->request->get('description');
        $index = $request->request->get('index');
        $type = $request->request->get('type');
        $pages_items = json_decode($request->request->get('pagesItems'));
        $other_items = json_decode($request->request->get('otherItems'));
        $items = [
            'pages' => $pages_items,
            'other' => $other_items
        ];

        if (!isset($name) || !isset($server_name) || !isset($index) || !isset($type)) {

            $errors = [];

            if (!isset($name)) $errors['name'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!isset($server_name)) $errors['serverName'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!isset($index)) $errors['index'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!isset($type)) $errors['type'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            return $response->setErrors($errors)->json();

        }

        $editable_field_type = $em->getRepository(EditableFieldType::class)->find($type);

        if (!$editable_field_type) {

            return $response->setErrors(['type' => [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ]])->json();

        }

        $editable_field = $em->getRepository(EditableField::class)->findBy(['serverName' => $server_name]);

        if ($editable_field) {

            return $response->setErrors(['serverName' => [
                'method' => $method_name,
                'code' => 0,
                'type' => 'wrong'
            ]])->json();

        }

        $new_editable_field = new EditableField();
        $new_editable_field->setName($name)->setServerName($server_name)->setIndexNumber($index)->setType($editable_field_type)->setItems($items);

        if (isset($description)) $new_editable_field->setDescription($description);

        $em->persist($new_editable_field);
        $em->flush();

        $editable_field_service->applyForItems($new_editable_field);

        return $response->setSuccess()->json();

    }

    /**
     * @Route("{id}", name="editable-field_api")
     */
    public function editableFieldApi(Request $request, AjaxResponse $response, UserInterface $user = null, $id, CmsUserService $cms_user_service, EditableFieldService $editable_field_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'editable-field_api';

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();

        if (!isset($id)) {
            return $response->setMessage('message.api.editableField.errors.idNotTransfer')->json();
        }

        $editable_field = $em->getRepository(EditableField::class)->find($id);

        if (!$editable_field) {
            return $response->setMessage('message.api.editableField.errors.fieldNotFind');
        }

        $name = $request->request->get('name');
        $server_name = $request->request->get('serverName');
        $description = $request->request->get('description');
        $index = $request->request->get('index');
        $type = $request->request->get('type');
        $pages_items = json_decode($request->request->get('pagesItems'));
        $other_items = json_decode($request->request->get('otherItems'));
        $items = [
            'pages' => $pages_items,
            'other' => $other_items
        ];

        if (!isset($name) || !isset($server_name) || !isset($index) || !isset($type)) {

            $errors = [];

            if (!isset($name)) $errors['name'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!isset($server_name)) $errors['serverName'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!isset($index)) $errors['index'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!isset($type)) $errors['type'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            return $response->setErrors($errors)->json();

        }

        $editable_field_type = $em->getRepository(EditableFieldType::class)->find($type);

        if (!$editable_field_type) {

            return $response->setErrors(['type' => [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ]])->json();

        }

        $editable_field->setName($name)->setServerName($server_name)->setIndexNumber($index)->setType($editable_field_type)->setItems($items);

        if (isset($description)) $editable_field->setDescription($description);

        $em->flush();

        $editable_field_service->applyForItems($editable_field);

        return $response->setSuccess()->json();

    }

}