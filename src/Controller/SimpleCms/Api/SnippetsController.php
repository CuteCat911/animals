<?php

namespace App\Controller\SimpleCms\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\SnippetTemplate;
use App\Entity\SimpleCms\Snippet;
use App\Service\SimpleCms\CmsUserService;
use App\Service\SimpleCms\SnippetService;
use App\View\AjaxResponse;

/**
 * @Route("/sc/api/")
 */
class SnippetsController extends AbstractController
{

    /**
     * @Route("snippets/data", name="snippets_api_data")
     */
    public function snippetsApiDataAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'snippets_api_get-data';

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $snippets = $em->getRepository(SnippetTemplate::class)->findAll();
        $snippets_data = [];

        foreach ($snippets as $snippet) {

            if (!$snippet->getPublished()) continue;

            $snippets_data[] = [
                'id' => $snippet->getId(),
                'name' => $snippet->getName(),
                'index' => $snippet->getIndexNumber()
            ];
        }

        $response->setData(['list' => $snippets_data])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("snippet-template/data", name="snippet-template_api_data")
     */
    public function snippetTemplateApiData(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'snippets_api_snippet-template_data';

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $id = $request->request->get('id');

        if (!isset($id)) {
            return $response->setMessage('')->json();
        }

        $snippet = $em->getRepository(SnippetTemplate::class)->find($id);

        if (!$snippet) {
            return $response->setMessage('')->json();
        }

        $response->setData(['snippet' => $snippet->getJsonData()])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("snippet/new ", name="snippet_api_new")
     */
    public function snippetApiNew(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, SnippetService $snippet_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'snippets_api_snippet_new';
        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $template_id = $request->request->get('templateId');

        if (!$template_id) {
            return $response->setMessage('')->json();
        }

        $template = $em->getRepository(SnippetTemplate::class)->find($template_id);

        if (!$template) {
            return $response->setMessage('')->json();
        }

        $data = $request->request->get('structure');

        if (!$data) {
            return $response->setMessage('')->json();
        }

        $data = json_decode($data);
        $check_structure = $snippet_service->checkStructure($template, $data);

        if (!$check_structure['status']) {
            return $response->setMessage('')->json();
        }

        $new_structure = $check_structure['structure'];
        $snippet_service->setSnippetFilesData($request, $new_structure, $template->getDefaultDirs());
        $snippet = $snippet_service->createNewSnippet($template, $new_structure);
        $snippet->setServerName('new');
        $em->persist($snippet);
        $em->flush();
        $snippet->setServerName($template->getServerName() . '-' . $snippet->getId());
        $em->flush();
        $response->setData(['content' => $snippet->getContent()])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("snippet/remove ", name="snippet_api_remove")
     */
    public function snippetApiRemove(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'snippets_api_snippet_remove';
        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $snippet_id = $request->request->get('id');

        if (!$snippet_id) {
            return $response->setMessage('')->json();
        }

        $snippet = $em->getRepository(Snippet::class)->find($snippet_id);

        if (!$snippet) {
            return $response->setMessage('')->json();
        }

        $em->remove($snippet);
        $em->flush();
        $response->setSuccess();

        return $response->json();

    }

}