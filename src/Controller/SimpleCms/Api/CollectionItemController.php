<?php

namespace App\Controller\SimpleCms\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\EditableField;
use App\Service\SimpleCms\CmsUserService;
use App\Service\SimpleCms\EditableFieldService;
use App\Service\SimpleCms\CollectionItemService;
use App\View\AjaxResponse;

/**
 * @Route("/sc/api/collection-item/")
 */
class CollectionItemController extends AbstractController
{

    /**
     * @Route("new", name="collection-item_api_new")
     */
    public function collectionItemApiNewAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, EditableFieldService $editable_field_service, CollectionItemService $collection_item_service, KernelInterface $kernel)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'collection-item_api_new';

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $item_data = json_decode($request->request->get('item'));
        $collection_items = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml')['simpleManager']['extension']['collection']['items'];

        // TODO распутать ситуацию что для проверки данных требуется наличие корректного элемента коллекции и наоборот
        // TODO не записывает other fields в базу данных

        if (!array_key_exists($item_data->type, $collection_items)) return $response->setMessage('')->json();

        $collection_item = $collection_items[$item_data->type];
        $check_collection_item = $collection_item_service->checkItemData($item_data, $response);

        if ($check_collection_item !== true) return $check_collection_item;

        $entity_namespace = $collection_item['namespace'];
        $entity = new $entity_namespace();
        $set_item_data = $collection_item_service->setItemData($request, $response, $method_name, $collection_item, $entity);

        $entity->setServerName('new');

        if ($set_item_data !== true) return $set_item_data;

        $em->persist($entity);
        $em->flush();
        $entity->setServerName($item_data->type . '-' . $entity->getId());
        $em->flush();
        $editable_fields = $em->getRepository(EditableField::class)->findAll();

        foreach ($editable_fields as $editable_field) $editable_field_service->applyForItems($editable_field);

        return $response->setSuccess()->json();

    }

    /**
     * @Route("{id}", name="collection-item_api")
     */
    public function collectionItemApiAction(Request $request, AjaxResponse $response, UserInterface $user = null, $id, CmsUserService $cms_user_service, CollectionItemService $collection_item_service, KernelInterface $kernel)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'collection-item_api';

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) return $response->json();

        $response->setLogged();
        $item_data = json_decode($request->request->get('item'));
        $collection_items = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml')['simpleManager']['extension']['collection']['items'];

        // TODO распутать ситуацию что для проверки данных требуется наличие корректного элемента коллекции и наоборот

        if (!array_key_exists($item_data->type, $collection_items)) return $response->setMessage('')->json();

        $collection_item = $collection_items[$item_data->type];
        $check_collection_item = $collection_item_service->checkItemData($item_data, $response, $id, $collection_item);

        if ($check_collection_item !== true) return $check_collection_item;

        $entity_namespace = $collection_item['namespace'];
        $entity = $em->getRepository($entity_namespace)->find($id);
        $set_item_data = $collection_item_service->setItemData($request, $response, $method_name, $collection_item, $entity);

        if ($set_item_data !== true) return $set_item_data;

        $em->flush();

        return $response->setSuccess()->json();

    }

}