<?php

namespace App\Controller\SimpleCms\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Entity\SimpleCms\CmsUser;
use App\Service\SimpleCms\CmsUserService;
use App\View\AjaxResponse;

/**
 * @Route("/sc/api/user/")
 */
class UserController extends AbstractController
{

    /**
     * @Route("login", name="cms_user_api_login")
     */
    public function cmsUserApiLoginAction(Request $request, AjaxResponse $response, UserPasswordEncoderInterface $encoder, AuthenticationManagerInterface $am, TokenStorageInterface $token_storage, CmsUserService $cms_user_service)
    {

        $em = $this->getDoctrine()->getManager();
        $method_name = 'cms_user_api_login';
        $login = $request->request->get('login');
        $password = $request->request->get('password');
        $action = $request->request->get('action');
        $action = json_decode($action);

        if (!$login || !$password) {

            $errors = [];

            if (!$login) $errors['login'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            if (!$password) $errors['password'] = [
                'method' => $method_name,
                'code' => 0,
                'type' => 'empty'
            ];

            return $response->setErrors($errors)->json();

        }

        $login_is_email = preg_match('/.+@.+\..+/i', $login);
        $check_password = mb_strlen($password) >= 6;

        if (!$check_password) {
            return $response->setErrors(['password' => [
                'method' => $method_name,
                'code' => 0,
                'type' => 'min'
            ]])->json();
        }

        if (!$login_is_email) {

            $check_login = preg_match('/[^а-яёА-ЯЁ]{3,}/i', $login);

            if (!$check_login) {
                return $response->setErrors(['login' => [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'regExp'
                ]])->json();
            }

        }

        $user = $login_is_email ? $em->getRepository(CmsUser::class)->findOneBy(['email' => $login]) : $em->getRepository(CmsUser::class)->findOneBy(['login' => $login]);

        if (!$user) {
            return $response->setErrors([
                'login' => [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'wrong'
                ],
                'password' => [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'wrong'
                ]
                ])->json();
        }

        if (!$encoder->isPasswordValid($user, $password)) {
            return $response->setErrors([
                'login' => [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'wrong'
                ],
                'password' => [
                    'method' => $method_name,
                    'code' => 0,
                    'type' => 'wrong'
                ],
            ])->json();
        }

        if ($user->getIsBlocked()) {
            return  $response->setErrors(['login' => [
                'method' => $method_name,
                'code' => 0,
                'type' => 'blocked'
            ]])->json();
        }

        $token = new UsernamePasswordToken(
            $user->getUsername(),
            $password,
            'cms_user_area',
            $user->getRoles()
        );
        $auth_token = $am->authenticate($token);
        $token_storage->setToken($auth_token);
        $cms_user_service->addAction($user, (array)$action);

        return $response->setLogged()->setSuccess()->setData([
            'user' => [
                'id' => $user->getId(),
                'login' => $user->getLogin(),
                'email' => $user->getEmail(),
                'roles' => $user->getRoles(),
                'sessionTime' => $user->getSessionTime()
            ]
        ])->json();

    }

    /**
     * @Route("logout", name="cms_user_api_logout")
     */
    public function cmsUserApiLogoutAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        if (!$user) {
            return $response->setSuccess()->json();
        }

        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);

        return $response->setLogged()->setSuccess()->json();

    }

    /**
     * @Route("add-action", name="cms_user_api_add-action")
     */
    public function cmsUserApiAddActionAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        if (!$user) {
            return $response->setSuccess()->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);

        return $response->setSuccess()->json();

    }

    /**
     * @Route("change-lang", name="cms_user_api_change-lang")
     */
    public function cmsUserApiChangeLangAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, KernelInterface $kernel)
    {

        if (!$user) {
            return $response->setSuccess()->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $lang = $request->request->get('lang');

        if (!$lang) {
            $response->setMessage('message.api.user.changeLang.errors.langNotFound')->json();
        }

        $cms_info_yaml_path = $kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml';
        $cms_info = Yaml::parseFile($cms_info_yaml_path);
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);

        if ($cms_info['lang'] != $lang) {
            $cms_info['lang'] = $lang;
            file_put_contents($cms_info_yaml_path, Yaml::dump($cms_info));
        }

        return $response->setSuccess()->json();

    }

}