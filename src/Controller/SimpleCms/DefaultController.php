<?php

namespace App\Controller\SimpleCms;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{

    /**
     * @Route("/simple-cms/{path}", name="simple_cms", requirements={"path"=".*"})
     */
    public function simpleCmsAction()
    {
        return $this->render('simple_cms/index.html.twig');
    }

}