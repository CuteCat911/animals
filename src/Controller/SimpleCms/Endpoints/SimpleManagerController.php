<?php

namespace App\Controller\SimpleCms\Endpoints;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Service\SimpleCms\CmsUserService;
use App\Service\SimpleCms\CmsCollectionService;
use App\View\AjaxResponse;

/**
 * @Route("/sc/endpoints/simple-manager")
 */
class SimpleManagerController extends AbstractController
{

    /**
     * @Route("", name="sc_simple-manager_endpoint")
     */
    public function scSimpleManagerEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);
        $response->setSuccess();

        return $response->json();

    }

    /**
     * @Route("/extensions", name="sc_simple-manager_extensions_endpoint")
     */
    public function scSimpleManagerExtensionsEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, CmsCollectionService $collection_service)
    {

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $collections_data = $collection_service->getCmsCollectionsJsonData();
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);
        $response->setSuccess();
        $response->setData([
            'collections' => $collections_data
        ]);

        return $response->json();

    }

//    /**
//     * @Route("/extension/{id}", name="sc_simple-manager_extension_endpoint")
//     */
//    public function scSimpleManagerExtensionEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, $id)
//    {
//
//        if (!$user) {
//            return $response->json();
//        }
//
//        $cms_user_service = $this->get(CmsUserService::class);
//        $session = $cms_user_service->checkSession($user);
//
//        if (!$session) {
//            return $response->json();
//        }
//
//        $action = $request->request->get('action');
//        $action = json_decode($action);
//        $cms_user_service->addAction($user, (array)$action);
//        $response->setLogged()->setSuccess();
//
//        return $response->json();
//
//    }

}