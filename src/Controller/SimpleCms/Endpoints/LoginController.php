<?php

namespace App\Controller\SimpleCms\Endpoints;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Service\SimpleCms\CmsUserService;

class LoginController extends AbstractController
{

    /**
     * @Route("/sc/endpoints/login", name="sc_login_endpoint")
     */
    public function scLoginEndpointAction(UserInterface $user = null, CmsUserService $cms_user_service, KernelInterface $kernel)
    {

        $session = $cms_user_service->checkSession($user);

        if ($user && !$session) {
            return new JsonResponse([
                'success' => false,
                'logged' => false,
                'data' => [
                    'redirect' => [
                        'name' => 'logout'
                    ]
                ]
            ]);
        }

        $cms_info = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml');
        $site_info = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/site_config.yaml');

        if ($user) {
            return new JsonResponse([
                'success' => true,
                'data' => [
                    'redirect' => [
                        'name' => 'index'
                    ]
                ]
            ]);
        } else {
            return new JsonResponse([
                'success' => true,
                'data' => [
                    'cms' => [
                        'id' => $cms_info['id'],
                        'version' => $cms_info['version'],
                        'lang' => $cms_info['lang']
                    ],
                    'site' => [
                        'url' => $site_info['url']
                    ]
                ]
            ]);
        }

    }

}