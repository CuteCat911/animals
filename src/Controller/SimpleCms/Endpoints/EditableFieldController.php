<?php

namespace App\Controller\SimpleCms\Endpoints;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Service\SimpleCms\CmsUserService;
use App\Entity\SimpleCms\EditableFieldType;
use App\Entity\SimpleCms\EditableField;
use App\Entity\SimpleCms\Page;
use App\View\AjaxResponse;

/**
 * @Route("/sc/endpoints/simple-manager/")
 */
class EditableFieldController extends AbstractController
{

    private function fieldTypes(): ?array
    {

        $em = $this->getDoctrine()->getManager();
        $field_types = $em->getRepository(EditableFieldType::class)->findAll();
        $field_types_data = [];

        foreach ($field_types as $field_type) {
            $field_types_data[] = [
                'id' => $field_type->getId(),
                'name' => $field_type->getName()
            ];
        }

        return $field_types_data;

    }
    private function pageItems(): ?array
    {

        $em = $this->getDoctrine()->getManager();
        $pages = $em->getRepository(Page::class)->findAll();
        $pages_data = [];

        foreach ($pages as $page) {
            $pages_data[] = (object) [
                'id' => $page->getId(),
                'name' => $page->getName(),
                'index' => $page->getIndexNumber(),
                'ml' => false,
                'checked' => false
            ];
        }

        return $pages_data;

    }
    private function otherItems(KernelInterface $kernel): ?array
    {

        $other_items = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml')['simpleManager']['editableField']['otherItems'];
        $items = [];

        foreach ($other_items as $other_item) {
            $items[] = (object) $other_item;
        }

        return $items;

    }
    private function concatItems($editable_field_items, $type_editable_field_items, $default_items, $type): ?array
    {

        if ($type == 'add') {

            foreach ($default_items as $default_item) {
                $match = false;
                foreach ($editable_field_items[$type_editable_field_items] as $field_item) if ($default_item->id === $field_item->id) $match = true;
                if (!$match) $editable_field_items[$type_editable_field_items][] = $default_item;
            }

        } else if ($type == 'remove') {

            foreach ($editable_field_items[$type_editable_field_items] as $key=>$field_item) {
                $match = false;
                foreach ($default_items as $default_item) if ($field_item->id == $default_item->id) $match = true;
                if (!$match) array_splice($editable_field_items[$type_editable_field_items], $key, 1);
            }

        }

        return $editable_field_items;

    }
    private function concatWithDefaultItems(?EditableField $editableField): ?EditableField
    {

        $em = $this->getDoctrine()->getManager();
        $items = $editableField->getItems();
        $page_items = $this->pageItems();
        $other_items = $this->otherItems();
        $items = $this->concatItems($items, 'pages', $page_items, 'add');
        $items = $this->concatItems($items, 'other', $other_items, 'add');
        $items = $this->concatItems($items, 'pages', $page_items, 'remove');
        $items = $this->concatItems($items, 'other', $other_items, 'remove');
        $editableField->setItems($items);
        $em->flush();

        return $editableField;

    }

    /**
     * @Route("editable-field/new", name="sc_new_editable-field_endpoint")
     */
    public function scNewEditableFieldEndpoint(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

//        $em = $this->getDoctrine()->getManager();

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);

        $response->setData([
            'types' => $this->fieldTypes(),
            'items' => [
                'pages' => $this->pageItems(),
                'other' => $this->otherItems()
            ]
        ])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("editable-field", name="sc_editable-field_endpoint")
     */
    public function scEditableFieldEndpoint(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        $em = $this->getDoctrine()->getManager();

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $editable_field_id = $request->request->get('id');

        if (!isset($editable_field_id)) {
            return $response->setMessage('message.endpoints.editableField.errors.idNotTransfer')->json();
        }

        $editable_field = $em->getRepository(EditableField::class)->find($editable_field_id);

        if (!$editable_field) {
            return $response->setMessage('message.endpoints.editableField.errors.notFound')->json();
        }

        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);
        $editable_field = $this->concatWithDefaultItems($editable_field);

        $response->setData([
            'types' => $this->fieldTypes(),
            'editableField' => $editable_field->getJsonData()
        ]);

        return $response->setSuccess()->json();

    }

}