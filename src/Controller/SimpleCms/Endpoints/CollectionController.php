<?php

namespace App\Controller\SimpleCms\Endpoints;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\CmsCollection;
use App\Service\SimpleCms\CmsUserService;
use App\Service\SimpleCms\CmsCollectionService;
use App\Service\SimpleCms\EditableFieldService;
use App\View\AjaxResponse;

/**
 * @Route("/sc/endpoints/simple-manager/")
 */
class CollectionController extends AbstractController
{

    public function categories($collection)
    {

        $em = $this->getDoctrine()->getManager();
        $collection = $em->getRepository(CmsCollection::class)->find($collection);

        if (empty($collection)) return false;

        $categories = $collection->getCategories();
        $categories_data = [];

        foreach ($categories as $category) {
            $categories_data[] = (object) [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'index' => $category->getIndexNumber(),
                'ml' => false,
                'checked' => false
            ];
        }

        return $categories_data;

    }

    /**
     * @Route("collections", name="sc_collections_endpoint")
     */
    public function scCollectionsEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, CmsCollectionService $collection_service)
    {

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);
        $collections_data = $collection_service->getCmsCollectionsJsonData();
        $response->setData([
            'collections' => $collections_data
        ])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("collection", name="sc_collection_endpoint")
     */
    public function scCollectionEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, CmsCollectionService $collection_service)
    {

        $em = $this->getDoctrine()->getManager();

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $collection_id = $request->request->get('id');

        if (!$collection_id) {
            return $response->setMessage('message.endpoint.collection.errors.emptyId')->json();
        }

        $collection = $em->getRepository(CmsCollection::class)->find($collection_id);

        if (!$collection) {
            return $response->setMessage('message.endpoint.collection.errors.collectionNotFound')->json();
        }

        $collection_data = $collection_service->getCmsCollectionJsonData($collection);
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);
        $response->setData([
            'collection' => $collection_data
        ])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("collection/new-item", name="sc_collection_new-item_endpoint")
     */
    public function scCollectionNewItemEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, CmsCollectionService $collection_service, EditableFieldService $editable_field_service, KernelInterface $kernel)
    {

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $item_data = json_decode($request->request->get('item'));
        $check_collection_item = $collection_service->checkCollectionItemData($item_data, $response);

        if ($check_collection_item !== true) return $check_collection_item;

        $collection_items = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml')['simpleManager']['extension']['collection']['items'];

        if (!array_key_exists($item_data->type, $collection_items)) return $response->setMessage('')->json();

        $default_structure = $collection_items[$item_data->type]['structure'];

        if (!$default_structure) return $response->setMessage('')->json();

        $structure = $editable_field_service->getStructureForCollectionItem($item_data->type, $default_structure, $this->categories($item_data->collectionId));

        if (empty($structure)) return $response->setMessage('')->json();

        $action = json_decode($request->request->get('action'));
        $cms_user_service->addAction($user, (array)$action);
        $response->setData(['structure' => $structure])->setSuccess();

        return $response->json();

    }

    /**
     * @Route("collection/item", name="sc_collection_item_endpoint")
     */
    public function scCollectionItemEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service, CmsCollectionService $collection_service, EditableFieldService $editable_field_service, KernelInterface $kernel)
    {

        $em = $this->getDoctrine()->getManager();

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $item_data = json_decode($request->request->get('item'));
        $collection_items = Yaml::parseFile($kernel->getProjectDir() . '/config/simple_cms/cms_config.yaml')['simpleManager']['extension']['collection']['items'];

        // TODO распутать ситуацию что для проверки данных требуется наличие корректного элемента коллекции и наоборот

        if (!array_key_exists($item_data->type, $collection_items)) return $response->setMessage('')->json();

        $collection_item = $collection_items[$item_data->type];
        $check_collection_item = $collection_service->checkCollectionItemData($item_data, $response, $collection_item);

        if ($check_collection_item !== true) return $check_collection_item;

        $default_structure = $collection_item['structure'];

        if (!$default_structure) return $response->setMessage('')->json();

        $entity_namespace = $collection_item['namespace'];
        $entity = $em->getRepository($entity_namespace)->find($item_data->id);
        $structure = $editable_field_service->getStructureForCollectionItem($item_data->type, $default_structure, $this->categories($item_data->collectionId), $entity);

        if (empty($structure)) return $response->setMessage('')->json();

        $action = json_decode($request->request->get('action'));
        $cms_user_service->addAction($user, (array)$action);
        $response->setData(['structure' => $structure])->setSuccess();

        return $response->json();

    }

}