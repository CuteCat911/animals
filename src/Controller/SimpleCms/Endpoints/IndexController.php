<?php

namespace App\Controller\SimpleCms\Endpoints;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Service\SimpleCms\CmsUserService;
use App\View\AjaxResponse;

class IndexController extends AbstractController
{

    /**
     * @Route("/sc/endpoints/index", name="sc_index_endpoint")
     */
    public function scIndexEndpointAction(Request $request, AjaxResponse $response, UserInterface $user = null, CmsUserService $cms_user_service)
    {

        if (!$user) {
            return $response->json();
        }

        $session = $cms_user_service->checkSession($user);

        if (!$session) {
            return $response->json();
        }

        $response->setLogged();
        $action = $request->request->get('action');
        $action = json_decode($action);
        $cms_user_service->addAction($user, (array)$action);
        $response->setSuccess();

        return $response->json();

    }

}