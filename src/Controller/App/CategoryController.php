<?php

namespace App\Controller\App;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\GlobalData;
use App\Entity\App\Category;
use App\Entity\App\Article;
use App\Service\SimpleCms\NavigationService;
use App\Service\SimpleCms\ViewService;
use App\Service\App\ArticleService;

/**
 * @Route("/kategorii/")
 */
class CategoryController extends AbstractController
{

    /**
     * @Route("{category_path}", name="category")
     */
    public function categoryPageAction(Request $request, $category_path, UserInterface $user = null, NavigationService $navigation_service)
    {

        $em = $this->getDoctrine()->getManager();
        $page_index = $request->query->get('page') ? $request->query->get('page') : 1;
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $page = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);
        $articles = $em->getRepository(Article::class)->getArticlesOnPage($page, $page_index);
        $next_articles = $em->getRepository(Article::class)->getArticlesOnPage($page, $page_index + 1);
        $popular_articles = $em->getRepository(Article::class)->getPopularArticles();
        $navigation = $navigation_service->getNavigation();

        if (!$page) {
            throw $this->createNotFoundException();
        }

        return $this->render('app/site/pages/category.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'seo' => $page->getSeoInfo(),
                'data' => $page,
                'breadcrumbs' => [
                    [
                        'name' => $page->getBreadcrumbName(),
                        'last' => true
                    ]
                ]
            ],
            'user' => $user,
            'articles' => [
                'list' => $articles,
                'popular' => $popular_articles,
                'next_page' => count($next_articles) ? true : false
            ],
            'navigation' => $navigation
        ]);

    }

    /**
     * @Route("{category_path}/{article_path}", name="article")
     */
    public function articlePageAction(Request $request, $category_path, $article_path, UserInterface $user = null, ArticleService $article_service, NavigationService $navigation_service, ViewService $view_service)
    {

        $em = $this->getDoctrine()->getManager();
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $category = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);
        $category_articles = $category->getArticles();
        $page = null;
        $random_articles = $article_service->getRandomArticles(3, $category);
        $popular_articles = $em->getRepository(Article::class)->getPopularArticles();
        $navigation = $navigation_service->getNavigation();

        foreach ($category_articles as $category_article) {
            if ($category_article->getPath() == $article_path) {
                $page = $category_article;
            }
        }

        if (!$page) {
            throw $this->createNotFoundException();
        }

        $view_service->createView($request->headers->get('User-Agent'), $page);
        $favorites_users = $page->getFavoritesUsers();

        foreach ($favorites_users as $favorites_user) {

            if ($user && $user == $favorites_user) {
                $favorite = true;
            }

        }

        return $this->render('app/site/pages/article.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'type' => 'article',
                'seo' => $page->getSeoInfo(),
                'data' => $page,
                'category' => $category,
                'breadcrumbs' => [
                    [
                        'name' => $category->getBreadcrumbName(),
                        'path' => $this->generateUrl('category', ['category_path' => $category_path])
                    ],
                    [
                        'name' => $page->getBreadcrumbName(),
                        'last' => true
                    ]
                ],
                'favorite' => isset($favorite) ? $favorite : false
            ],
            'articles' => [
                'popular' => $popular_articles,
                'random' => $random_articles
            ],
            'user' => $user,
            'navigation' => $navigation
        ]);

    }

}