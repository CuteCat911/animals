<?php

namespace App\Controller\App;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\GlobalData;
use App\Entity\SimpleCms\Page;
use App\Entity\App\Article;
use App\Service\SimpleCms\NavigationService;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="main")
     */
    public function mainPageAction(UserInterface $user = null, NavigationService $navigation_service)
    {

        $em = $this->getDoctrine()->getManager();
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $page = $em->getRepository(Page::class)->findOneBy(['serverName' => 'main']);
        $articles = $em->getRepository(Article::class)->getArticlesOnMainPage(1);
        $next_articles = $em->getRepository(Article::class)->getArticlesOnMainPage(2);
        $popular_articles = $em->getRepository(Article::class)->getPopularArticles();
        $navigation = $navigation_service->getNavigation();

        return $this->render('app/site/pages/index.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'seo' => $page->getSeoInfo(),
                'data' => $page
            ],
            'articles' => [
                'page_index' => 1,
                'list' => $articles,
                'popular' => $popular_articles,
                'next_page' => count($next_articles) ? true : false
            ],
            'user' => $user,
            'navigation' => $navigation
        ]);

    }

    /**
     * @Route("/adminer.php", name="adminer")
     */
    public function adminerPageAction()
    {
        return $this->redirectToRoute('main');
    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutPageAction(NavigationService $navigation_service)
    {

        $em = $this->getDoctrine()->getManager();
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $page = $em->getRepository(Page::class)->findOneBy(['serverName' => 'about']);
        $popular_articles = $em->getRepository(Article::class)->getPopularArticles();
        $navigation = $navigation_service->getNavigation();

        return $this->render('app/site/pages/about.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'seo' => $page->getSeoInfo(),
                'data' => $page,
                'breadcrumbs' => [
                    [
                        'name' => $page->getBreadcrumbName(),
                        'last' => true
                    ]
                ]
            ],
            'articles' => [
                'popular' => $popular_articles
            ],
            'navigation' => $navigation
        ]);

    }

    /**
     * @Route("/ads", name="advertising")
     */
    public function getPageAdvertising(NavigationService $navigation_service)
    {

        $em = $this->getDoctrine()->getManager();
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $page = $em->getRepository(Page::class)->findOneBy(['serverName' => 'advertising']);
        $popular_articles = $em->getRepository(Article::class)->getPopularArticles();
        $navigation = $navigation_service->getNavigation();

        return $this->render('app/site/pages/advertising.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'seo' => $page->getSeoInfo(),
                'data' => $page,
                'breadcrumbs' => [
                    [
                        'name' => $page->getBreadcrumbName(),
                        'last' => true
                    ]
                ]
            ],
            'articles' => [
                'popular' => $popular_articles
            ],
            'navigation' => $navigation
        ]);

    }

}