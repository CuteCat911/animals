<?php

namespace App\Controller\App;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\SimpleCms\GlobalData;
use App\Entity\SimpleCms\Page;
use App\Service\SimpleCms\NavigationService;

/**
 * @Route("/profile/")
 */
class AdminController extends AbstractController
{

    /**
     * @Route("", name="profile")
     */
    public function profilePageAction(UserInterface $user = null, NavigationService $navigation_service)
    {

        if (!$user) {
            return $this->redirectToRoute('main', ['_fragment' => 'login']);
        }

        $em = $this->getDoctrine()->getManager();
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $page = $em->getRepository(Page::class)->findOneBy(['serverName' => 'profile']);
        $navigation = $navigation_service->getNavigation();

        return $this->render('app/site/pages/profile.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'seo' => $page->getSeoInfo(),
                'data' => $page
            ],
            'user' => $user,
            'navigation' => $navigation
        ]);

    }

    /**
     * @Route("izbrannoe", name="favorites")
     */
    public function profileArticlesPageAction(UserInterface $user = null, NavigationService $navigation_service)
    {

        if (!$user) {
            return $this->redirectToRoute('main', ['_fragment' => 'login']);
        }

        $em = $this->getDoctrine()->getManager();
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();
        $page = $em->getRepository(Page::class)->findOneBy(['serverName' => 'profile_articles']);
        $navigation = $navigation_service->getNavigation();

        return $this->render('app/site/pages/profile-articles.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'seo' => $page->getSeoInfo(),
                'data' => $page
            ],
            'user' => $user,
            'navigation' => $navigation
        ]);

    }

}