<?php

namespace App\Controller\App\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\App\Category;
use App\Entity\App\Article;
use App\View\AjaxResponse;

/**
 * @Route("/api/articles/")
 */

class ArticlesController extends AbstractController
{

    /**
     * @Route("next-main-page", name="articles_next-main-page_api")
     */
    public function articlesNextMainPageApi(Request $request, AjaxResponse $response)
    {

        $em = $this->getDoctrine()->getManager();
        $page_index = $request->request->get('pageIndex');

        if (!$page_index) {
            return $response->setErrors(['pageIndex' => 'empty'])->json();
        }

        $articles = $em->getRepository(Article::class)->getArticlesOnMainPage($page_index);

        return $this->render('app/site/blocks/articles-on-main-page.html.twig', [
            'articles' => [
                'page_index' => $page_index,
                'list' => $articles
            ]
        ]);

    }

    /**
     * @Route("check-next-main-page", name="articles_check-next-main-page_api")
     */
    public function articlesCheckNextMainPageApi(Request $request, AjaxResponse $response)
    {

        $em = $this->getDoctrine()->getManager();
        $page_index = $request->request->get('pageIndex');

        if (!$page_index) {
            return $response->setErrors(['pageIndex' => 'empty'])->json();
        }

        $articles = $em->getRepository(Article::class)->getArticlesOnMainPage($page_index + 1);
        $response->setData(['nextPage' => count($articles) ? true : false]);

        return $response->setSuccess()->json();

    }

    /**
     * @Route("next-category-page", name="articles_next-category-page_api")
     */
    public function articlesNextCategoryPageApi(Request $request, AjaxResponse $response)
    {

        $em = $this->getDoctrine()->getManager();
        $page_index = $request->request->get('pageIndex');
        $category_path = $request->request->get('category');

        if (!$page_index || !$category_path) {

            $errors = [];

            if (!$page_index) {
                $errors['pageIndex'] = 'empty';
            }

            if (!$category_path) {
                $errors['category'] = 'empty';
            }

            return $response->setErrors($errors)->json();

        }

        $category = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);

        if (!$category) {
            return $response->setErrors(['category' => 'notFound'])->json();
        }

        $articles = $em->getRepository(Article::class)->getArticlesOnPage($category, $page_index);

        return $this->render('app/site/blocks/articles-on-category-page.html.twig', [
            'page' => [
                'data' => $category
            ],
            'articles' => [
                'page_index' => $page_index,
                'list' => $articles
            ]
        ]);

    }

    /**
     * @Route("check-next-category-page", name="articles_check-next-category-page_api")
     */
    public function articlesCheckNextCategoryPageApi(Request $request, AjaxResponse $response)
    {

        $em = $this->getDoctrine()->getManager();
        $page_index = $request->request->get('pageIndex');
        $category_path = $request->request->get('category');

        if (!$page_index || !$category_path) {

            $errors = [];

            if (!$page_index) {
                $errors['pageIndex'] = 'empty';
            }

            if (!$category_path) {
                $errors['category'] = 'empty';
            }

            return $response->setErrors($errors)->json();

        }

        $category = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);

        if (!$category) {
            return $response->setErrors(['category' => 'notFound'])->json();
        }

        $articles = $em->getRepository(Article::class)->getArticlesOnPage($category, $page_index + 1);
        $response->setData(['nextPage' => count($articles) ? true : false]);

        return $response->setSuccess()->json();

    }

}