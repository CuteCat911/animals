<?php

namespace App\Controller\App\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\App\Article;
use App\Entity\App\User;
use App\View\AjaxResponse;

/**
 * @Route("/api/profile/")
 */
class ProfileController extends AbstractController
{

    /**
     * @Route("registration", name="profile_registration_api")
     */
    public function profileRegistrationApiAction(Request $request, AjaxResponse $response, UserPasswordEncoderInterface $encoder)
    {

        $em = $this->getDoctrine()->getManager();
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        if (!isset($email) || !isset($password)) {

            $errors = [];

            if (!isset($email)) $errors['email'] = 'Введите email';
            if (!isset($password)) $errors['password'] = 'Введите пароль';

            return $response->setErrors($errors)->json();

        }

        $registered_user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

        if ($registered_user) {

            $message = 'Пользователь с таким email уже зарегестрирован';
            return $response->setMessage($message)->setErrors(['email' => $message])->json();

        }

        $check_email = preg_match('/.+@.+\..+/i', $email);
        $check_password = strlen($password) >= 6;

        if (!$check_email || !$check_password) {

            $errors = [];

            if (!$check_email) $errors['email'] = 'Введите корректный email';
            if (!$check_password) $errors['password'] = 'Пароль должен быть больше 5 символов';

            return $response->setErrors($errors)->json();

        }

        $new_user = new User();
        $encoded_password = $encoder->encodePassword($new_user, $password);
        $new_user->setEmail($email)->setPassword($encoded_password);
        $em->persist($new_user);
        $em->flush();
        $message = 'Вы успешно зарегестрировались. На указанную почту: <b>' . $email . '</b>, выслано письмо для подтверждения регистрации.';

        return $response->setSuccess()->setMessage($message)->json();

    }

    /**
     * @Route("login", name="profile_login_api")
     */
    public function profileLoginApiAction(Request $request, AjaxResponse $response, UserPasswordEncoderInterface $encoder, AuthenticationManagerInterface $am, TokenStorageInterface $token_storage)
    {

        $em = $this->getDoctrine()->getManager();
        $login = $request->request->get('login');
        $password = $request->request->get('password');

//        $login = 'CuteCat';
//        $password = 'ibanezjem777';

        if (!isset($login) || !isset($password)) {

            $errors = [];

            if (!isset($login)) $errors['login'] = 'Введите email или логин';
            if (!isset($password)) $errors['password'] = 'Введите пароль';

            return $response->setErrors($errors)->json();

        }

        $user = $em->getRepository(User::class)->findOneBy(['login' => $login]);

        if (!$user) $user = $em->getRepository(User::class)->findOneBy(['email' => $login]);

        if (!$user) {
            return $response->setErrors(['login' => 'Неверный логин или email', 'password' => 'Неверный логин или email'])->json();
        }

        $user_is_active = $user->getIsActive();

        if (!$user_is_active) {
            return $response->setMessage('Необходимо подтвердить email, письмо для поддтверждения было выслано вам на почту при регистрации.')->json();
        }

        $user_is_blocked = $user->getIsBlocked();

        if ($user_is_blocked) {
            return $response->setMessage('Ваш пользователь заблокирован, за дополнительной информацией обратитесь с службу поддержки.')->json();
        }

        $check_password = $encoder->isPasswordValid($user, $password);

        if (!$check_password) {
            return $response->setErrors(['login' => 'Неверный логин или email', 'password' => 'Неверный логин или email'])->json();
        }

        $token = new UsernamePasswordToken(
            $user->getUsername(),
            $password,
            'app_user_area',
            $user->getRoles()
        );
        $auth_token = $am->authenticate($token);
        $token_storage->setToken($auth_token);

        return $response->setSuccess()->setLogged()->setRedirect($this->generateUrl('profile'))->json();

    }

    /**
     * @Route("favorite-article/add", name="add_favorite_article_in_profile_api")
     */
    public function addFavoriteArticleInProfileApiAction(Request $request, AjaxResponse $response, UserInterface $user = null)
    {

        if (!$user) {
            return $response->setSuccess()->json();
        } else {
            $response->setLogged();
        }

        $em = $this->getDoctrine()->getManager();
        $article_id = $request->request->get('articleId');

        if (!$article_id) {
            return $response->setMessage('Не передан id статьи.')->json();
        }

        $article = $em->getRepository(Article::class)->find($article_id);

        if (!$article) {
            return $response->setMessage('Статья с id: ' . $article_id . ', не найдена.')->json();
        }

        $user->addFavoriteArticle($article);

        return $response->setSuccess()->json();

    }

    /**
     * @Route("favorite-article/remove", name="remove_favorite_article_in_profile_api")
     */
    public function removeFavoriteArticleInProfileApiAction(Request $request, AjaxResponse $response, UserInterface $user = null)
    {

        if (!$user) {
            return $response->setSuccess()->json();
        } else {
            $response->setLogged();
        }

        $em = $this->getDoctrine()->getManager();
        $article_id = $request->request->get('articleId');

        if (!$article_id) {
            return $response->setMessage('Не передан id статьи.')->json();
        }

        $article = $em->getRepository(Article::class)->find($article_id);

        if (!$article) {
            return $response->setMessage('Статья с id: ' . $article_id . ', не найдена.')->json();
        }

        $user->removeFavoriteArticle($article);

        return $response->setSuccess()->json();

    }

}