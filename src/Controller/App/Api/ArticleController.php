<?php

namespace App\Controller\App\Api;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Entity\SimpleCms\GlobalData;
use App\Entity\App\Article;
use App\Entity\App\Category;
use App\Service\App\ArticleService;
use App\Service\SimpleCms\ViewService;
use App\View\AjaxResponse;

/**
 * @Route("/api/article/")
 */
class ArticleController extends AbstractController
{

    /**
     * @Route("next", name="article_next_api")
     */
    public function articleNextApi(Request $request, AjaxResponse $response, ViewService $view_service)
    {

        $em = $this->getDoctrine()->getManager();
        $category_path = $request->request->get('category');
        $load_articles = $request->request->get('loadArticles');

        if (!$category_path || !$load_articles) {

            $errors = [];

            if (!$category_path) {
                $errors['category'] = 'empty';
            }

            if (!$load_articles) {
                $errors['loadArticles'] = 'empty';
            }

            return $response->setErrors($errors)->json();

        }

        $load_articles = json_decode($load_articles);
        $category = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);

        if (!$category) {
            return $response->setErrors(['category' => 'notFound'])->json();
        }

        $articles = $em->getRepository(Article::class)->findByCategory($category);
        $next_article = null;
        $global_data = $em->getRepository(GlobalData::class)->find(1)->getData();

        foreach ($articles as $article) {
            if (!in_array($article->getId(), $load_articles)) {
                $next_article = $article;
                break;
            }
        }

        if (!$next_article) {
            return $response->setData(['end' => true])->setSuccess()->json();
        }

        $view_service->createView($request->headers->get('User-Agent'), $next_article);

        return $this->render('app/site/blocks/article.html.twig', [
            'global_data' => $global_data,
            'page' => [
                'type' => 'article',
                'category' => $category,
                'data' => $next_article
            ]
        ]);

    }

    /**
     * @Route("next-data", name="article_next-data_api")
     */
    public function articleNextDataApi(Request$request, AjaxResponse $response)
    {

        $em = $this->getDoctrine()->getManager();
        $category_path = $request->request->get('category');
        $article_id = $request->request->get('id');

        if (!$category_path || !$article_id) {

            $errors = [];

            if (!$category_path) {
                $errors['category'] = 'empty';
            }

            if (!$article_id) {
                $errors['id'] = 'empty';
            }

            return $response->setErrors($errors)->json();
        }

        $category = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);

        if (!$category) {
            return $response->setErrors(['category' => 'notFound'])->json();
        }

        $article = $em->getRepository(Article::class)->find($article_id);

        if (!$article) {
            return $response->setErrors(['article' => 'notFound'])->json();
        }

        $article_seo = $article->getSeoInfo();
        $global_data = $em->getRepository(GlobalData::class)->find(1);
        $site_name = $global_data->getSiteName();
        $share_user_agents = $global_data->getShareUserAgents();
        $data = [
            'id' => $article->getId(),
            'title' => ($global_data->getSiteNameOnTitle() && $site_name) ? $article_seo['title'] . ' — ' . $site_name : $article_seo['title'],
            'canonical' => $article_seo['canonical'] ? $request->getUriForPath($article_seo['canonical']) : null,
            'keywords' => $article_seo['keywords'],
            'description' => $article_seo['description'],
            'url' => $this->generateUrl('article', ['category_path' => $category_path, 'article_path' => $article->getPath()], UrlGeneratorInterface::ABSOLUTE_URL)
        ];

        if (count($share_user_agents)) {

        } else {

            if ($article_seo['image'] && $article_seo['image']['default']) {
                $data['images'] = $request->getUriForPath($article_seo['image']['default']);
            } else {
                $data['images'] = $request->getUriForPath($global_data->getDefaultShareImage());
            }

        }

        $response->setData(['articleData' => $data]);

        return $response->setSuccess()->json();

    }

    /**
     * @Route("random-articles", name="article_random-articles_api")
     */
    public function articleRandomArticlesApi(Request $request, AjaxResponse $response, ArticleService $article_service)
    {

        $em = $this->getDoctrine()->getManager();
        $category_path = $request->request->get('category');

        if (!$category_path) {
            return $response->setErrors(['category' => 'empty'])->json();
        }

        $category = $em->getRepository(Category::class)->findOneBy(['path' => $category_path]);

        if (!$category) {
            return $response->setErrors(['category' => 'notFound'])->json();
        }

        $random_articles = $article_service->getRandomArticles(3, $category);

        return $this->render('app/site/blocks/random-articles.html.twig', [
            'articles' => [
                'random' => $random_articles
            ]
        ]);

    }

}