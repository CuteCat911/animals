import {checkType} from "./check-type";

export let destructuring = obj => {

    if (!checkType(obj, "object")) {
        if (DEV) console.error("Invalid variable type ($obg). Type required 'object'. Type received '%s'.", typeof obj);
        return false;
    }

    return obj;

};