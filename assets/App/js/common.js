import "babel-polyfill";
import "es6-promise/auto";
import "intersection-observer";

import {LazyImages} from "../../General/js/general/lazy-images";
import {Popups} from "../../General/js/general/popups";
import {Validation} from "../../General/js/general/validation";
import {focusInput} from "./modules/focus-input";
import {showPassword} from "./modules/show-password";
import {sendForm} from "./modules/send-form";
import {SlideSidebar} from "../../General/js/general/slide-sidebar";
import {RatioBlocks} from "../../General/js/general/ratio-blocks";
import {youtubeVideos} from "./modules/youtube-videos";
import {ProgressBar} from "../../General/js/general/progress-bar";
import {NextArticle} from "./modules/next-article";
import {NextPage} from "./modules/next-page";
import {openClass, closeClass} from "../../General/js/general/state-classes";

let ratioBlocks = null;
let lazyImages = null;
let defaultPopups = null;
let validationForms = null;
let progressBar = null;

document.addEventListener("DOMContentLoaded", () => {

    let slideSidebar = new SlideSidebar({
        sidebarWrapper: "js-slide-sidebar-wrapper",
        sidebar: "js-slide-sidebar",
        content: "js-slide-sidebar-content",
        indent: {
            top: 76,
            bottom: 16
        },
        minWidth: 860
    });
    let nextArticle = new NextArticle();
    let mobileMenu = new Popups({
        popups: "js-mobile-menu",
        wrapper: "js-mobile-menu-wrapper",
        btns: {
            open: "js-open-mobile-menu",
            close: "js-close-mobile-menu"
        },
        popup: {
            transitionTime: "0.3s"
        },
        overlay: {
            hide: true,
            color: "#f9f5f0"
        },
        openClass,
        closeClass
    });

    lazyImages = new LazyImages({
        images: "js-lazy-image",
        indent: {
            top: 100,
            bottom: 100
        }
    });
    defaultPopups = new Popups({
        popups: "js-popup",
        wrapper: "js-popups-wrapper",
        btns: {
            open: "js-open-popup",
            close: "js-close-popup"
        },
        overlay: {
            hide: true,
            opacity: 0.8,
            transitionTime: "0.6s"
        },
        mode: "lap"
    });
    validationForms = new Validation({
        forms: "js-form",
        inputs: "js-input",
        warnBlocks: "js-warn-block",
        submitBtns: "js-submit-btn",
        classes: {
            default: true
        },
        ajax: true,
        rules: {
            global: {
                input: {
                    name: {
                        email: {
                            texts: {
                                error: "Введите email"
                            }
                        },
                        password: {
                            texts: {
                                error: "Введите текст"
                            }
                        }
                    }
                }
            },
            login: {
                input: {
                    name: {
                        login: {
                            texts: {
                                error: "Введите email или логин"
                            }
                        }
                    }
                }
            }
        }
    });
    ratioBlocks = new RatioBlocks("js-ratio");
    progressBar = new ProgressBar({
        progress: "js-progress",
        observable: "js-observable"
    });
    new NextPage();

    if (!progressBar.fallInstall) progressBar.addFuncs(nextArticle.loadArticle);

    focusInput();
    showPassword();
    validationForms.addFuncs({
        event: "success",
        formsName: ["login"],
        funcs: sendForm
    });
    youtubeVideos();

});

export {ratioBlocks, lazyImages, defaultPopups, validationForms, progressBar};