import {findElemsClass, findFirstClass} from "../../../General/js/general/find";
import {focusedClass} from "../../../General/js/general/state-classes";

export let focusInput = () => {

    const classes = {
        wrapper: "js-focus-input-wrapper",
        input: "js-focus-input"
    };
    const elems = {
        wrappers: findElemsClass(classes.wrapper, document)
    };

    if (!elems.wrappers) {
        if (DEV) console.error();
        return false;
    }

    for (let wrapper of elems.wrappers) {

        let input = findFirstClass(classes.input, wrapper);

        if (!input) {
            if (DEV) console.error();
            return false;
        }

        let value = input.value;

        if (value) wrapper.classList.add(focusedClass);

        input.addEventListener("focus", () => {
            wrapper.classList.add(focusedClass);
        });

        input.addEventListener("blur", () => {
            let value = input.value;
            if (!value) wrapper.classList.remove(focusedClass);
        });

    }

};