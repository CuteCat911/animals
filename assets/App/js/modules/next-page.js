import axios from "axios";

import {findFirstClass} from "../../../General/js/general/find";
import {activeClass} from "../../../General/js/general/state-classes";
import {serialize} from "../../../General/js/general/serialize";
import {checkType} from "../../../General/js/general/check-type";

import {lazyImages} from "../common";
import {attr} from "../../../General/js/general/attr";
import {getWindowScroll, windowScroll} from "../../../General/js/general/window-scroll";

export let NextPage = class NextPage {

    #data = {
        classes: {
            articlesList: "js-articles-list",
            moreBtnWrapper: "js-more-btn-wrapper",
            moreBtn: "js-more-btn",
            preloader: "js-preloader",
            style: {
                listItem: "preview-articles__list-item"
            },
            js: {
                checkPageIndex: "js-check-page-index"
            }
        },
        elems: {
            articlesList: null,
            moreBtnWrapper: null,
            moreBtn: null,
            loader: null
        },
        api: {
            mainPage: {
                articles: {
                    method: "post",
                    url: "/api/articles/next-main-page"
                },
                nextPage: {
                    method: "post",
                    url: "/api/articles/check-next-main-page"
                }
            },
            category: {
                articles: {
                    method: "post",
                    url: "/api/articles/next-category-page"
                },
                nextPage: {
                    method: "post",
                    url: "/api/articles/check-next-category-page"
                }
            }
        }
    };
    #system = {
        pageIndex: null,
        category: null,
        items: [],
        viewedPages: []
    };
    #attrs = {
        pageIndex: "data-page-index"
    };

    constructor() {

        let elems = this.#data.elems;

        this.#__findElems();

        if (!elems.articlesList || !elems.moreBtnWrapper || !elems.moreBtn || !elems.preloader) {
            if (DEV) console.error();
            return {fallInstall: true};
        }

        this.#__checkCategory();

        elems.moreBtn.addEventListener("click", () => {
            this.#__setPageIndex();
            this.#__loadArticles();
        });

        windowScroll(() => {
            this.#__checkPageUrl();
        });

        window.onpopstate = (e) => {
            if (!e.state.url) return false;
            window.location = e.state.url;
        };

    }

    #__findElems = () => {

        let classes = this.#data.classes;
        let elems = this.#data.elems;

        elems.articlesList = findFirstClass(classes.articlesList, document);
        elems.moreBtnWrapper = findFirstClass(classes.moreBtnWrapper, document);
        elems.moreBtn = findFirstClass(classes.moreBtn, document);
        elems.preloader = findFirstClass(classes.preloader, document);

    };

    #__checkCategory = () => {
        let urlPaths = window.location.pathname.split("/");
        this.#system.category = urlPaths[urlPaths.length - 1];
    };

    #__setPageIndex = () => {

        let system = this.#system;
        let getParams = window.location.search.substr(1).split("&");

        for (let param of getParams) {

            let items = param.split("=");
            if (items[0] !== "page") continue;
            system.pageIndex = +items[1];

        }

        if (!system.pageIndex) system.pageIndex = 1;

    };

    #__updatePageIndex = index => {

        if (!checkType(index, "string")) {
            if (DEV) console.error();
            return false;
        }

        let checkPage = window.location.href.indexOf("page=") !== -1;
        let href = window.location.href;
        let newUrl = null;

        if (checkPage) {
            newUrl = window.location.href.replace(new RegExp("/?page=+[0-9]{1,}|&page=+[0-9]{1,}", "gm"), "page=" + index);
        } else {
            newUrl = window.location.search ? href + "&page=" + index : href + "?page=" + index;
        }

        this.#system.pageIndex = +index;
        if (this.#system.category) history.pushState({url: newUrl}, "", newUrl);

    };

    #__checkPageUrl = () => {

        let system = this.#system;

        if (!system.items) return false;

        let scroll = getWindowScroll();

        for (let item of system.items) {

            let itemTop = item.getBoundingClientRect().top + scroll;
            let pageIndex = attr(item, this.#attrs.pageIndex);

            if (scroll >= itemTop - 100) {

                if (system.viewedPages.indexOf(pageIndex) !== -1) continue;

                system.viewedPages.push(pageIndex);
                this.#__updatePageIndex(pageIndex);

            } else {

                let itemIndex = system.viewedPages.indexOf(pageIndex);

                if (itemIndex === -1) continue;

                system.viewedPages.splice(itemIndex, 1);
                this.#__updatePageIndex(+pageIndex - 1 + "");

            }
        }


    };

    #__addArticles = articles => {

        if (!checkType(articles, "string")) {
            if (DEV) console.error();
            return false;
        }

        let system = this.#system;
        let data = this.#data;
        let elems = data.elems;
        let listItem = document.createElement("div");

        listItem.classList.add(data.classes.style.listItem);
        listItem.classList.add(data.classes.js.checkPageIndex);
        attr(listItem, this.#attrs.pageIndex, system.pageIndex + 1);
        listItem.innerHTML = articles;
        system.items.push(listItem);
        elems.articlesList.appendChild(listItem);
        lazyImages.findImages();

    };

    #__setMoreBtnStatus = (status) => {

        if (!checkType(status, "boolean")) {
            if (DEV) console.error();
            return false;
        }

        let elems = this.#data.elems;

        if (status) {
            elems.moreBtnWrapper.classList.add(activeClass);
        } else {
            elems.moreBtnWrapper.classList.remove(activeClass);
        }

    };

    #__loadArticles = () => {

        let data = this.#data;
        let elems = data.elems;
        let request = this.#system.category ? data.api.category.articles : data.api.mainPage.articles;

        elems.moreBtnWrapper.classList.remove(activeClass);
        elems.preloader.classList.add(activeClass);

        axios({
            method: request.method,
            url: request.url,
            data: serialize(request.method, {
                pageIndex: this.#system.pageIndex + 1,
                category: this.#system.category
            }),
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        })
            .then(response => {

                this.#__checkNextPage()
                    .then(check => {

                        this.#__addArticles(response.data);
                        this.#__setMoreBtnStatus(check);
                        elems.preloader.classList.remove(activeClass);

                    })
                    .catch(() => {
                        elems.preloader.classList.remove(activeClass);
                    });

            })
            .catch(error => {
                if (DEV) console.error(error);
                elems.preloader.classList.remove(activeClass);
            });

    };

    #__checkNextPage = () => {

        return new Promise((resolve, reject) => {

            let api = this.#data.api;
            let request = this.#system.category ? api.category.nextPage : api.mainPage.nextPage

            axios({
                method: request.method,
                url: request.url,
                data: serialize(request.method, {
                    pageIndex: this.#system.pageIndex + 1,
                    category: this.#system.category
                }),
                headers: {"Content-Type": "application/x-www-form-urlencoded"}
            })
                .then(response => {

                    let requestData = response.data;

                    if (!requestData.success) {
                        reject();
                        return false;
                    }

                    resolve(requestData.data.nextPage);

                })
                .catch(error => {
                    if (DEV) console.error(error);
                    reject();
                });

        });

    };

};