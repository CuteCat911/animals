import {findElemsClass, findFirstClass} from "../../../General/js/general/find";
import {attr} from "../../../General/js/general/attr";
import {activeClass} from "../../../General/js/general/state-classes";

export let showPassword = () => {

    const classes = {
        wrapper: "js-show-password-wrapper",
        input: "js-show-password-input",
        btn: "js-show-password-btn"
    };
    const elems = {
        wrappers: findElemsClass(classes.wrapper, document)
    };

    if (!elems.wrappers) {
        if (DEV) console.error();
        return false;
    }

    let setInputType = (input, btn) => {

        let type = attr(input, "type");

        if (type === "password") {
            attr(input, "type", "text");
            btn.classList.add(activeClass);
        } else {
            attr(input, "type", "password");
            btn.classList.remove(activeClass);
        }

    };

    for (let wrapper of elems.wrappers) {

        let input = findFirstClass(classes.input, wrapper);
        let btn = findFirstClass(classes.btn, wrapper);

        if (!input || !btn) {
            if (DEV) console.error();
            return false;
        }

        btn.addEventListener("click", () => {
            setInputType(input, btn);
        });

    }

};