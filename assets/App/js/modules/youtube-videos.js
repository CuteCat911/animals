import {findElemsClass} from "../../../General/js/general/find";
import {attr} from "../../../General/js/general/attr";
import {checkType} from "../../../General/js/general/check-type";
import {pageLoad} from "../../../General/js/general/page-load";

export let youtubeVideos = (update) => {

    const classes = {
        video: "js-youtube-video"
    };
    const elems = {
        videos: findElemsClass(classes.video, document)
    };
    const attrs = {
        src: "data-video-src"
    };

    if (!elems.videos || !YT) {
        if (DEV) console.error();
        return false;
    }

    let initPlayers = () => {

        for (let video of elems.videos) {

            let src = attr(video, attrs.src);

            if (!checkType(src, "string")) continue;

            let player = new YT.Player(video, {
                width: "100%",
                height: "100%",
                videoId: src.split("watch?v=")[1],
                playerVars: {
                    rel: 0,
                    showinfo: 0
                }
            });

        }

    };

    if (checkType(update, "boolean") && update) initPlayers();

    pageLoad(initPlayers);

};