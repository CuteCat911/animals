import axios from "axios";

import {findElemsTag, findFirstClass, findFirstTag} from "../../../General/js/general/find";
import {attr} from "../../../General/js/general/attr";
import {checkType} from "../../../General/js/general/check-type";
import {serialize} from "../../../General/js/general/serialize";
import {activeClass} from "../../../General/js/general/state-classes";
import {getWindowScroll, windowScroll} from "../../../General/js/general/window-scroll";
import {RatioBlocks} from "../../../General/js/general/ratio-blocks";

import {lazyImages, ratioBlocks} from "../common";
import {progressBar} from "../common";
import {youtubeVideos} from "./youtube-videos";
import {destructuring} from "../../../General/js/general/destructuring";

export let NextArticle = class NextArticle {

    #data = {
        classes: {
            articlesList: "js-articles-list",
            article: "js-article",
            preloader: "js-preloader",
            style: {
                articleItem: "articles-list__item"
            },
            js: {
                observable: "js-observable"
            }
        },
        elems: {
            articlesList: null,
            article: null,
            preloader: null,
            seo: {
                meta: {
                    keywords: null,
                    description: null
                },
                og: {
                    title: null,
                    description: null,
                    url: null,
                    image: null
                },
                canonical: null,
                title: null
            }
        },
        api: {
            article: {
                method: "post",
                url: "/api/article/next"
            },
            articleData: {
                method: "post",
                url: "/api/article/next-data"
            },
            randomArticles: {
                method: "post",
                url: "/api/article/random-articles"
            }
        }
    };
    #system = {
        category: null,
        articlesList: [],
        articles: {},
        currentArticleData: {
            id: null,
            elem: null,
            title: null,
            canonical: null,
            keywords: null,
            description: null,
            url: null,
            image: null,
            nextLoad: false,
            error: false,
            endArticles: false
        },
        viewedArticles: []
    };
    #attrs = {
        articleId: "data-article-id"
    };

    constructor() {

        let elems = this.#data.elems;
        let seo = elems.seo;

        this.#__findElems();

        if (!elems.articlesList || !elems.article || !elems.preloader) {
            if (DEV) console.error();
            return {fallInstall: true};
        }

        this.#__findSeoElems();

        if (!seo.title || !seo.canonical || !seo.meta.description || !seo.meta.keywords || !seo.og.title || !seo.og.description || !seo.og.url || !seo.og.image) {
            if (DEV) console.error();
            return {fallInstall: true};
        }

        this.#__checkCategory();
        this.#__setArticleData();
        this.#__addArticle();

        windowScroll(() => {
            this.#__checkPageUrl();
        });

        window.onpopstate = (e) => {
            if (!e.state.url) return false;
            window.location = e.state.url;
        };

    };

    #__findElems = () => {

        let classes = this.#data.classes;
        let elems = this.#data.elems;

        elems.articlesList = findFirstClass(classes.articlesList, document);
        elems.article = findFirstClass(classes.article, document);
        elems.preloader = findFirstClass(classes.preloader, document);

    };

    #__findTitle = () => {
        this.#data.elems.seo.title = findFirstTag("title", document);
    };

    #__findCanonical = () => {

        let links = findElemsTag("link", document);

        for (let link of links) {
            if (attr(link, "rel") !== "canonical") continue;
            this.#data.elems.seo.canonical = link;
        }

    };

    #__findMeta = () => {

        let metas = findElemsTag("meta", document);
        let metaElems = this.#data.elems.seo.meta;

        for (let i in metaElems) {
            for (let meta of metas) {
                let name = attr(meta, "name");
                if (name !== i) continue;
                metaElems[i] = meta;
            }
        }

    };

    #__findOg = () => {

        let metas = findElemsTag("meta", document);
        let ogElems = this.#data.elems.seo.og;

        for (let i in ogElems) {
            for (let meta of metas) {
                let property = attr(meta, "property");
                if (!property || property !== "og:" + i) continue;
                ogElems[i] = meta;
            }
        }

    };

    #__findSeoElems = () => {
        this.#__findTitle();
        this.#__findCanonical();
        this.#__findMeta();
        this.#__findOg();
    };

    #__checkCategory = () => {
        let urlPaths = window.location.pathname.split("/");
        this.#system.category = urlPaths[urlPaths.length - 2];
    };

    #__setArticleData = data => {

        let articleData = this.#system.currentArticleData;

        if (checkType(data, "object")) {
            this.#system.currentArticleData = data;
        } else {

            let elems = this.#data.elems;
            let seo = elems.seo;

            articleData.id = +attr(elems.article, this.#attrs.articleId);
            articleData.elem = elems.article;
            articleData.title = seo.title.innerText;
            articleData.canonical = attr(seo.canonical, "href");
            articleData.description = attr(seo.meta.description, "content");
            articleData.keywords = attr(seo.meta.keywords, "content");
            articleData.url = attr(seo.og.url, "content");
            articleData.image = attr(seo.og.image, "content");

        }

    };

    #__addArticle = data => {

        let system = this.#system;
        let articleData = system.currentArticleData;

        if (checkType(data, "object")) {

            system.articles[data.id] = data;
            system.articlesList.push(data.elem);

        } else {

            system.articles[articleData.id] = articleData;
            system.articlesList.push(articleData.elem);

        }
    };

    #__loadNextArticle = () => {

        let data = this.#data;
        let system = this.#system;
        let elems = data.elems;
        let request = data.api.article;
        let loadArticles = [];

        elems.preloader.classList.add(activeClass);

        for (let i in system.articles) {
            loadArticles.push(system.articles[i].id);
        }

        axios({
            method: request.method,
            url: request.url,
            data: serialize(request.method, {
                category: system.category,
                loadArticles: JSON.stringify(loadArticles)
            }),
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        })
            .then(response => {

                let requestData = response.data;

                if (checkType(requestData, "object")) {

                    if (!requestData.success) {
                        system.currentArticleData.error = true;
                    } else {
                        system.currentArticleData.endArticles = true;
                    }

                    elems.preloader.classList.remove(activeClass);
                    return false;

                }

                this.#__loadRandomArticles()
                    .then(articles => {

                        this.#__createNewArticle(requestData, articles)
                            .then(() => {
                                system.currentArticleData.nextLoad = true;
                            })
                            .catch(() => {
                                system.currentArticleData.error = true;
                            })
                            .finally(() => {
                                elems.preloader.classList.remove(activeClass);
                            });

                    })
                    .catch(() => {
                        system.currentArticleData.error = true;
                        elems.preloader.classList.remove(activeClass);
                    });

            })
            .catch(error => {

                if (DEV) console.error(error);
                system.currentArticleData.error = true;
                elems.preloader.classList.remove(activeClass);

            });

    };

    #__loadRandomArticles = () => {

        let request = this.#data.api.randomArticles;

        return new Promise((resolve, reject) => {

            axios({
                method: request.method,
                url: request.url,
                data: serialize(request.method, {
                    category: this.#system.category
                }),
                headers: {"Content-Type": "application/x-www-form-urlencoded"}
            })
                .then(response => {

                    let requestData = response.data;

                    if (checkType(requestData, "object") && !requestData.success) {
                        reject();
                        return false;
                    }

                    resolve(requestData);

                })
                .catch(error => {
                    if (DEV) console.error(error);
                    reject();
                });

        });

    };

    #__loadArticleData = id => {

        let request = this.#data.api.articleData;

        return new Promise((resolve, reject) => {

            if (!checkType(id, "number")) {
                if (DEV) console.error();
                reject();
            }

            axios({
                method: request.method,
                url: request.url,
                data: serialize(request.method, {
                    category: this.#system.category,
                    id
                }),
                headers: {"Content-Type": "application/x-www-form-urlencoded"}
            })
                .then(response => {

                    let requestData = response.data;

                    if (!requestData.success) {
                        reject();
                        return false;
                    }

                    resolve(requestData.data.articleData);

                })
                .catch(error => {
                    if (DEV) console.error(error);
                    reject();
                });

        });

    };

    #__createNewArticle = (article, randomArticles) => {

        let classes = this.#data.classes;

        return new Promise((resolve, reject) => {

            if (!checkType(article, "string") || !checkType(randomArticles, "string")) {
                if (DEV) console.error();
                reject();
            }

            let articleItem = document.createElement("div");
            let articleElem = null;
            let articleId = null;

            articleItem.classList.add(classes.style.articleItem);
            articleItem.innerHTML = article + randomArticles;
            articleElem = findFirstClass(classes.article, articleItem);

            if (!articleElem) reject();

            articleId = attr(articleElem, this.#attrs.articleId);

            if (!checkType(articleId, "string")) return false;

            this.#__loadArticleData(+articleId)
                .then(articleData => {

                    if (!checkType(articleData, "object")) return false;

                    articleData.elem = articleElem;
                    articleData.nextLoad = false;
                    articleData.endArticles = false;
                    articleData.error = false;
                    articleData.endArticles = false;
                    this.#__addArticle(articleData);
                    this.#data.elems.articlesList.appendChild(articleItem);
                    ratioBlocks.findElems();
                    ratioBlocks.applyResize();
                    lazyImages.findImages();
                    youtubeVideos(true);
                    resolve();

                })
                .catch(() => {
                    reject();
                });

        });

    };

    #__checkPageUrl = () => {

        let system = this.#system;

        if (!system.articlesList.length === 1) return false;

        let scroll = getWindowScroll();

        for (let item of system.articlesList) {

            let itemTop = item.getBoundingClientRect().top + scroll;
            let articleId = attr(item, this.#attrs.articleId);

            if (!checkType(articleId, "string")) continue;

            if (scroll >= itemTop - 76) {

                if (system.viewedArticles.indexOf(articleId) !== -1) continue;

                system.viewedArticles.push(articleId);
                this.#__changeObservableArticle(item);
                this.#__updatePageSeo(system.articles[articleId]);

            } else {

                let itemIndex = system.viewedArticles.indexOf(articleId);

                if (itemIndex === -1) continue;

                system.viewedArticles.splice(itemIndex, 1);
                this.#__changeObservableArticle(system.articlesList[itemIndex - 1]);
                this.#__updatePageSeo(system.articles[system.viewedArticles[itemIndex - 1]]);

            }

        }

    };

    #__updatePageSeo = articleData => {

        if (!checkType(articleData, "object")) {
            if (DEV) console.error();
            return false;
        }

        let seo = this.#data.elems.seo;
        let currentArticle = this.#system.currentArticleData;

        if (articleData.id === currentArticle.id) return false;

        this.#__setArticleData(articleData);
        currentArticle = this.#system.currentArticleData;

        let {title, canonical, keywords, description, url, image} = destructuring(currentArticle);

        seo.title.innerText = title;
        attr(seo.canonical, "href", canonical ? canonical : url);
        attr(seo.meta.keywords, "content", keywords);
        attr(seo.meta.description, "content", description);
        attr(seo.og.title, "content", title);
        attr(seo.og.description, "content", description);
        attr(seo.og.url, "content", url);
        attr(seo.og.image, "content", image);
        history.pushState({url: url}, "", url);

    };

    #__changeObservableArticle = article => {

        if (!checkType(article, "object")) {
            if (DEV) console.error();
            return false;
        }

        let classes = this.#data.classes;
        let system = this.#system;
        let articleId = attr(article, this.#attrs.articleId);

        if (!checkType(articleId, "string") || +articleId === system.currentArticleData.id) return false;

        for (let $article of system.articlesList) {
            if ($article === article) {
                $article.classList.add(classes.js.observable);
            } else {
                $article.classList.remove(classes.js.observable);
            }
        }

        progressBar.findElems();

    };

    loadArticle = (progress) => {

        let articleData = this.#system.currentArticleData;

        if (progress < 50 || (articleData.nextLoad || articleData.endArticles || articleData.error)) return false;

        articleData.nextLoad = true;
        this.#__loadNextArticle();

    };

};