import axios from "axios";

import {checkType} from "../../../General/js/general/check-type";
import {attr} from "../../../General/js/general/attr";
import {serialize} from "../../../General/js/general/serialize";
import {validationForms} from "../common";

export let sendForm = (formInfo) => {

    if (!checkType(formInfo, "object")) {
        if (DEV) console.error();
        return false;
    }

    let url = attr(formInfo.elem, "action");
    let method = attr(formInfo.elem, "method");

    axios({
        url,
        method,
        data: serialize(method, formInfo.data),
        headers: {"Content-Type": "application/x-www-form-urlencoded"}
    })
        .then(response => {

            let requestData = response.data;

            if (!requestData.success) {

                if (requestData.errors) {
                    for (let i in requestData.errors) validationForms.setCustomInputError(i, formInfo.params.name, requestData.errors[i]);
                }

                return false;

            }

            if (requestData.redirect) window.location = requestData.redirect;

        })
        .catch(error => {

        });

};