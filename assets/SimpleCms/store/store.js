import Vue from "vue";
import Vuex from "vuex";

// Modules

import {Cms} from "./modules/cms";
import {Site} from "./modules/site";
import {User} from "./modules/user";
import {Endpoints} from "./modules/endpoints";
import {Api} from "./modules/api";
import {SimpleManager} from "./modules/simple-manager";
import {Wysiwyg} from "./modules/wysiwyg";

// End Modules

Vue.use(Vuex);

export const Store = new Vuex.Store({
    strict: process.env.NODE_ENV !== "production",
    modules: {
        cms: Cms,
        site: Site,
        user: User,
        endpoints: Endpoints,
        api: Api,
        simpleManager: SimpleManager,
        wysiwyg: Wysiwyg
    }
});