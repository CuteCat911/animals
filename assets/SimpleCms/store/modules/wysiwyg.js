import {checkType} from "../../../General/js/general/check-type";

export const Wysiwyg = {
    namespaced: true,
    state: {
        snippets: []
    },
    getters: {
        snippets: state => {
            return state.snippets;
        }
    },
    mutations: {
        snippets: (state, data) => {

            if (!checkType(data, "array")) {
                if (DEV) console.error();
                return false;
            }

            state.snippets = data;

        }
    }
};