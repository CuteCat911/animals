import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

export const User = {
    namespaced: true,
    state: {
        id: null,
        login: null,
        email: null,
        roles: null,
        sessionTime: null,
        actions: {
            last: {
                code: null,
                event: null,
                time: null
            },
            list: []
        }
    },
    getters: {
        all: state => {
            return state;
        },
        id: state => {
            return state.id;
        },
        login: state => {
            return state.login;
        },
        email: state => {
            return state.email;
        },
        roles: state => {
            return state.roles;
        },
        sessionTime: state => {
            return state.sessionTime;
        },
        actions: state => {
            return state.actions;
        }
    },
    mutations: {
        id: (state, id) => {

            if (!checkType(id, "number")) {
                if (DEV) console.error("Invalid variable type ($id). Type required 'number'. Type received '%s'.", typeof id);
                return false;
            }

            state.id = id;

        },
        login: (state, login) => {

            if (!checkType(login, "string")) {
                if (DEV) console.error("Invalid variable type ($login). Type required 'string'. Type received '%s'.", typeof login);
                return false;
            }

            state.login = login;

        },
        email: (state, email) => {

            if (!checkType(email, "string")) {
                if (DEV) console.error("Invalid variable type ($email). Type required 'string'. Type received '%s'.", typeof email);
                return false;
            }

            state.email = email;

        },
        roles: (state, roles) => {

            if (!checkType(roles, "array")) {
                if (DEV) console.error("Invalid variable type ($roles). Type required 'object (array)'. Type received '%s'.", typeof roles);
                return false;
            }

            state.roles = roles;

        },
        sessionTime: (state, time) => {

            if (!checkType(time, "number")) {
                if (DEV) console.error("Invalid variable type ($time). Type required 'number'. Type received '%s'.", typeof time);
                return false;
            }

            state.sessionTime = time;

        },
        lastAction: (state, action) => {

            if (!checkType(action, "object")) {
                if (DEV) console.error("Invalid variable type ($action). Type required 'object'. Type received '%s'.", typeof action);
                return false;
            }

            let {code, event, time} = destructuring(action);

            if (!checkType(code, "number") || !checkType(event, "string") || !checkType(time, "string")) {
                if (DEV) console.error("Invalid variable type ($code or $event or $time). Type required $code = 'number', $event = 'string', $time = 'string'. Type received $code = '%s', $event = '%s', $time = '%s'.", typeof code, typeof event, typeof time);
                return false;
            }

            state.actions.last = {code, event, time};

        },
        addAction: (state, action) => {

            if (!checkType(action, "object")) {
                if (DEV) console.error("Invalid variable type ($action). Type required 'object'. Type received '%s'.", typeof action);
                return false;
            }

            let {code, event, time} = destructuring(action);

            if (!checkType(code, "number") || !checkType(event, "string") || !checkType(time, "string")) {
                if (DEV) console.error("Invalid variable type ($code or $event or $time). Type required $code = 'number', $event = 'string', $time = 'string'. Type received $code = '%s', $event = '%s', $time = '%s'.", typeof code, typeof event, typeof time);
                return false;
            }

            state.actions.list.push(action);

        }
    },
    actions: {
        login: ({dispatch, commit, state}, data) => {

            if (!checkType(data, "object") || !checkType(data.data, "object")) {
                if (DEV) console.error("Invalid variable type ($data or $data.data). Type required $data = 'object', $data.data = 'object'. Type received $data = '%s', $data.data = '%s'.", typeof data, typeof data.data);
                return false;
            }

            let {id, login, email, roles, sessionTime} = destructuring(data.data);
            let storage = data.storage;

            commit("id", id);
            commit("login", login);
            commit("email", email);
            commit("roles", roles);
            commit("sessionTime", sessionTime);

            if (storage) {
                storage.set("user", {
                    id: state.id,
                    login: state.login,
                    email: state.email,
                    roles: state.roles,
                    sessionTime: state.sessionTime,
                    lastActionTime: state.actions.last.time
                });
            }

            dispatch("addAction", {
                action: {
                    code: 0,
                    event: "login",
                    time: new Date().toString()
                },
                storage
            });

        },
        addAction: ({commit, state}, data) => {

            if (!checkType(data, "object") || !checkType(data.action, "object")) {
                if (DEV) console.error("Invalid variable type ($data or $data.action). Type required $data = 'object', $data.action = 'object'. Type received $data = '%s', $data.action = '%s'.", typeof data, data.action);
                return false;
            }

            let action = data.action;
            let storage = data.storage;

            commit("lastAction", action);
            commit("addAction", action);

            if (storage) {
                let userData = storage.get("user");
                if (checkType(userData, "object")) {
                    userData.lastActionTime = checkType(action.time, "string") ? action.time : null;
                    storage.set("user", userData);
                }
            }

        }
    }
};