import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

export const Cms = {
    namespaced: true,
    state: {
        id: null,
        version: null,
        lang: "ru"
    },
    getters: {
        all: state => {
            return state;
        },
        id: state => {
            return state.id;
        },
        version: state => {
            return state.version;
        },
        lang: state => {
            return state.lang;
        }
    },
    mutations: {
        id: (state, id) => {

            if (!checkType(id, "string")) {
                if (DEV) console.error("Invalid variable type ($id). Type required 'string'. Type received '%s'.", typeof id);
                return false;
            }

            state.id = id;

        },
        version: (state, version) => {

            if (!checkType(version, "string")) {
                if (DEV) console.error("Invalid variable type ($version). Type required 'string'. Type received '%s'.", typeof version);
                return false;
            }

            state.version = version;

        },
        lang: (state, lang) => {

            if (!checkType(lang, "string")) {
                if (DEV) console.error("Invalid variable type ($lang). Type required 'string'. Type received '%s'.", typeof lang);
                return false;
            }

            state.lang = lang;

        }
    },
    actions: {
        setData: ({commit, state}, data) => {

            if (!checkType(data, "object") || !checkType(data.data, "object")) {
                if (DEV) console.error("Invalid variable type ($data or $data.data). Type required $data = 'object', $data.data = 'object'. Type received $data = '%s', $data.data = '%s'.", typeof data, typeof data.data);
                return false;
            }

            let {id, version, lang} = destructuring(data.data);
            let storage = data.storage;

            commit("id", id);
            commit("version", version);
            commit("lang", lang);

            if (checkType(storage, "object")) {
                storage.set("cms", {
                    id: state.id,
                    version: state.version,
                    lang: state.lang
                });
            }

        },
        changeLang: ({commit}, data) => {

            if (!checkType(data, "object")) {
                if (DEV) console.error();
                return false;
            }

            let {storage, lang} = destructuring(data);

            if (!checkType(lang, "string")) {
                if (DEV) console.error();
                return false;
            }

            commit("lang", lang);

            if (checkType(storage, "object")) {

                let newData = storage.get("cms");

                newData.lang = lang;
                storage.set("cms", newData);

            }

        }
    }
};