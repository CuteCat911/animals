import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

export const Site = {
    namespaced: true,
    state: {
        url: null
    },
    getters: {
        all: state => {
            return state;
        },
        url: state => {
            return state.url;
        }
    },
    mutations: {
        url: (state, url) => {

            if (!checkType(url, "string")) {
                if (DEV) console.error("Invalid variable type ($url). Type required 'string'. Type received '%s'.", typeof url);
                return false;
            }

            state.url = url;

        }
    },
    actions: {
        setData: ({commit, state}, data) => {

            if (!checkType(data, "object") || !checkType(data.data, "object")) {
                if (DEV) console.error("Invalid variable type ($data or $data.data). Type required $data = 'object', $data.data = 'object'. Type received $data = '%s', $data.data = '%s'.", typeof data, typeof data.data);
                return false;
            }

            let {url} = destructuring(data.data);
            let storage = data.storage;

            commit("url", url);

            if (storage) {
                storage.set("site", {
                    url: state.url
                });
            }

        }
    }
};