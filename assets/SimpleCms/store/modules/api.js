let path = "/sc/api/";

export const Api = {
    namespaced: true,
    state: {
        user: {
            login: {
                method: "post",
                url: path + "user/login"
            },
            logout: {
                method: "post",
                url: path + "user/logout"
            },
            backendLogout: "/simple-cms/logout_",
            changeLang: {
                method: "post",
                url: path + "user/change-lang"
            }
        },
        editableField: {
            new: {
                method: "post",
                url: path + "editable-field/new"
            },
            default: {
                method: "post",
                url: path + "editable-field/"
            }
        },
        collectionItem: {
            new: {
                method: "post",
                url: path + "collection-item/new"
            },
            default: {
                method: "post",
                url: path + "collection-item/"
            }
        },
        snippets: {
            data: {
                method: "post",
                url: path + "snippets/data"
            }
        },
        snippetTemplate: {
            data: {
                method: "post",
                url: path + "snippet-template/data"
            }
        },
        snippet: {
            new: {
                method: "post",
                url: path + "snippet/new"
            },
            remove: {
                method: "post",
                url: path + "snippet/remove"
            }
        }
    },
    getters: {
        all: state => {
            return state;
        },
        user: state => {
            return state.user;
        },
        editableField: state => {
            return state.editableField;
        },
        collectionItem: state => {
            return state.collectionItem;
        },
        snippets: state => {
            return state.snippets;
        },
        snippetTemplate: state => {
            return state.snippetTemplate;
        },
        snippet: state => {
            return state.snippet;
        }
    }
};