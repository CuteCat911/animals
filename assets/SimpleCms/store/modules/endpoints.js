let path = "/sc/endpoints/";

export const Endpoints = {
    namespaced: true,
    state: {
        login: {
            method: "post",
            url: path + "login"
        },
        index: {
            method: "post",
            url: path + "index"
        },
        simpleManager: {
            method: "post",
            url: path + "simple-manager"
        },
        extensions: {
            method: "post",
            url: path + "simple-manager/extensions"
        },
        collections: {
            method: "post",
            url: path + "simple-manager/collections"
        },
        newCollectionItem: {
            method: "post",
            url: path + "simple-manager/collection/new-item"
        },
        collectionItem: {
            method: "post",
            url: path + "simple-manager/collection/item"
        },
        collection: {
            method: "post",
            url: path + "simple-manager/collection"
        },
        newEditableField: {
            method: "post",
            url: path + "simple-manager/editable-field/new"
        },
        editableField: {
            method: "post",
            url: path + "simple-manager/editable-field"
        }
    },
    getters: {
        all: state => {
            return state;
        },
        login: state => {
            return state.login;
        },
        index: state => {
            return state.index;
        },
        simpleManager: state => {
            return state.simpleManager;
        },
        extensions: state => {
            return state.extensions;
        },
        collections: state => {
            return state.collections;
        },
        newCollectionItem: state => {
            return state.newCollectionItem;
        },
        collectionItem: state => {
            return state.collectionItem;
        },
        collection: state => {
            return state.collection;
        },
        newEditableField: state => {
            return state.newEditableField;
        },
        editableField: state => {
            return state.editableField;
        }
    }
};