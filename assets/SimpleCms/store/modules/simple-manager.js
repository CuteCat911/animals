import {checkType} from "../../../General/js/general/check-type";

export const SimpleManager = {
    namespaced: true,
    state: {
        collections: {
            list: null,
            current: null
        }
    },
    getters: {
        collections: state => {
            return state.collections;
        }
    },
    mutations: {
        collectionsList: (state, collections) => {

            if (!checkType(collections, "array")) {
                if (DEV) console.error();
                return false;
            }

            state.collections.list = collections;

        },
        currentCollection: (state, collection) => {

            if (!checkType(collection, "object")) {
                if (DEV) console.error();
                return false;
            }

            state.collections.current = collection;

        }
    }
};