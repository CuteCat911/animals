import {checkType} from "../../../General/js/general/check-type";
import {translit} from "./translit";

export let autoGenerateRules = {
    path: (currentLang, text) => {

        if (!checkType(currentLang, "string") || !checkType(text, "string")) {
            if (DEV) console.error();
            return null;
        }

        return translit(currentLang, text).replace(new RegExp(" ", "gmi"), "_").toLowerCase();

    }
};