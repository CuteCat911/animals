export let formInputTemplate = {
    type: null,
    title: null,
    description: null,
    value: null,
    success: false,
    error: {
        status: false,
        message: null,
        texts: null
    },
    length: {
        min: null,
        max: null
    },
    regExp: {
        pattern: null,
        flags: null
    },
    params: {
        notValid: false,
        onlyNumber: false,
        checkboxes: null,
        autoGenerate: {
            status: false,
            baseInput: null,
            rule: null,
            canEdit: false,
            edit: false
        }
    },
    selectList: null,
    fieldData: {
        fieldId: null,
        fieldItemId: null
    }
};