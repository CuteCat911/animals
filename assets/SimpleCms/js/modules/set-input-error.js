import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

export let setInputError = (inputName, input, error) => {

    if (!checkType(inputName, "string") || !checkType(input, "object") || !checkType(error, "object")) {
        if (DEV) console.error("Invalid variables type ($inputName or $input or $error). Type required $inputName = 'string', $input = 'object', $error = 'object'. Type received $inputName = '%s', $input = '%s', $error = '%s'.", typeof inputName, typeof input, typeof error);
        return false;
    }

    let {method, code, type} = destructuring(error);

    if (!checkType(method, "string") || !checkType(code, "number") || !checkType(type, "string")) {
        if (DEV) console.error("Invalid variables type ($method or $code or $type). Type required $method = 'string', $code = 'number', $type = 'string'. Type received $method = '%s', $code = '%s', $type = '%s'.", typeof method, typeof code, typeof type);
        return false;
    }

    let $error = input.error;

    input.success = false;
    $error.status = true;
    $error.message = $error.texts[type] ? $error.texts[type] : null;

};