import {checkType} from "../../../General/js/general/check-type";
import {orderIndex} from "./order-index";
import {createInputData} from "./create-input-data";
import {destructuring} from "../../../General/js/general/destructuring";

export let generateInputsByStructure = (structure, section, itemType) => {

    if (!checkType(structure, "object") || !checkType(section, "string") || !checkType(itemType, "string")) {
        if (DEV) console.error();
        return false;
    }

    let inputs = {};

    if (!checkType(structure, "array") && checkType(structure, "object")) {

        for (let i in structure) {

            inputs[i] = {};

            for (let item of orderIndex(structure[i])) {

                let {name, serverName, description, type, value, params, fieldData} = destructuring(item);
                let allInputs = inputs[i];

                if (i === "other") {
                    inputs[i][serverName] = createInputData(Object.assign({
                        title: name,
                    }, {description, type, value, params: {notValid: true}, fieldData, allInputs}));
                } else if (i === "seo") {
                    inputs[i][name] = createInputData(Object.assign({
                        title: "default.seo." + name + ".title"
                    }, {description, type, value, params, allInputs}));
                } else {
                    inputs[i][name] = createInputData(Object.assign({
                        title: "customInputs." + section + "." + itemType + "." + name + ".title"
                    }, {description, type, value, params, allInputs}));
                }

            }

        }

    } else if (checkType(structure, "array")) {

        for (let item of orderIndex(structure)) {

            let {name, serverName, description, type, value, params, fieldData} = destructuring(item);
            let allInputs = inputs;

            if (serverName && name) {
                inputs[serverName] = createInputData(Object.assign({
                    title: name
                }, {description, type, value, params, allInputs}));
            } else if (serverName && !name) {
                inputs[serverName] = createInputData(Object.assign({
                    title: "customInputs." + section + "." + itemType + "." + serverName + ".title"
                }, {description, type, value, params, allInputs}));
            } else if (name && !serverName) {
                inputs[name] = createInputData(Object.assign({
                    title: "customInputs." + section + "." + itemType + "." + name + ".title"
                }, {description, type, value, params, allInputs}));
            }

        }

    }

    return inputs;

};