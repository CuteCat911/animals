import {checkType} from "../../../General/js/general/check-type";

export let orderIndex = (array, order = "ASC") => {

    if ((!checkType(array, "array") && !array.length) || !checkType(order, "string")) {
        if (DEV) console.error();
        return false;
    }

    let orderedArray = [];
    let maxIndex = 0;

    for (let item of array) {
        if (checkType(item.index, "number") && item.index > maxIndex) maxIndex = item.index;
    }

    let index = (order === "ASC") ? 0 : maxIndex;

    while (order === "ASC" ? index <= maxIndex : index >= 0) {

        for (let item of array) {
            if (checkType(item.index, "number") && item.index === index) orderedArray.push(item);
        }

        if (order === "ASC") {
            index++;
        } else {
            index--;
        }

    }

    return orderedArray;

};