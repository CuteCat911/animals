import {checkType} from "../../../General/js/general/check-type";

export let checkMl = text => {

    if (!checkType(text, "string")) {
        if (DEV) console.error();
        return false;
    }

    return text.indexOf(".") !== -1;

};