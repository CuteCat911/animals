import {checkType} from "../../../General/js/general/check-type";

export let checkContentField = inputs => {

    if (!checkType(inputs, "object")) {
        if (DEV) console.error();
        return false;
    }

    let content = false;

    for (let i in inputs) {

        let item = inputs[i];

        if (!item.type) {
            content = checkContentField(item);
        } else if (item.type === "content") {
            content = true;
        }

    }

    return content;

};