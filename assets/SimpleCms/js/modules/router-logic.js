import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";
import {checkSession} from "./check-session";

export let routerLogic = params => {

    if (!checkType(params, "object")) {
        if (DEV) console.error("Invalid variable type ($params). Type required 'params'. Type received '%s'.", typeof params);
        return false;
    }

    let {vue, store, storage, to, from, next} = destructuring(params);
    let session = checkSession(store, storage);
    let go = false;

    if (session) {
        go = true;
    } else if (!session && to.name === "login") {
        go = true;
    } else if (to.name === "logout") {
        go = true;
    } else {
        next({name: "logout"});
    }

    if (go) {
        store.dispatch("user/addAction", {
            action: {
                code: 0,
                event: "go to page {# " + to.name + " #}",
                time: new Date().toString()
            },
            storage
        });
        next();
    }

};