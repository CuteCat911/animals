import axios from "axios";
import {checkType} from "../../../General/js/general/check-type";
import {serialize} from "../../../General/js/general/serialize";

export let changeLang = function(lang) {

    if (!checkType(lang, "string")) {
        if (DEV) console.error();
        return false;
    }

    let checkUser = !!this.$store.getters["user/id"];

    if (checkUser) {

        let vue = this;
        let request = this.$store.getters["api/user"].changeLang;

        this.$store.dispatch("user/addAction", {
            action: {
                code: 0,
                event: "changing lang to {# " + lang + " #}",
                time: new Date().toString()
            },
            storage: this.$storage
        });

        axios({
            method: request.method,
            url: request.url,
            data: serialize(request.method, {
                lang,
                action: JSON.stringify(this.$store.getters["user/actions"].last)
            }),
            headers : {"Content-Type": "application/x-www-form-urlencoded"}
        })
            .then(response => {

                let requestData = response.data;

                if (!requestData.success) {
                    return false;
                }

                if (!requestData.logged) {
                    vue.$root.defaultLogout();
                }

                vue.$store.dispatch("cms/changeLang", {storage: this.$storage, lang});
                vue.$ml.change(lang);

            })
            .catch(error => {

            });

    } else {

        this.$store.dispatch("cms/changeLang", {storage: this.$storage, lang});
        this.$ml.change(lang);

    }

};