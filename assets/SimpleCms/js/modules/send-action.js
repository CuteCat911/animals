import {checkType} from "../../../General/js/general/check-type";

export let sendAction = function(action) {

    if (!checkType(action, "object")) {
        if (DEV) console.error();
        return false;
    }

    console.log(this);

};