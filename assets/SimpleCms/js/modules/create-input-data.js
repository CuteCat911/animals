import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";
import {copyObj} from "../../../General/js/general/copy-obj";
import {formInputTemplate} from "./form-input-template";
import {autoGenerateRules} from "./auto-generate-rules";

export let createInputData = data => {

    let inputData = copyObj(formInputTemplate);

    if (!checkType(data, "object")) {
        return inputData;
    }

    let {type, title, description, value, valueForServer, success, errorStatus, errorMessage, texts, length, regExp, params, selectList, fieldData, allInputs} = destructuring(data);

    inputData.type = (checkType(type, "string")) ? type : null;
    inputData.title = (checkType(title, "string")) ? title : null;
    inputData.description = (checkType(description, "string")) ? description : null;
    // inputData.value = (value !== undefined) ? value : null;
    inputData.valueForServer = (checkType(valueForServer, "function")) ? valueForServer : null;
    inputData.success = checkType(success, "boolean") ? success : false;
    inputData.error.status = checkType(errorStatus, "boolean") ? errorStatus : false;
    inputData.error.message = checkType(errorMessage, "string") ? errorMessage : null;
    inputData.error.texts = checkType(texts, "object") ? texts : {};
    inputData.selectList = checkType(selectList, "array") ? selectList : null;

    if (value !== undefined) {
        inputData.value = value;
    } else {

        if (type === "image") {
            inputData.value = {
                path: null,
                name: null,
                acceptTypes: ["png", "jpg", "jpeg"]
            };
        } else {
            inputData.value = null;
        }

    }

    if (checkType(length, "object")) {
        inputData.length.min = checkType(length.min, "number") ? length.min : null;
        inputData.length.max = checkType(length.max, "number") ? length.max : null;
    }

    if (checkType(regExp, "object")) {
        inputData.regExp.pattern = checkType(regExp.pattern, "string") ? regExp.pattern : null;
        inputData.regExp.flags = checkType(regExp.flags, "string") ? regExp.flags : null;
    }

    if (checkType(params, "object")) {

        inputData.params.notValid = checkType(params.notValid, "boolean") ? params.notValid : false;
        inputData.params.onlyNumber = checkType(params.onlyNumber, "boolean") ? params.onlyNumber : false;
        inputData.params.checkboxes = checkType(params.checkboxes, "array") ? params.checkboxes : null;

        let autoGenerate = params.autoGenerate;

        if (checkType(autoGenerate, "object")) {
            inputData.params.autoGenerate.status = checkType(autoGenerate.status, "boolean") ? autoGenerate.status : false;
            inputData.params.autoGenerate.baseInput = checkType(autoGenerate.baseInput, "string") ? allInputs[autoGenerate.baseInput] : null;
            inputData.params.autoGenerate.rule = (checkType(autoGenerate.rule, "string") && checkType(autoGenerateRules[autoGenerate.rule], "function")) ? autoGenerateRules[autoGenerate.rule] : null;
            inputData.params.autoGenerate.canEdit = checkType(autoGenerate.canEdit, "boolean") ? autoGenerate.canEdit : false;
            inputData.params.autoGenerate.edit = checkType(autoGenerate.edit, "boolean") ? autoGenerate.edit : false;
        }

    }

    if (checkType(fieldData, "object")) {
        inputData.fieldData.fieldId = checkType(fieldData.fieldId, "number") ? fieldData.fieldId : null;
        inputData.fieldData.fieldItemId = checkType(fieldData.fieldItemId, "number") ? fieldData.fieldItemId : null;
    }

    return inputData;

};