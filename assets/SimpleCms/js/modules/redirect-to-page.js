import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

export let redirectToPage = (router, data) => {

    if (!checkType(router, "object") || !checkType(data, "object")) {
        if (DEV) console.error("Invalid variables type ($router or $data). Type required $router = 'object', $data = 'object'. Type received $router = '%s', $data = '%s'.", typeof router, typeof data);
        return false;
    }

    let {name, params, query, hash} = destructuring(data);

    router.push({name, params, query, hash});

};