import {checkType} from "../../../General/js/general/check-type";

export let translit = (lang, text, reverse = false) => {

    if (!checkType(lang, "string") || !checkType(text, "string")) {
        if (DEV) console.error();
        return false;
    }

    let associations = {
        "а": "a",
        "б": "b",
        "в": "v",
        "г": "g",
        "д": "d",
        "е": "e",
        "ё": "yo",
        "ж": "zh",
        "з": "z",
        "и": "i",
        "й": "j",
        "к": "k",
        "л": "l",
        "м": "m",
        "н": "n",
        "о": "o",
        "п": "p",
        "р": "r",
        "с": "s",
        "т": "t",
        "у": "u",
        "ф": "f",
        "х": "h",
        "ц": "c",
        "ч": "ch",
        "ш": "sh",
        "щ": "shch",
        "ъ": "",
        "ы": "y",
        "ь": "",
        "э": "eh",
        "ю": "yu",
        "я": "ya",
    };
    let newText = "";

    text = text.normalize();

    for (let item of text) {

        let isUpperCase = item === item.toUpperCase();
        let newLetter = associations[item.toLowerCase()];

        if (checkType(newLetter, "string")) {
            newText += isUpperCase ? newLetter.toUpperCase() : newLetter;
        } else {
            newText += isUpperCase ? item.toUpperCase() : item;
        }

    }

    return newText;

};