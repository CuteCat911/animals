import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

export let checkSession = (store, storage) => {

    if (!checkType(store, "object") || !checkType(storage, "object")) {
        if (DEV) console.error("Invalid variable type ($store or $storage). Type required $store = 'object', $storage = 'object'. Type received $store = '%s', $storage = '%s'.", typeof store, typeof storage);
        return false;
    }


    let cms = storage.get("cms");
    let site = storage.get("site");
    let user = storage.get("user");

    if (!checkType(cms, "object") || !checkType(site, "object") || !checkType(user, "object")) {
        if (DEV) console.warn("Not found cms, site and user data in local storage.");
        return false;
    }

    let lastActionTime = user.lastActionTime ? new Date(user.lastActionTime).getTime() : null;

    if (!lastActionTime) {
        return false;
    }

    let nowTime = new Date().getTime();
    let timeDiff = nowTime - lastActionTime;

    if (timeDiff < user.sessionTime) {

        let cmsData = store.getters["cms/all"];
        let siteData = store.getters["site/all"];
        let userData = store.getters["user/all"];

        if (cmsData.id && cmsData.version && siteData.url && userData.id && userData.login && userData.email && userData.roles && userData.sessionTime) {
            return true;
        }

        store.dispatch("cms/setData", {data: cms});
        store.dispatch("site/setData", {data: site});
        store.dispatch("user/login", {data: user});

        return true;

    } else {
        return false;
    }

};