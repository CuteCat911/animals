import axios from "axios";
import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";
import {serialize} from "../../../General/js/general/serialize";

export let getEndpointData = params => {

    if (!checkType(params, "object")) {
        if (DEV) console.error("Invalid variable type ($params). Type required 'object'. Type received '%s'.", typeof params);
        return false;
    }

    let {server, funcs} = destructuring(params);

    if (!checkType(server, "object") || !checkType(funcs, "object")) {
        if (DEV) console.error("Invalid variable type ($server or $funcs). Type required $server = 'object', $funcs = 'object'. Type received $server = '%s', $funcs = '%s'.", typeof server, typeof funcs);
        return false;
    }

    let {method, url, data, headers} = destructuring(server);

    if (!checkType(method, "string") || !checkType(url, "string")) {
        if (DEV) console.error("Invalid variable type ($method or $url). Type required $method = 'string', $url = 'string'. Type received $method = '%s', $url = '%s'.", typeof method, typeof url);
        return false;
    }

    let {success, notSuccess, notLogged, always} = destructuring(funcs);

    if (!checkType(success, "function")) {
        if (DEV) console.error("Invalid variable type ($success). Type required 'function'. Type received '%s'.", typeof success);
        return false;
    }

    axios({
        method,
        url,
        data: checkType(data, "object") ? serialize(method, data) : null,
        headers: checkType(headers, "object") ? headers : {"Content-Type": "application/x-www-form-urlencoded"}
    })
        .then(response => {

            let requestData = response.data;

            if (checkType(always, "function")) always(requestData);

            if (requestData.logged !== undefined && !requestData.logged) {
                if (checkType(notLogged, "function")) notLogged(requestData);
                return false;
            }

            if (!requestData.success) {
                if (checkType(notSuccess, "function")) notSuccess(requestData);
                return false;
            }

            success(requestData);

        })
        .catch(error => {

        });

};