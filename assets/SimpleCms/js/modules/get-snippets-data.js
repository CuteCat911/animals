import axios from "axios";

export let getSnippetsData = function() {

    let vue = this;
    let request = this.$store.getters["api/snippets"].data;

    axios({
        method: request.method,
        url: request.url,
        headers: {"Content-Type": "application/x-www-form-urlencoded"}
    })
        .then(response => {

            let requestData = response.data;

            if (!requestData.success) {

            }

            vue.$store.commit("wysiwyg/snippets", requestData.data.list);

        })
        .catch(error => {

        });

};