import {checkType} from "../../../General/js/general/check-type";
import {destructuring} from "../../../General/js/general/destructuring";

let setError = (input, type, error) => {

    if (!checkType(input, "object") || !checkType(type, "string") || !checkType(error, "object")) {
        if (DEV) console.error("Invalid variables type ($input or $type or $error). Type required $input = 'object', $type = 'string', $error = 'object'. Type received $input = '%s', $type = '%s', $error = '%s'.", typeof input, typeof type, typeof error);
        return false;
    }

    input.error.status = true;
    input.success = false;
    input.error.message = input.error.texts[type] ? input.error.texts[type] : null;
    error.type = type;

};

export let checkForm = inputs => {

    if (!checkType(inputs, "object")) {
        if (DEV) console.error("Invalid variable type ($inputs). Type required 'object'. Type received '%s'.", typeof inputs);
        return false;
    }

    let errors = [];

    for (let i in inputs) {

        let input = inputs[i];

        if (!checkType(input, "object")) {
            if (DEV) console.warn();
            continue;
        }

        let {value, type, error, length: {min, max}, regExp: {pattern, flags}, params: {notValid}} = destructuring(input);
        let $error = {
            input: i,
            type: null
        };

        if (!notValid) {

            switch (type) {
                case "image":

                    if (!checkType(value.file, "object")) {
                        setError(input, "empty", $error);
                    }

                    break;
                case "checkbox":
                    break;
                default:

                    if (!value && value !== 0) {
                        setError(input, "empty", $error);
                    } else {

                        if (checkType(min, "number") && value.length < min) {
                            setError(input, "min", $error);
                        }

                        if (checkType(max, "number") && value.length > max) {
                            setError(input, "max", $error);
                        }

                        if (checkType(pattern, "string") && checkType(flags, "string") && !new RegExp(pattern, flags).test(value)) {
                            setError(input, "regExp", $error);
                        }

                    }

                    break;
            }

        }

        if (!error.status) {
            error.status = false;
            error.message = null;
            input.success = true;
        } else {
            errors.push($error);
        }

    }

    return {
        status: errors.length === 0,
        errors
    };

};