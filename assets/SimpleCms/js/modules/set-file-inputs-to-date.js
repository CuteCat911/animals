import {checkType} from "../../../General/js/general/check-type";

export let setFileInputsToDate = (inputs, data) => {

    if (!checkType(inputs, "object") || !checkType(data, "object")) {
        if (DEV) console.error();
        return false;
    }

    for (let i in inputs) {
        if (inputs[i].type === "image" && inputs[i].value.file) {
            data.append(inputs[i].type + "-" + i, inputs[i].value.file);
        }
    }

};