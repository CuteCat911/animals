import {checkType} from "../../../General/js/general/check-type";

export let setInputsToDate = (inputs, other = false) => {

    if (!checkType(inputs, "object")) {
        if (DEV) console.error();
        return false;
    }

    let data = {};

    for (let i in inputs) {
        if (other) {

            if (inputs[i].type !== "image") {
                data[i] = JSON.stringify({
                    value: inputs[i].value,
                    type: inputs[i].type,
                    fieldData: inputs[i].fieldData
                });
            } else {
                data[i] = JSON.stringify({
                    value: inputs[i].value.name,
                    type: inputs[i].type,
                    fieldData: inputs[i].fieldData
                });
            }

        } else {
            data[i] = inputs[i].value;
        }
    }

    return data;

};