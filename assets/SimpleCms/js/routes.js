// Pages

import index from "../components/pages/index";
import login from "../components/pages/login";
import logout from "../components/pages/logout";
import simpleManager from "../components/pages/simple-manager/simple-manager";
import simpleManagerInfo from "../components/pages/simple-manager/simple-manager-info";
import extensions from "../components/pages/simple-manager/extensions/extensions";
import collections from "../components/pages/simple-manager/extensions/collections/collections";
import collection from "../components/pages/simple-manager/extensions/collections/collection";
import newCollectionItem from "../components/pages/simple-manager/extensions/collections/new-collection-item";
import collectionItem from "../components/pages/simple-manager/extensions/collections/collection-item";
import editableFields from "../components/pages/simple-manager/editable-fields/editable-fields";
import newEditableField from "../components/pages/simple-manager/editable-fields/new-eidtable-field";
import editableField from "../components/pages/simple-manager/editable-fields/editable-field";
import categories from "../components/pages/simple-manager/categories/categories";
import newCategory from "../components/pages/simple-manager/categories/new-category";
import category from "../components/pages/simple-manager/categories/category";

// End Pages

export const Routes = [
    {
        path: "/",
        name: "index",
        component: index
    },
    {
        path: "/login",
        name: "login",
        component: login
    },
    {
        path: "/logout",
        name: "logout",
        component: logout
    },
    {
        path: "/logout_",
        name: "backendLogout"
    },
    {
        path: "/simple-manager/",
        name: "simpleManager",
        redirect: {name: "simpleManagerInfo"},
        component: simpleManager,
        children: [
            {
                path: "",
                name: "simpleManagerInfo",
                component: simpleManagerInfo
            },
            {
                path: "extensions",
                name: "extensions",
                component: extensions
            },
            {
                path: "collections",
                name: "collections",
                component: collections
            },
            {
                path: "collection/:id",
                name: "collection",
                component: collection
            },
            {
                path: "collection/:collectionId/category/:type/:categoryId",
                name: "collectionCategory"
            },
            {
                path: "collection/:collectionId/item/:type/new",
                name: "createCollectionItem",
                component: collectionItem
            },
            {
                path: "collection/:collectionId/item/:type/:itemId",
                name: "collectionItem",
                component: collectionItem
            },
            {
                path: "editable-fields",
                name: "editableFields",
                component: editableFields
            },
            {
                path: "editable-field/new",
                name: "createEditableFields",
                component: newEditableField
            },
            {
                path: "editable-field/:id",
                name: "editableField",
                component: editableField
            },
            {
                path: "categories",
                name: "categories",
                component: categories
            },
            {
                path: "category/new",
                name: "createCategory",
                component: newCategory
            },
            {
                path: "category/:id",
                name: "category",
                component: category
            }
        ]
    },
    {
        path: "*"
    }
];