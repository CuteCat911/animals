import "babel-polyfill";
import "es6-promise/auto";
import Vue from "vue";
import {Router} from "./router";
import {Store} from "../store/store";
import Meta from "vue-meta";
import Vue2Storage from "vue2-storage";
import {mapState} from "vuex";
import "../languages/ml";

import cmsHeader from "../components/general/header";

import {getEndpointData} from "./modules/get-endpoint-data";
import {checkForm} from "./modules/check-form";
import {setInputError} from "./modules/set-input-error";
import {redirectToPage} from "./modules/redirect-to-page";
import {changeLang} from "./modules/change-lang";
import {orderIndex} from "./modules/order-index";
import {generateInputsByStructure} from "./modules/generate-inputs-by-structure";
import {getSnippetsData} from "./modules/get-snippets-data";
import {checkMl} from "./modules/check-ml";

Vue.use(Meta);
Vue.use(Vue2Storage, {
    prefix: "sc_",
    ttl: 60 * 60 * 24 * 1000 * 7
});

document.addEventListener("DOMContentLoaded", () => {
    
    const CMS = new Vue({
        el: "#cms",
        router: Router,
        store: Store,
        components: {
            cmsHeader
        },
        computed: {
            ...mapState("cms", {
                lang: state => state.lang
            })
        },
        methods: {
            getEndpointData,
            checkForm,
            setInputError,
            redirectToPage,
            defaultLogout() {
                this.redirectToPage(this.$router, {name: "logout"});
            },
            changeLang,
            orderIndex,
            generateInputsByStructure,
            getSnippetsData,
            checkMl
        }
    });
    
});