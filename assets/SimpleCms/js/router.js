import Vue from "vue";
import VueRouter from "vue-router";
import {stateClasses} from "../../General/js/general/state-classes";
import {Routes} from "./routes";
import {routerLogic} from "./modules/router-logic";
import {Store} from "../store/store";

Vue.use(VueRouter);

export const Router = new VueRouter({
    base: "/simple-cms/",
    mode: "history",
    linkActiveClass: stateClasses.active,
    linkExactActiveClass: stateClasses.exactActive,
    routes: Routes
});

Router.beforeEach((to, from, next) => {
    routerLogic({
        vue: Router.app,
        store: Store,
        storage: Router.app.$storage,
        to,
        from,
        next
    });
});