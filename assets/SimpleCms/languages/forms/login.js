export const FormLogin = {
    ru: {
        titles: {
            title: "Вход в учетную запись SimpleCms",
            inputs: {
                login: "Логин",
                password: "Пароль"
            }
        },
        btns: {
            entry: "Вход"
        },
        errors: {
            inputs: {
                login: {
                    empty: "Введите логин",
                    min: "Логин должен быть длиннее 2 символов",
                    regExp: "Введите корректный логин",
                    wrong: "Неверно указан логин или пароль",
                    blocked: "Пользователь заблокирован"
                },
                password: {
                    empty: "Введите пароль",
                    min: "Пароль должен длиннее 5 символов",
                    wrong: "Неверно указан логин или пароль"
                }
            }
        }
    },
    en: {
        titles: {
            title: "",
            inputs: {
                login: "",
                password: ""
            }
        },
        btns: {
            entry: ""
        },
        errors: {
            inputs: {
                login: {
                    empty: "",
                    min: "",
                    regExp: "",
                    notCorrect: "",
                    wrong: "",
                    blocked: ""
                },
                password: {
                    empty: "",
                    min: "",
                    notCorrect: "",
                    wrong: ""
                }
            }
        }
    }
};