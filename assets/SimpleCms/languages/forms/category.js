export const CategoryForm = {
    ru: {
        titles: {
            name: "Название",
            serverName: "Серверное имя",
            description: "Описание",
            index: "Порядковый номер"
        },
        btns: {

        },
        errors: {

        }
    },
    en: {
        titles: {
            name: "",
            serverName: "",
            description: "",
            index: ""
        },
        btns: {

        },
        errors: {

        }
    }
};