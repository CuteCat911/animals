export const EditableFieldForm = {
    ru: {
        titles: {
            name: "Название",
            serverName: "Серверное имя",
            description: "Описание",
            index: "Порядковый номер",
            type: "Тип поля",
            defaultValue: "Значение по умолчанию",
            pagesItems: "Страницы",
            otherItems: "Другие ресурсы"
        },
        btns: {
            save: "Сохранить"
        },
        errors: {

        }
    },
    en: {
        titles: {
            name: "",
            serverName: "",
            description: "",
            index: "",
            type: "",
            defaultValue: "",
            pagesItems: "",
            otherItems: ""
        },
        btns: {
            save: ""
        },
        errors: {

        }
    }
};