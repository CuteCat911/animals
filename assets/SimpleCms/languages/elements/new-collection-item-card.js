export const NewCollectionItemCard = {
    ru: {
        titles: {
            categories: "Новая категория",
            items: "Новый объект"
        },
        other: {
            emptySelectOption: "Выберите"
        },
        btns: {
            create: "Создать"
        }
    },
    en: {
        titles: {
            categories: "",
            items: ""
        },
        other: {
            emptySelectOption: ""
        },
        btns: {
            create: ""
        }
    }
};