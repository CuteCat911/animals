// Blocks

import {Header} from "./blocks/header";
import {Notifications} from "./blocks/notifications";
import {OnlineUsers} from "./blocks/online-users";
import {SiteStatistics} from "./blocks/site-statistics";
import {SimpleCmsNews} from "./blocks/simple-cms-news";
import {SimpleManagerAside} from "./blocks/simple-manager-aside";
import {Wysiwyg} from "./blocks/wysiwyg";

// End Blocks

// Elements

import {CollectionCard} from "./elements/collecion-card";
import {NewCollectionItemCard} from "./elements/new-collection-item-card";

// End Elements

// Pages

import {PageLogin} from "./pages/login";
import {PageLogout} from "./pages/logout";
import {PageIndex} from "./pages/index";
import {SimpleManagerInfo} from "./pages/simple-manager-info";
import {Extensions} from "./pages/extensions";
import {Collections} from "./pages/collections";
import {Collection} from "./pages/collection";
import {NewCollectionItem} from "./pages/new-collection-item";
import {CollectionItem} from "./pages/collection-item";
import {NewEditableField} from "./pages/new-eidtable-field";
import {EditableField} from "./pages/editable-field";
import {Categories} from "./pages/categories";
import {Category} from "./pages/category";
import {NewCategory} from "./pages/new-category";

// End Pages

// Forms

import {FormLogin} from "./forms/login";
import {CategoryForm} from "./forms/category";
import {EditableFieldForm} from "./forms/editable-field";

// End Forms

// Custom inputs

import {CustomCollectionItem} from "./custom-inputs/collection-item";
import {EditableFieldApplyItem} from "./custom-inputs/editable-field-apply-item";

// End Custom inputs

// Default

import {Seo} from "./default/seo";

// End Default

export const En = {
    blocks: {
        header: Header.en,
        notifications: Notifications.en,
        onlineUsers: OnlineUsers.en,
        siteStatistics: SiteStatistics.en,
        simpleCmsNews: SimpleCmsNews.en,
        simpleManagerAside: SimpleManagerAside.en,
        wysiwyg: Wysiwyg.en
    },
    elements: {
        collectionCard: CollectionCard.en,
        newCollectionItemCard: NewCollectionItemCard.en
    },
    forms: {
        login: FormLogin.en,
        category: CategoryForm.en,
        editableField: EditableFieldForm.en
    },
    pages: {
        login: PageLogin.en,
        logout: PageLogout.en,
        index: PageIndex.en,
        simpleManagerInfo: SimpleManagerInfo.en,
        extensions: Extensions.en,
        collections: Collections.en,
        collection: Collection.en,
        newCollectionItem: NewCollectionItem.en,
        collectionItem: CollectionItem.en,
        newEditableField: NewEditableField.en,
        editableField: EditableField.en,
        categories: Categories.en,
        category: Category.en,
        newCategory: NewCategory.en
    },
    popups: {},
    customInputs: {
        collectionItem: CustomCollectionItem.en,
        editableFieldApplyItem: EditableFieldApplyItem.en
    },
    default: {
        seo: Seo.en
    }
};