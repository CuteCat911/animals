export const PageLogin = {
    ru: {
        meta: {
            title: "Вход"
        },
        links: {
            faq: "Faq",
            instruction: "Инструкция",
            support: "Тех. поддержка",
            login: "Вход в учетную запись",
            recovery: "Забыли пароль?"
        }
    },
    en: {
        meta: {
            title: "Login"
        },
        links: {
            faq: "",
            instruction: "",
            support: "",
            login: "",
            recovery: ""
        }
    }
};