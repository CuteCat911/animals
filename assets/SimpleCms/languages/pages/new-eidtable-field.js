export const NewEditableField = {
    ru: {
        meta: {
            title: ""
        },
        types: {
            text: "Текст",
            content: "Контент",
            image: "Изображение",
            file: "Файл",
            multiple: "Несколько полей"
        }
    },
    en: {
        meta: {
            title: ""
        },
        types: {
            text: "",
            content: "",
            image: "",
            file: "",
            multiple: ""
        }
    }
};