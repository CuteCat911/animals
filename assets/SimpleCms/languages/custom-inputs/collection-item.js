export const CustomCollectionItem = {
    ru: {
        article: {
            name: {
                title: "Название статьи"
            },
            breadcrumbName: {
                title: "Название статья для хлебных крошек"
            },
            path: {
                title: "Url"
            },
            published: {
                title: "Опубликовать статью"
            },
            index: {
                title: "Позиция"
            },
            publicationTime: {
                title: "Время публикации"
            },
            category: {
                title: "Категории"
            }
        }
    },
    en: {
        article: {
            name: {
                title: ""
            },
            breadcrumbName: {
                title: ""
            },
            path: {
                title: ""
            },
            published: {
                title: ""
            },
            index: {
                title: ""
            },
            publicationTime: {
                title: ""
            },
            category: {
                title: ""
            }
        }
    }
};