// Blocks

import {Header} from "./blocks/header";
import {Notifications} from "./blocks/notifications";
import {OnlineUsers} from "./blocks/online-users";
import {SiteStatistics} from "./blocks/site-statistics";
import {SimpleCmsNews} from "./blocks/simple-cms-news";
import {SimpleManagerAside} from "./blocks/simple-manager-aside";
import {Wysiwyg} from "./blocks/wysiwyg";

// End Blocks

// Elements

import {CollectionCard} from "./elements/collecion-card";
import {NewCollectionItemCard} from "./elements/new-collection-item-card";

// End Elements

// Pages

import {PageLogin} from "./pages/login";
import {PageLogout} from "./pages/logout";
import {PageIndex} from "./pages/index";
import {SimpleManagerInfo} from "./pages/simple-manager-info";
import {Extensions} from "./pages/extensions";
import {Collections} from "./pages/collections";
import {Collection} from "./pages/collection";
import {NewCollectionItem} from "./pages/new-collection-item";
import {CollectionItem} from "./pages/collection-item";
import {NewEditableField} from "./pages/new-eidtable-field";
import {EditableField} from "./pages/editable-field";
import {Categories} from "./pages/categories";
import {Category} from "./pages/category";
import {NewCategory} from "./pages/new-category";

// End Pages

// Forms

import {FormLogin} from "./forms/login";
import {CategoryForm} from "./forms/category";
import {EditableFieldForm} from "./forms/editable-field";

// End Forms

// Custom inputs

import {CustomCollectionItem} from "./custom-inputs/collection-item";
import {EditableFieldApplyItem} from "./custom-inputs/editable-field-apply-item";

// End Custom inputs

// Default

import {Seo} from "./default/seo";

// End DEfault

export const Ru = {
    blocks: {
        header: Header.ru,
        notifications: Notifications.ru,
        onlineUsers: OnlineUsers.ru,
        siteStatistics: SiteStatistics.ru,
        simpleCmsNews: SimpleCmsNews.ru,
        simpleManagerAside: SimpleManagerAside.ru,
        wysiwyg: Wysiwyg.ru
    },
    elements: {
        collectionCard: CollectionCard.ru,
        newCollectionItemCard: NewCollectionItemCard.ru
    },
    forms: {
        login: FormLogin.ru,
        category: CategoryForm.ru,
        editableField: EditableFieldForm.ru
    },
    pages: {
        login: PageLogin.ru,
        logout: PageLogout.ru,
        index: PageIndex.ru,
        simpleManagerInfo: SimpleManagerInfo.ru,
        extensions: Extensions.ru,
        collections: Collections.ru,
        collection: Collection.ru,
        newCollectionItem: NewCollectionItem.ru,
        collectionItem: CollectionItem.ru,
        newEditableField: NewEditableField.ru,
        editableField: EditableField.ru,
        categories: Categories.ru,
        category: Category.ru,
        newCategory: NewCategory.ru
    },
    popups: {},
    customInputs: {
        collectionItem: CustomCollectionItem.ru,
        editableFieldApplyItem: EditableFieldApplyItem.ru
    },
    default: {
        seo: Seo.ru
    }
};