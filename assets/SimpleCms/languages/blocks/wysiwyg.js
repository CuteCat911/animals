export const Wysiwyg = {
    ru: {
        selects: {
            textStyles: {
                p: "Параграф",
                h1: "Заголовок H1",
                h2: "Заголовок H2",
                h3: "Заголовок H3",
                h4: "Заголовок H4",
                h5: "Заголовок H5",
                h6: "Заголовок H6",
                blockquote: "Цитата"
            }
        },
        snippets: {
            btn: "Сниппеты"
        }
    },
    en: {
        selects: {
            textStyles: {
                p: "",
                h1: "",
                h2: "",
                h3: "",
                h4: "",
                h5: "",
                h6: "",
                blockquote: ""
            }
        },
        snippets: {
            btn: ""
        }
    }
};