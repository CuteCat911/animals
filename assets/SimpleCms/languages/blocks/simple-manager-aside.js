export const SimpleManagerAside = {
    ru: {
        nav: {
            extensions: "Расширения",
            collections: "Коллекции",
            editableFields: "Редактируемые поля",
            categories: "Категории"
        }
    },
    en: {
        nav: {
            extensions: "",
            collections: "",
            editableFields: "",
            categories: ""
        }
    }
};