import Vue from "vue";
import {MLInstaller, MLCreate, MLanguage} from "vue-multilanguage";
import {Store} from "../store/store";

// Languages

import {Ru} from "./ru";
import {En} from "./en";

// End Languages

Vue.use(MLInstaller);

export default new MLCreate({
    initial: Store.getters["cms/lang"],
    languages: [
        new MLanguage("en").create(En),
        new MLanguage("ru").create(Ru)
    ]
});