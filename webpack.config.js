var Encore = require('@symfony/webpack-encore');

Encore
    .configureDefinePlugin((options) => {
        options.DEV = JSON.stringify(!Encore.isProduction());
    })
    .enableStylusLoader()
    .setOutputPath("public/app/build/")
    .setPublicPath("/app/build")
    .addEntry("common_js", "./assets/App/js/common.js")
    .addStyleEntry("common_css", "./assets/App/stylus/common.styl")
    .enablePostCssLoader((options) => {
        options.config = {
            path: "config/postcss.config.js"
        };
    })
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
;

const AppConfig = Encore.getWebpackConfig();
AppConfig.name = "app";

Encore.reset();

Encore
    .configureDefinePlugin((options) => {
        options.DEV = JSON.stringify(!Encore.isProduction());
    })
    .enableStylusLoader()
    .setOutputPath("public/simple_cms/build/")
    .setPublicPath("/simple_cms/build")
    .addEntry("simple_cms_js", "./assets/SimpleCms/js/cms.js")
    .addStyleEntry("simple_cms_css", "./assets/SimpleCms/stylus/cms.styl")
    .enablePostCssLoader((options) => {
        options.config = {
            path: "config/postcss.config.js"
        };
    })
    .enableVueLoader()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
;

const SimpleCMSConfig = Encore.getWebpackConfig();
SimpleCMSConfig.name = "simple_cms";

module.exports = [AppConfig, SimpleCMSConfig];
